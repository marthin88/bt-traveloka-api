package com.ocbcnisp.traveloka.configuration;

import static java.util.Arrays.asList;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.ocbcnisp.traveloka.security.filter.JWTAuthenticationFilter;
import com.ocbcnisp.traveloka.security.filter.RequestBodyAuthenticationFilter;
import com.ocbcnisp.traveloka.security.handler.SuccessAuthenticationHandler;
import com.ocbcnisp.traveloka.security.provider.NISPADProvider;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private NISPADProvider provider;
	
	@Autowired
	private Environment env;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(provider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		final String jwtSecret = env.getProperty("nispad.secret");
		if (jwtSecret == null || jwtSecret.isEmpty()) {
			throw new IllegalStateException("JWT secret is mandatory");
		}
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
		http.csrf().disable().exceptionHandling().and();
		http.cors().and();
		authorizations(http);
		hsts(http);
		http.addFilterBefore(requestBodyAuthFilter(), UsernamePasswordAuthenticationFilter.class);
		http.addFilterBefore(new JWTAuthenticationFilter(jwtSecret), BasicAuthenticationFilter.class);
	}
	
	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/api/auth/getRSA").antMatchers("/api/login");
	}
	
	@Bean
    public RequestBodyAuthenticationFilter requestBodyAuthFilter() throws Exception{
    	RequestBodyAuthenticationFilter filter = new RequestBodyAuthenticationFilter();
    	filter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/api/login", "POST"));
    	filter.setAuthenticationSuccessHandler(new SuccessAuthenticationHandler(env.getProperty("nispad.secret")));
    	filter.setAuthenticationManager(authenticationManagerBean());
    	return filter;
    }
	private HttpSecurity authorizations(HttpSecurity security) throws Exception{
		return security.authorizeRequests()
				.anyRequest().authenticated().and();
	}
	
	private HttpSecurity hsts(HttpSecurity security) throws Exception {
        HeadersConfigurer<HttpSecurity>.HstsConfig hstsConfig = security.headers().httpStrictTransportSecurity();

        Boolean hstsEnabled = true;
        if (hstsEnabled) {
            return hstsConfig
                    .includeSubDomains(true)
                    .maxAgeInSeconds(31536000L)
                    .and().and();
        }
        return hstsConfig.disable().and();
    }
	
	@Bean
    public CorsConfigurationSource corsConfigurationSource() {
//        final CorsConfiguration config = new CorsConfiguration();
//        config.setAllowCredentials(true);
//        config.setAllowedOrigins(getPropertiesAsList("http.cors.allow-origin", "*"));
//        config.setAllowedHeaders(getPropertiesAsList("http.cors.allow-headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, If-Match, OCBCNISP-TOKEN"));
//        config.setAllowedMethods(getPropertiesAsList("http.cors.allow-methods", "GET, POST, PUT, DELETE"));
//        config.setExposedHeaders(getPropertiesAsList("http.cors.exposed-headers", "ETag"));
//        config.setMaxAge(env.getProperty("http.cors.max-age", Long.class, 1728000L));
//        config.setExposedHeaders(Collections.singletonList("ETag"));

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
	
	private List<String> getPropertiesAsList(final String propertyKey, final String defaultValue) {
        String property = env.getProperty(propertyKey);
        if (property == null) {
            property = defaultValue;
        }
        return asList(property.replaceAll("\\s+","").split(","));
    }
}
