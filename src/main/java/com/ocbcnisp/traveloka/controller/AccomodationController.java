package com.ocbcnisp.traveloka.controller;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.BTDetailHarpaEntity;
import com.ocbcnisp.traveloka.entity.ClaimEntity;
import com.ocbcnisp.traveloka.entity.ClaimHarpaEntity;
import com.ocbcnisp.traveloka.entity.GeoLocationEntity;
import com.ocbcnisp.traveloka.exception.AccomodationException;
import com.ocbcnisp.traveloka.model.accomodation.response.GeoList;
import com.ocbcnisp.traveloka.model.accomodation.response.GeoRegion;
import com.ocbcnisp.traveloka.model.accomodation.response.HotelBook;
import com.ocbcnisp.traveloka.model.accomodation.response.PreBook;
import com.ocbcnisp.traveloka.model.accomodation.response.RoomsList;
import com.ocbcnisp.traveloka.model.accomodation.response.SearchList;
import com.ocbcnisp.traveloka.model.http.request.HBook;
import com.ocbcnisp.traveloka.model.http.request.HPreBook;
import com.ocbcnisp.traveloka.model.http.request.HRoom;
import com.ocbcnisp.traveloka.model.http.response.GlobalModel;
import com.ocbcnisp.traveloka.repository.IGeoLocationRepository2;
import com.ocbcnisp.traveloka.service.IAccomodationService;
import com.ocbcnisp.traveloka.service.IBTDetailService;
@RestController
//@CrossOrigin(allowCredentials = "true",origins = "*")
@RequestMapping("api/accomodation")
public class AccomodationController {
	
	@Autowired
	private IAccomodationService accomodationService;
	
	@Autowired
	private IBTDetailService btDetailService;
	
	@Autowired
	private IGeoLocationRepository2 geoRepo;
	
//	@GetMapping("/validateBT")
//	public GeoList getLocation(@RequestParam String btNumber) throws URISyntaxException {
//		return accomodationService.getGeoList(location);
//	}
//	@GetMapping("/get-geo")
//	public GeoList getGeo() throws URISyntaxException {
//			Location location = new Location();
//			location.setCountryCode("ID");
//			location.setLimit("500");
//			location.setOffset("7501");
//			GeoList geo = accomodationService.getGeoList(location);
//		return geo;
//	}
//	
//	@PostMapping("/get-geo")
//	public String setGeo(@RequestBody GeoList geoList) {
//		for (GeoRegion geoRegion : geoList.getGeoRegionList()) {
//			GeoLocationEntity geoLoc = new GeoLocationEntity();
//			geoLoc.setGeoId(geoRegion.getGeoId());
//			geoLoc.setName(geoRegion.getName());
//			geoLoc.setLocaleName(geoRegion.getLocaleName());
//			geoLoc.setParentId(geoRegion.getParentId());
//			geoLoc.setType(geoRegion.getType());
//			geoLoc.setNewEntity(true);
//			geoRepo.save(geoLoc);
//		}
//		return null;
//	}
	
	@GetMapping("/findBTEligible")
	public GlobalModel findBTEligible(@RequestParam String btId) {
		GlobalModel model = new GlobalModel();
		BTDetailEntity entity = btDetailService.getById(btId);
		if (entity == null) {
			model.setMessage("BT is not eligible");
			model.setData(null);
		}else {
			model.setMessage("SUCCESS");
			model.setData(entity);
		}
		return model;
	}
	
	@GetMapping("/findListBT")
	public GlobalModel findListBT() {
		GlobalModel model = new GlobalModel();
		List<BTDetailHarpaEntity> list = btDetailService.getListBT();
		model.setMessage("SUCCESS");
		model.setData(list);
		return model; 
	}
	
	@GetMapping("/search-hotel")
	public SearchList getHotel(@RequestParam String btId, @RequestParam String limit, @RequestParam String offset) throws URISyntaxException, ParseException, UnknownHostException {
		return accomodationService.getSearchList(btId, limit, offset);
	}
	
	@PostMapping("/search-room")
	public RoomsList getRoom(@RequestBody HRoom hRoom) throws URISyntaxException {
		return accomodationService.getRoomsList(hRoom);
	}
	
	@PostMapping("/prebook")
	public PreBook getPrebook(@RequestBody HPreBook preBook) throws URISyntaxException {
		BTDetailHarpaEntity btDetail = btDetailService.getByBtIdOnHarpa(preBook.getBtId().get(0));
		ClaimHarpaEntity claims = new ClaimHarpaEntity();
		for (ClaimHarpaEntity claim : btDetail.getClaimEntity()) {
			if (claim.getClaimItem().equals("Hotel")) {
				claims = claim;
			}
		}
		return accomodationService.getPreBook(claims, preBook.getPropertyId(), preBook.getRoomId(), preBook.getRateKey(),"2");
	}
	
	@PostMapping("/create-book")
	public HotelBook postbook(@RequestBody HBook hbook) throws URISyntaxException, UnknownHostException, ParseException, AccomodationException {
		return accomodationService.createBook(hbook);
	}

}
