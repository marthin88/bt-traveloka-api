package com.ocbcnisp.traveloka.controller;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocbcnisp.traveloka.security.model.RSAPublicKeyAD;
import com.ocbcnisp.traveloka.security.provider.NISPADService;
import com.ocbcnisp.traveloka.utility.CommonUtil;

@RestController
@RequestMapping("api/auth")
public class AuthController {

	@Autowired
	private NISPADService nispADService;
	
	@Autowired
	private Environment env;
	@GetMapping("/getRSA")
	public RSAPublicKeyAD getPublicKey() throws RemoteException {
		String pubKey = nispADService.getRSAPublicKey(env.getProperty("nispad.url"));
		RSAPublicKeyAD rsaResult = (RSAPublicKeyAD) CommonUtil.convertStringToObject(RSAPublicKeyAD.class, pubKey);
		KeyFactory factory;
		try {
			factory = KeyFactory.getInstance("RSA");
			byte[] modulusBytes = Base64.getDecoder().decode(rsaResult.getModulus());
			byte[] exponentBytes = Base64.getDecoder().decode(rsaResult.getExponent());
			
			BigInteger mod = new BigInteger(1, modulusBytes);
			BigInteger exp = new BigInteger(1, exponentBytes);
			RSAPublicKeySpec spec = new RSAPublicKeySpec(mod, exp);
			RSAPublicKey pub = (RSAPublicKey) factory.generatePublic(spec);
			rsaResult.setModulus(pub.getModulus().toString(16));
			rsaResult.setExponent(pub.getPublicExponent().toString(16));
			return rsaResult;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
}
