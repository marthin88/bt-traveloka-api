package com.ocbcnisp.traveloka.controller;

import java.net.URISyntaxException;

import com.ocbcnisp.traveloka.model.auth.APIType;
import com.ocbcnisp.traveloka.model.flight.response.AirlineData;
import com.ocbcnisp.traveloka.model.flight.response.AirportData;
import com.ocbcnisp.traveloka.service.impl.RestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/flight")
public class FlightController {
	
	@Autowired
	private RestService restService;
	
	@GetMapping("/airport-data")
	public AirportData testConnection() throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/flight/data/airports?locale=en_EN";
		AirportData airportData = (AirportData) restService.get(APIType.FLIGHT, url, AirportData.class);
		return airportData;
		
	}
	
	@GetMapping("/airline-data")
	public AirlineData testAirline() throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/flight/data/airports?locale=en_EN";
		AirlineData airlineData = (AirlineData) restService.get(APIType.FLIGHT, url, AirlineData.class);
		return airlineData;
		
	}
}
