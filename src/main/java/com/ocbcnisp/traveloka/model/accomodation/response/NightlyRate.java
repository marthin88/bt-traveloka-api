
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "baseRate",
    "promo",
    "nightRate"
})
public class NightlyRate {

    @JsonProperty("date")
    private String date;
    @JsonProperty("baseRate")
    private String baseRate;
    @JsonProperty("promo")
    private Boolean promo;
    @JsonProperty("nightRate")
    private String nightRate;

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    public NightlyRate withDate(String date) {
        this.date = date;
        return this;
    }

    @JsonProperty("baseRate")
    public String getBaseRate() {
        return baseRate;
    }

    @JsonProperty("baseRate")
    public void setBaseRate(String baseRate) {
        this.baseRate = baseRate;
    }

    public NightlyRate withBaseRate(String baseRate) {
        this.baseRate = baseRate;
        return this;
    }

    @JsonProperty("promo")
    public Boolean getPromo() {
        return promo;
    }

    @JsonProperty("promo")
    public void setPromo(Boolean promo) {
        this.promo = promo;
    }

    public NightlyRate withPromo(Boolean promo) {
        this.promo = promo;
        return this;
    }

    @JsonProperty("nightRate")
    public String getNightRate() {
        return nightRate;
    }

    @JsonProperty("nightRate")
    public void setNightRate(String nightRate) {
        this.nightRate = nightRate;
    }

    public NightlyRate withNightRate(String nightRate) {
        this.nightRate = nightRate;
        return this;
    }

}
