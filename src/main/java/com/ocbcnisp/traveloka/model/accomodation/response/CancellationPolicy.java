
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "currencyCode",
    "timeZoneDisplay",
    "cancelDateTime",
    "fees",
    "isAfter",
    "after"
})
public class CancellationPolicy {

    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("timeZoneDisplay")
    private String timeZoneDisplay;
    @JsonProperty("cancelDateTime")
    private String cancelDateTime;
    @JsonProperty("fees")
    private String fees;
    @JsonProperty("isAfter")
    private Boolean isAfter;
    @JsonProperty("after")
    private Boolean after;

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public CancellationPolicy withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    @JsonProperty("timeZoneDisplay")
    public String getTimeZoneDisplay() {
        return timeZoneDisplay;
    }

    @JsonProperty("timeZoneDisplay")
    public void setTimeZoneDisplay(String timeZoneDisplay) {
        this.timeZoneDisplay = timeZoneDisplay;
    }

    public CancellationPolicy withTimeZoneDisplay(String timeZoneDisplay) {
        this.timeZoneDisplay = timeZoneDisplay;
        return this;
    }

    @JsonProperty("cancelDateTime")
    public String getCancelDateTime() {
        return cancelDateTime;
    }

    @JsonProperty("cancelDateTime")
    public void setCancelDateTime(String cancelDateTime) {
        this.cancelDateTime = cancelDateTime;
    }

    public CancellationPolicy withCancelDateTime(String cancelDateTime) {
        this.cancelDateTime = cancelDateTime;
        return this;
    }

    @JsonProperty("fees")
    public String getFees() {
        return fees;
    }

    @JsonProperty("fees")
    public void setFees(String fees) {
        this.fees = fees;
    }

    public CancellationPolicy withFees(String fees) {
        this.fees = fees;
        return this;
    }

    @JsonProperty("isAfter")
    public Boolean getIsAfter() {
        return isAfter;
    }

    @JsonProperty("isAfter")
    public void setIsAfter(Boolean isAfter) {
        this.isAfter = isAfter;
    }

    public CancellationPolicy withIsAfter(Boolean isAfter) {
        this.isAfter = isAfter;
        return this;
    }

    @JsonProperty("after")
    public Boolean getAfter() {
        return after;
    }

    @JsonProperty("after")
    public void setAfter(Boolean after) {
        this.after = after;
    }

    public CancellationPolicy withAfter(Boolean after) {
        this.after = after;
        return this;
    }

}
