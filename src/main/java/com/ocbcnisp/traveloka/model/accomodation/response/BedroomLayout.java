
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "arrangements",
    "arrangementType"
})
public class BedroomLayout {

    @JsonProperty("arrangements")
    private List<Arrangement> arrangements = new ArrayList<Arrangement>();
    @JsonProperty("arrangementType")
    private String arrangementType;

    @JsonProperty("arrangements")
    public List<Arrangement> getArrangements() {
        return arrangements;
    }

    @JsonProperty("arrangements")
    public void setArrangements(List<Arrangement> arrangements) {
        this.arrangements = arrangements;
    }

    public BedroomLayout withArrangements(List<Arrangement> arrangements) {
        this.arrangements = arrangements;
        return this;
    }

    @JsonProperty("arrangementType")
    public String getArrangementType() {
        return arrangementType;
    }

    @JsonProperty("arrangementType")
    public void setArrangementType(String arrangementType) {
        this.arrangementType = arrangementType;
    }

    public BedroomLayout withArrangementType(String arrangementType) {
        this.arrangementType = arrangementType;
        return this;
    }

}
