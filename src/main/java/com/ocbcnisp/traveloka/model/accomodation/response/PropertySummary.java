
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "formerName",
    "address",
    "localAddress",
    "starRating",
    "reviewScore",
    "accommodationType",
    "geoLocation"
})
public class PropertySummary {

    @JsonProperty("name")
    private String name;
    @JsonProperty("formerName")
    private Object formerName;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("localAddress")
    private LocalAddress localAddress;
    @JsonProperty("starRating")
    private String starRating;
    @JsonProperty("reviewScore")
    private String reviewScore;
    @JsonProperty("accommodationType")
    private String accommodationType;
    @JsonProperty("geoLocation")
    private GeoLocation geoLocation;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public PropertySummary withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("formerName")
    public Object getFormerName() {
        return formerName;
    }

    @JsonProperty("formerName")
    public void setFormerName(Object formerName) {
        this.formerName = formerName;
    }

    public PropertySummary withFormerName(Object formerName) {
        this.formerName = formerName;
        return this;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    public PropertySummary withAddress(Address address) {
        this.address = address;
        return this;
    }

    @JsonProperty("localAddress")
    public LocalAddress getLocalAddress() {
        return localAddress;
    }

    @JsonProperty("localAddress")
    public void setLocalAddress(LocalAddress localAddress) {
        this.localAddress = localAddress;
    }

    public PropertySummary withLocalAddress(LocalAddress localAddress) {
        this.localAddress = localAddress;
        return this;
    }

    @JsonProperty("starRating")
    public String getStarRating() {
        return starRating;
    }

    @JsonProperty("starRating")
    public void setStarRating(String starRating) {
        this.starRating = starRating;
    }

    public PropertySummary withStarRating(String starRating) {
        this.starRating = starRating;
        return this;
    }

    @JsonProperty("reviewScore")
    public String getReviewScore() {
        return reviewScore;
    }

    @JsonProperty("reviewScore")
    public void setReviewScore(String reviewScore) {
        this.reviewScore = reviewScore;
    }

    public PropertySummary withReviewScore(String reviewScore) {
        this.reviewScore = reviewScore;
        return this;
    }

    @JsonProperty("accommodationType")
    public String getAccommodationType() {
        return accommodationType;
    }

    @JsonProperty("accommodationType")
    public void setAccommodationType(String accommodationType) {
        this.accommodationType = accommodationType;
    }

    public PropertySummary withAccommodationType(String accommodationType) {
        this.accommodationType = accommodationType;
        return this;
    }

    @JsonProperty("geoLocation")
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    @JsonProperty("geoLocation")
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public PropertySummary withGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
        return this;
    }

}
