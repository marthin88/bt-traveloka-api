
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "lon",
    "lat",
    "valid"
})
public class Centroid {

    @JsonProperty("lon")
    private String lon;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("valid")
    private Boolean valid;

    @JsonProperty("lon")
    public String getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon) {
        this.lon = lon;
    }

    public Centroid withLon(String lon) {
        this.lon = lon;
        return this;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    public Centroid withLat(String lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("valid")
    public Boolean getValid() {
        return valid;
    }

    @JsonProperty("valid")
    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Centroid withValid(Boolean valid) {
        this.valid = valid;
        return this;
    }

}
