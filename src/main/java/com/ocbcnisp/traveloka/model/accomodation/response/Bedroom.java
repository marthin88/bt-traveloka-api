
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bedroomLayouts"
})
public class Bedroom {

    @JsonProperty("bedroomLayouts")
    private List<BedroomLayout> bedroomLayouts = new ArrayList<BedroomLayout>();

    @JsonProperty("bedroomLayouts")
    public List<BedroomLayout> getBedroomLayouts() {
        return bedroomLayouts;
    }

    @JsonProperty("bedroomLayouts")
    public void setBedroomLayouts(List<BedroomLayout> bedroomLayouts) {
        this.bedroomLayouts = bedroomLayouts;
    }

    public Bedroom withBedroomLayouts(List<BedroomLayout> bedroomLayouts) {
        this.bedroomLayouts = bedroomLayouts;
        return this;
    }

}
