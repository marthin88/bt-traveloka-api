
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "roomTypeName",
    "description",
    "images",
    "amenities",
    "quotedOccupancy",
    "maxOccupancy",
    "smokingPreference",
    "rates",
    "breakfastIncluded",
    "wifiIncluded",
    "bedType",
    "bedroomSummary",
    "bedrooms",
    "roomSize"
})
public class Room {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("roomTypeName")
    private Object roomTypeName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("images")
    private List<Image> images = new ArrayList<Image>();
    @JsonProperty("amenities")
    private List<Amenity> amenities = new ArrayList<Amenity>();
    @JsonProperty("quotedOccupancy")
    private String quotedOccupancy;
    @JsonProperty("maxOccupancy")
    private String maxOccupancy;
    @JsonProperty("smokingPreference")
    private Object smokingPreference;
    @JsonProperty("rates")
    private Rates rates;
    @JsonProperty("breakfastIncluded")
    private Boolean breakfastIncluded;
    @JsonProperty("wifiIncluded")
    private Boolean wifiIncluded;
    @JsonProperty("bedType")
    private String bedType;
    @JsonProperty("bedroomSummary")
    private String bedroomSummary;
    @JsonProperty("bedrooms")
    private List<Bedroom> bedrooms = new ArrayList<Bedroom>();
    @JsonProperty("roomSize")
    private RoomSize roomSize;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public Room withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Room withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("roomTypeName")
    public Object getRoomTypeName() {
        return roomTypeName;
    }

    @JsonProperty("roomTypeName")
    public void setRoomTypeName(Object roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    public Room withRoomTypeName(Object roomTypeName) {
        this.roomTypeName = roomTypeName;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Room withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Room withImages(List<Image> images) {
        this.images = images;
        return this;
    }

    @JsonProperty("amenities")
    public List<Amenity> getAmenities() {
        return amenities;
    }

    @JsonProperty("amenities")
    public void setAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
    }

    public Room withAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
        return this;
    }

    @JsonProperty("quotedOccupancy")
    public String getQuotedOccupancy() {
        return quotedOccupancy;
    }

    @JsonProperty("quotedOccupancy")
    public void setQuotedOccupancy(String quotedOccupancy) {
        this.quotedOccupancy = quotedOccupancy;
    }

    public Room withQuotedOccupancy(String quotedOccupancy) {
        this.quotedOccupancy = quotedOccupancy;
        return this;
    }

    @JsonProperty("maxOccupancy")
    public String getMaxOccupancy() {
        return maxOccupancy;
    }

    @JsonProperty("maxOccupancy")
    public void setMaxOccupancy(String maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public Room withMaxOccupancy(String maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
        return this;
    }

    @JsonProperty("smokingPreference")
    public Object getSmokingPreference() {
        return smokingPreference;
    }

    @JsonProperty("smokingPreference")
    public void setSmokingPreference(Object smokingPreference) {
        this.smokingPreference = smokingPreference;
    }

    public Room withSmokingPreference(Object smokingPreference) {
        this.smokingPreference = smokingPreference;
        return this;
    }

    @JsonProperty("rates")
    public Rates getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(Rates rates) {
        this.rates = rates;
    }

    public Room withRates(Rates rates) {
        this.rates = rates;
        return this;
    }

    @JsonProperty("breakfastIncluded")
    public Boolean getBreakfastIncluded() {
        return breakfastIncluded;
    }

    @JsonProperty("breakfastIncluded")
    public void setBreakfastIncluded(Boolean breakfastIncluded) {
        this.breakfastIncluded = breakfastIncluded;
    }

    public Room withBreakfastIncluded(Boolean breakfastIncluded) {
        this.breakfastIncluded = breakfastIncluded;
        return this;
    }

    @JsonProperty("wifiIncluded")
    public Boolean getWifiIncluded() {
        return wifiIncluded;
    }

    @JsonProperty("wifiIncluded")
    public void setWifiIncluded(Boolean wifiIncluded) {
        this.wifiIncluded = wifiIncluded;
    }

    public Room withWifiIncluded(Boolean wifiIncluded) {
        this.wifiIncluded = wifiIncluded;
        return this;
    }

    @JsonProperty("bedType")
    public String getBedType() {
        return bedType;
    }

    @JsonProperty("bedType")
    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public Room withBedType(String bedType) {
        this.bedType = bedType;
        return this;
    }

    @JsonProperty("bedroomSummary")
    public String getBedroomSummary() {
        return bedroomSummary;
    }

    @JsonProperty("bedroomSummary")
    public void setBedroomSummary(String bedroomSummary) {
        this.bedroomSummary = bedroomSummary;
    }

    public Room withBedroomSummary(String bedroomSummary) {
        this.bedroomSummary = bedroomSummary;
        return this;
    }

    @JsonProperty("bedrooms")
    public List<Bedroom> getBedrooms() {
        return bedrooms;
    }

    @JsonProperty("bedrooms")
    public void setBedrooms(List<Bedroom> bedrooms) {
        this.bedrooms = bedrooms;
    }

    public Room withBedrooms(List<Bedroom> bedrooms) {
        this.bedrooms = bedrooms;
        return this;
    }

    @JsonProperty("roomSize")
    public RoomSize getRoomSize() {
        return roomSize;
    }

    @JsonProperty("roomSize")
    public void setRoomSize(RoomSize roomSize) {
        this.roomSize = roomSize;
    }

    public Room withRoomSize(RoomSize roomSize) {
        this.roomSize = roomSize;
        return this;
    }

}
