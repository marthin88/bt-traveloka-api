
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "currencyCode",
    "averageBaseRate",
    "averageRate",
    "nightlyRateTotal",
    "surchargeTotal",
    "total",
    "surcharges",
    "nightlyRates",
    "chargeableRateInfo"
})
public class ChargeableRate {

    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("averageBaseRate")
    private String averageBaseRate;
    @JsonProperty("averageRate")
    private String averageRate;
    @JsonProperty("nightlyRateTotal")
    private String nightlyRateTotal;
    @JsonProperty("surchargeTotal")
    private String surchargeTotal;
    @JsonProperty("total")
    private String total;
    @JsonProperty("surcharges")
    private List<Surcharge> surcharges = new ArrayList<Surcharge>();
    @JsonProperty("nightlyRates")
    private List<NightlyRate> nightlyRates = new ArrayList<NightlyRate>();
    @JsonProperty("chargeableRateInfo")
    private Object chargeableRateInfo;

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public ChargeableRate withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    @JsonProperty("averageBaseRate")
    public String getAverageBaseRate() {
        return averageBaseRate;
    }

    @JsonProperty("averageBaseRate")
    public void setAverageBaseRate(String averageBaseRate) {
        this.averageBaseRate = averageBaseRate;
    }

    public ChargeableRate withAverageBaseRate(String averageBaseRate) {
        this.averageBaseRate = averageBaseRate;
        return this;
    }

    @JsonProperty("averageRate")
    public String getAverageRate() {
        return averageRate;
    }

    @JsonProperty("averageRate")
    public void setAverageRate(String averageRate) {
        this.averageRate = averageRate;
    }

    public ChargeableRate withAverageRate(String averageRate) {
        this.averageRate = averageRate;
        return this;
    }

    @JsonProperty("nightlyRateTotal")
    public String getNightlyRateTotal() {
        return nightlyRateTotal;
    }

    @JsonProperty("nightlyRateTotal")
    public void setNightlyRateTotal(String nightlyRateTotal) {
        this.nightlyRateTotal = nightlyRateTotal;
    }

    public ChargeableRate withNightlyRateTotal(String nightlyRateTotal) {
        this.nightlyRateTotal = nightlyRateTotal;
        return this;
    }

    @JsonProperty("surchargeTotal")
    public String getSurchargeTotal() {
        return surchargeTotal;
    }

    @JsonProperty("surchargeTotal")
    public void setSurchargeTotal(String surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
    }

    public ChargeableRate withSurchargeTotal(String surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
        return this;
    }

    @JsonProperty("total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(String total) {
        this.total = total;
    }

    public ChargeableRate withTotal(String total) {
        this.total = total;
        return this;
    }

    @JsonProperty("surcharges")
    public List<Surcharge> getSurcharges() {
        return surcharges;
    }

    @JsonProperty("surcharges")
    public void setSurcharges(List<Surcharge> surcharges) {
        this.surcharges = surcharges;
    }

    public ChargeableRate withSurcharges(List<Surcharge> surcharges) {
        this.surcharges = surcharges;
        return this;
    }

    @JsonProperty("nightlyRates")
    public List<NightlyRate> getNightlyRates() {
        return nightlyRates;
    }

    @JsonProperty("nightlyRates")
    public void setNightlyRates(List<NightlyRate> nightlyRates) {
        this.nightlyRates = nightlyRates;
    }

    public ChargeableRate withNightlyRates(List<NightlyRate> nightlyRates) {
        this.nightlyRates = nightlyRates;
        return this;
    }

    @JsonProperty("chargeableRateInfo")
    public Object getChargeableRateInfo() {
        return chargeableRateInfo;
    }

    @JsonProperty("chargeableRateInfo")
    public void setChargeableRateInfo(Object chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
    }

    public ChargeableRate withChargeableRateInfo(Object chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
        return this;
    }

}
