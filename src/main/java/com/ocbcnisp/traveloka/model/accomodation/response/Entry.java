
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imageType",
    "url"
})
public class Entry {

    @JsonProperty("imageType")
    private String imageType;
    @JsonProperty("url")
    private String url;

    @JsonProperty("imageType")
    public String getImageType() {
        return imageType;
    }

    @JsonProperty("imageType")
    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public Entry withImageType(String imageType) {
        this.imageType = imageType;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public Entry withUrl(String url) {
        this.url = url;
        return this;
    }

}
