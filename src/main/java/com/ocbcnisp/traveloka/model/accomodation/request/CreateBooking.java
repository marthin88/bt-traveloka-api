package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ocbcnisp.traveloka.model.accomodation.response.PreBook;
import com.ocbcnisp.traveloka.model.http.request.HBook;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "metadata",
        "affiliateConfirmationIds",
        "propertyId",
        "checkInDate",
        "checkOutDate",
        "rateTypes",
        "rooms",
        "expectedTotalChargeableRate",
        "chargeableRateInfo",
        "userContact",
        "affiliateContact",
        "userPaymentMethod",
        "specialRequest",
        "loginId",
        "loginType"
})
public class CreateBooking {

    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("affiliateConfirmationIds")
    private String affiliateConfirmationIds;
    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("checkInDate")
    private String checkInDate;
    @JsonProperty("checkOutDate")
    private String checkOutDate;
    @JsonProperty("rateTypes")
    private List<String> rateTypes = null;
    @JsonProperty("rooms")
    private List<Room> rooms = null;
    @JsonProperty("expectedTotalChargeableRate")
    private ExpectedTotalChargeableRate expectedTotalChargeableRate;
    @JsonProperty("chargeableRateInfo")
    private ChargeableRateInfo chargeableRateInfo;
    @JsonProperty("userContact")
    private UserContact userContact;
    @JsonProperty("affiliateContact")
    private AffiliateContact affiliateContact;
    @JsonProperty("userPaymentMethod")
    private String userPaymentMethod;
    @JsonProperty("specialRequest")
    private String specialRequest;
    @JsonProperty("loginId")
    private String loginId;
    @JsonProperty("loginType")
    private String loginType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public CreateBooking(HBook hbook, PreBook preBook) {
    	AffiliateContact affContact = new AffiliateContact();
    	affContact.setAffiliatePhoneNumber(Arrays.asList("0812345676"));
    	affContact.setAffiliateEmail("affiliate@ocbcnisp.com");
    	this.affiliateContact = affContact;
    	this.loginType = "EMAIL";
    	Room room = new Room();
    	room.setNumOfAdult("2");
    	room.setRoomId(preBook.getRooms().get(0).getId());
    	room.setRateKey(preBook.getRooms().get(0).getRates().getRateKey());
    	room.setGuests(hbook.getGuest());
    	this.rooms = Arrays.asList(room);
    	UserContact user = new UserContact();
    	user.setEmail(hbook.getEmail());
    	user.setFirstName(hbook.getGuest().get(0).getFirstName());
    	user.setLastName(hbook.getGuest().get(0).getLastName());
    	user.setTitle(hbook.getGuest().get(0).getTitle());
    	user.setPhones(Arrays.asList(hbook.getPhone()));
    	this.userContact = user;
    	
	}
    public CreateBooking() {
	}
    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonProperty("affiliateConfirmationIds")
    public String getAffiliateConfirmationIds() {
        return affiliateConfirmationIds;
    }

    @JsonProperty("affiliateConfirmationIds")
    public void setAffiliateConfirmationIds(String affiliateConfirmationIds) {
        this.affiliateConfirmationIds = affiliateConfirmationIds;
    }

    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    @JsonProperty("checkInDate")
    public String getCheckInDate() {
        return checkInDate;
    }

    @JsonProperty("checkInDate")
    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    @JsonProperty("checkOutDate")
    public String getCheckOutDate() {
        return checkOutDate;
    }

    @JsonProperty("checkOutDate")
    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @JsonProperty("rateTypes")
    public List<String> getRateTypes() {
        return rateTypes;
    }

    @JsonProperty("rateTypes")
    public void setRateTypes(List<String> rateTypes) {
        this.rateTypes = rateTypes;
    }

    @JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @JsonProperty("expectedTotalChargeableRate")
    public ExpectedTotalChargeableRate getExpectedTotalChargeableRate() {
        return expectedTotalChargeableRate;
    }

    @JsonProperty("expectedTotalChargeableRate")
    public void setExpectedTotalChargeableRate(ExpectedTotalChargeableRate expectedTotalChargeableRate) {
        this.expectedTotalChargeableRate = expectedTotalChargeableRate;
    }

    @JsonProperty("chargeableRateInfo")
    public ChargeableRateInfo getChargeableRateInfo() {
        return chargeableRateInfo;
    }

    @JsonProperty("chargeableRateInfo")
    public void setChargeableRateInfo(ChargeableRateInfo chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
    }

    @JsonProperty("userContact")
    public UserContact getUserContact() {
        return userContact;
    }

    @JsonProperty("userContact")
    public void setUserContact(UserContact userContact) {
        this.userContact = userContact;
    }

    @JsonProperty("affiliateContact")
    public AffiliateContact getAffiliateContact() {
        return affiliateContact;
    }

    @JsonProperty("affiliateContact")
    public void setAffiliateContact(AffiliateContact affiliateContact) {
        this.affiliateContact = affiliateContact;
    }

    @JsonProperty("userPaymentMethod")
    public String getUserPaymentMethod() {
        return userPaymentMethod;
    }

    @JsonProperty("userPaymentMethod")
    public void setUserPaymentMethod(String userPaymentMethod) {
        this.userPaymentMethod = userPaymentMethod;
    }

    @JsonProperty("specialRequest")
    public String getSpecialRequest() {
        return specialRequest;
    }

    @JsonProperty("specialRequest")
    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    @JsonProperty("loginId")
    public String getLoginId() {
        return loginId;
    }

    @JsonProperty("loginId")
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @JsonProperty("loginType")
    public String getLoginType() {
        return loginType;
    }

    @JsonProperty("loginType")
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}