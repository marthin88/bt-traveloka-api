
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalProperties",
    "maximalOffset",
    "properties"
})
public class SearchList {

    @JsonProperty("totalProperties")
    private String totalProperties;
    @JsonProperty("maximalOffset")
    private String maximalOffset;
    @JsonProperty("properties")
    private List<Property> properties = new ArrayList<Property>();

    @JsonProperty("totalProperties")
    public String getTotalProperties() {
        return totalProperties;
    }

    @JsonProperty("totalProperties")
    public void setTotalProperties(String totalProperties) {
        this.totalProperties = totalProperties;
    }

    public SearchList withTotalProperties(String totalProperties) {
        this.totalProperties = totalProperties;
        return this;
    }

    @JsonProperty("maximalOffset")
    public String getMaximalOffset() {
        return maximalOffset;
    }

    @JsonProperty("maximalOffset")
    public void setMaximalOffset(String maximalOffset) {
        this.maximalOffset = maximalOffset;
    }

    public SearchList withMaximalOffset(String maximalOffset) {
        this.maximalOffset = maximalOffset;
        return this;
    }

    @JsonProperty("properties")
    public List<Property> getProperties() {
        return properties;
    }

    @JsonProperty("properties")
    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public SearchList withProperties(List<Property> properties) {
        this.properties = properties;
        return this;
    }

}
