
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "propertyDatas"
})
public class PropertyDetails {

    @JsonProperty("propertyDatas")
    private List<PropertyData> propertyDatas = new ArrayList<PropertyData>();

    @JsonProperty("propertyDatas")
    public List<PropertyData> getPropertyDatas() {
        return propertyDatas;
    }

    @JsonProperty("propertyDatas")
    public void setPropertyDatas(List<PropertyData> propertyDatas) {
        this.propertyDatas = propertyDatas;
    }

    public PropertyDetails withPropertyDatas(List<PropertyData> propertyDatas) {
        this.propertyDatas = propertyDatas;
        return this;
    }

}
