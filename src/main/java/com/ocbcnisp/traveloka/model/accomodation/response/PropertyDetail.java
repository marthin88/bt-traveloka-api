
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "checkInTime",
    "checkOutTime",
    "importanceNotice",
    "hotelPolicy",
    "checkInInstruction",
    "availableRateTypes"
})
public class PropertyDetail {

    @JsonProperty("checkInTime")
    private String checkInTime;
    @JsonProperty("checkOutTime")
    private String checkOutTime;
    @JsonProperty("importanceNotice")
    private String importanceNotice;
    @JsonProperty("hotelPolicy")
    private String hotelPolicy;
    @JsonProperty("checkInInstruction")
    private String checkInInstruction;
    @JsonProperty("availableRateTypes")
    private List<String> availableRateTypes = new ArrayList<String>();

    @JsonProperty("checkInTime")
    public String getCheckInTime() {
        return checkInTime;
    }

    @JsonProperty("checkInTime")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public PropertyDetail withCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
        return this;
    }

    @JsonProperty("checkOutTime")
    public String getCheckOutTime() {
        return checkOutTime;
    }

    @JsonProperty("checkOutTime")
    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public PropertyDetail withCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
        return this;
    }

    @JsonProperty("importanceNotice")
    public String getImportanceNotice() {
        return importanceNotice;
    }

    @JsonProperty("importanceNotice")
    public void setImportanceNotice(String importanceNotice) {
        this.importanceNotice = importanceNotice;
    }

    public PropertyDetail withImportanceNotice(String importanceNotice) {
        this.importanceNotice = importanceNotice;
        return this;
    }

    @JsonProperty("hotelPolicy")
    public String getHotelPolicy() {
        return hotelPolicy;
    }

    @JsonProperty("hotelPolicy")
    public void setHotelPolicy(String hotelPolicy) {
        this.hotelPolicy = hotelPolicy;
    }

    public PropertyDetail withHotelPolicy(String hotelPolicy) {
        this.hotelPolicy = hotelPolicy;
        return this;
    }

    @JsonProperty("checkInInstruction")
    public String getCheckInInstruction() {
        return checkInInstruction;
    }

    @JsonProperty("checkInInstruction")
    public void setCheckInInstruction(String checkInInstruction) {
        this.checkInInstruction = checkInInstruction;
    }

    public PropertyDetail withCheckInInstruction(String checkInInstruction) {
        this.checkInInstruction = checkInInstruction;
        return this;
    }

    @JsonProperty("availableRateTypes")
    public List<String> getAvailableRateTypes() {
        return availableRateTypes;
    }

    @JsonProperty("availableRateTypes")
    public void setAvailableRateTypes(List<String> availableRateTypes) {
        this.availableRateTypes = availableRateTypes;
    }

    public PropertyDetail withAvailableRateTypes(List<String> availableRateTypes) {
        this.availableRateTypes = availableRateTypes;
        return this;
    }

}
