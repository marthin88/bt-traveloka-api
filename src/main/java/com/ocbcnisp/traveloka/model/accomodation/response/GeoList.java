
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "geoRegionList"
})
public class GeoList {

    @JsonProperty("geoRegionList")
    private List<GeoRegion> geoRegionList = new ArrayList<GeoRegion>();

    @JsonProperty("geoRegionList")
    public List<GeoRegion> getGeoRegionList() {
        return geoRegionList;
    }

    @JsonProperty("geoRegionList")
    public void setGeoRegionList(List<GeoRegion> geoRegionList) {
        this.geoRegionList = geoRegionList;
    }

    public GeoList withGeoRegionList(List<GeoRegion> geoRegionList) {
        this.geoRegionList = geoRegionList;
        return this;
    }

}
