
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "propertyId",
    "status",
    "propertySummary",
    "propertyDetail",
    "propertyImages",
    "propertyAmenities",
    "rooms"
})
public class RoomsList {

    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("propertySummary")
    private PropertySummary propertySummary;
    @JsonProperty("propertyDetail")
    private PropertyDetail propertyDetail;
    @JsonProperty("propertyImages")
    private List<PropertyImage> propertyImages = new ArrayList<PropertyImage>();
    @JsonProperty("propertyAmenities")
    private List<PropertyAmenity> propertyAmenities = new ArrayList<PropertyAmenity>();
    @JsonProperty("rooms")
    private List<Room> rooms = new ArrayList<Room>();

    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public RoomsList withPropertyId(String propertyId) {
        this.propertyId = propertyId;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public RoomsList withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("propertySummary")
    public PropertySummary getPropertySummary() {
        return propertySummary;
    }

    @JsonProperty("propertySummary")
    public void setPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
    }

    public RoomsList withPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
        return this;
    }

    @JsonProperty("propertyDetail")
    public PropertyDetail getPropertyDetail() {
        return propertyDetail;
    }

    @JsonProperty("propertyDetail")
    public void setPropertyDetail(PropertyDetail propertyDetail) {
        this.propertyDetail = propertyDetail;
    }

    public RoomsList withPropertyDetail(PropertyDetail propertyDetail) {
        this.propertyDetail = propertyDetail;
        return this;
    }

    @JsonProperty("propertyImages")
    public List<PropertyImage> getPropertyImages() {
        return propertyImages;
    }

    @JsonProperty("propertyImages")
    public void setPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
    }

    public RoomsList withPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
        return this;
    }

    @JsonProperty("propertyAmenities")
    public List<PropertyAmenity> getPropertyAmenities() {
        return propertyAmenities;
    }

    @JsonProperty("propertyAmenities")
    public void setPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
        this.propertyAmenities = propertyAmenities;
    }

    public RoomsList withPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
        this.propertyAmenities = propertyAmenities;
        return this;
    }

    @JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public RoomsList withRooms(List<Room> rooms) {
        this.rooms = rooms;
        return this;
    }

}
