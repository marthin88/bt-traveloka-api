
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "lat",
    "lon"
})
public class GeoLocation {

    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lon")
    private String lon;

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    public GeoLocation withLat(String lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lon")
    public String getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon) {
        this.lon = lon;
    }

    public GeoLocation withLon(String lon) {
        this.lon = lon;
        return this;
    }

}
