
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rateKey",
    "numOfAdults",
    "rateType",
    "promos",
    "currentAllotments",
    "cancellationPolicies",
    "cancellationPolicyDisplay",
    "chargeableRate",
    "convertedChargeableRate"
})
public class Rates {

    @JsonProperty("rateKey")
    private String rateKey;
    @JsonProperty("numOfAdults")
    private String numOfAdults;
    @JsonProperty("rateType")
    private String rateType;
    @JsonProperty("promos")
    private Object promos;
    @JsonProperty("currentAllotments")
    private String currentAllotments;
    @JsonProperty("cancellationPolicies")
    private List<CancellationPolicy> cancellationPolicies = new ArrayList<CancellationPolicy>();
    @JsonProperty("cancellationPolicyDisplay")
    private String cancellationPolicyDisplay;
    @JsonProperty("chargeableRate")
    private ChargeableRate chargeableRate;
    @JsonProperty("convertedChargeableRate")
    private ConvertedChargeableRate convertedChargeableRate;

    @JsonProperty("rateKey")
    public String getRateKey() {
        return rateKey;
    }

    @JsonProperty("rateKey")
    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public Rates withRateKey(String rateKey) {
        this.rateKey = rateKey;
        return this;
    }

    @JsonProperty("numOfAdults")
    public String getNumOfAdults() {
        return numOfAdults;
    }

    @JsonProperty("numOfAdults")
    public void setNumOfAdults(String numOfAdults) {
        this.numOfAdults = numOfAdults;
    }

    public Rates withNumOfAdults(String numOfAdults) {
        this.numOfAdults = numOfAdults;
        return this;
    }

    @JsonProperty("rateType")
    public String getRateType() {
        return rateType;
    }

    @JsonProperty("rateType")
    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public Rates withRateType(String rateType) {
        this.rateType = rateType;
        return this;
    }

    @JsonProperty("promos")
    public Object getPromos() {
        return promos;
    }

    @JsonProperty("promos")
    public void setPromos(Object promos) {
        this.promos = promos;
    }

    public Rates withPromos(Object promos) {
        this.promos = promos;
        return this;
    }

    @JsonProperty("currentAllotments")
    public String getCurrentAllotments() {
        return currentAllotments;
    }

    @JsonProperty("currentAllotments")
    public void setCurrentAllotments(String currentAllotments) {
        this.currentAllotments = currentAllotments;
    }

    public Rates withCurrentAllotments(String currentAllotments) {
        this.currentAllotments = currentAllotments;
        return this;
    }

    @JsonProperty("cancellationPolicies")
    public List<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    @JsonProperty("cancellationPolicies")
    public void setCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }

    public Rates withCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
        return this;
    }

    @JsonProperty("cancellationPolicyDisplay")
    public String getCancellationPolicyDisplay() {
        return cancellationPolicyDisplay;
    }

    @JsonProperty("cancellationPolicyDisplay")
    public void setCancellationPolicyDisplay(String cancellationPolicyDisplay) {
        this.cancellationPolicyDisplay = cancellationPolicyDisplay;
    }

    public Rates withCancellationPolicyDisplay(String cancellationPolicyDisplay) {
        this.cancellationPolicyDisplay = cancellationPolicyDisplay;
        return this;
    }

    @JsonProperty("chargeableRate")
    public ChargeableRate getChargeableRate() {
        return chargeableRate;
    }

    @JsonProperty("chargeableRate")
    public void setChargeableRate(ChargeableRate chargeableRate) {
        this.chargeableRate = chargeableRate;
    }

    public Rates withChargeableRate(ChargeableRate chargeableRate) {
        this.chargeableRate = chargeableRate;
        return this;
    }

    @JsonProperty("convertedChargeableRate")
    public ConvertedChargeableRate getConvertedChargeableRate() {
        return convertedChargeableRate;
    }

    @JsonProperty("convertedChargeableRate")
    public void setConvertedChargeableRate(ConvertedChargeableRate convertedChargeableRate) {
        this.convertedChargeableRate = convertedChargeableRate;
    }

    public Rates withConvertedChargeableRate(ConvertedChargeableRate convertedChargeableRate) {
        this.convertedChargeableRate = convertedChargeableRate;
        return this;
    }

}
