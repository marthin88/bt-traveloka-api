
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entries",
    "isMain"
})
public class PropertyImage {

    @JsonProperty("entries")
    private List<Entry> entries = new ArrayList<Entry>();
    @JsonProperty("isMain")
    private Boolean isMain;

    @JsonProperty("entries")
    public List<Entry> getEntries() {
        return entries;
    }

    @JsonProperty("entries")
    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public PropertyImage withEntries(List<Entry> entries) {
        this.entries = entries;
        return this;
    }

    @JsonProperty("isMain")
    public Boolean getIsMain() {
        return isMain;
    }

    @JsonProperty("isMain")
    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }

    public PropertyImage withIsMain(Boolean isMain) {
        this.isMain = isMain;
        return this;
    }

}
