
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chargeableRate",
    "convertedChargeableRate",
    "numOfRooms"
})
public class CheapestRoom {

    @JsonProperty("chargeableRate")
    private ChargeableRate chargeableRate;
    @JsonProperty("convertedChargeableRate")
    private ConvertedChargeableRate convertedChargeableRate;
    @JsonProperty("numOfRooms")
    private String numOfRooms;

    @JsonProperty("chargeableRate")
    public ChargeableRate getChargeableRate() {
        return chargeableRate;
    }

    @JsonProperty("chargeableRate")
    public void setChargeableRate(ChargeableRate chargeableRate) {
        this.chargeableRate = chargeableRate;
    }

    public CheapestRoom withChargeableRate(ChargeableRate chargeableRate) {
        this.chargeableRate = chargeableRate;
        return this;
    }

    @JsonProperty("convertedChargeableRate")
    public ConvertedChargeableRate getConvertedChargeableRate() {
        return convertedChargeableRate;
    }

    @JsonProperty("convertedChargeableRate")
    public void setConvertedChargeableRate(ConvertedChargeableRate convertedChargeableRate) {
        this.convertedChargeableRate = convertedChargeableRate;
    }

    public CheapestRoom withConvertedChargeableRate(ConvertedChargeableRate convertedChargeableRate) {
        this.convertedChargeableRate = convertedChargeableRate;
        return this;
    }

    @JsonProperty("numOfRooms")
    public String getNumOfRooms() {
        return numOfRooms;
    }

    @JsonProperty("numOfRooms")
    public void setNumOfRooms(String numOfRooms) {
        this.numOfRooms = numOfRooms;
    }

    public CheapestRoom withNumOfRooms(String numOfRooms) {
        this.numOfRooms = numOfRooms;
        return this;
    }

}
