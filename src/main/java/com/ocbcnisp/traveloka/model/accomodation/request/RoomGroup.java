
package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rooms"
})
public class RoomGroup {

	@JsonProperty("roomId")
	private String roomId;
    @JsonProperty("rooms")
    private List<Room> rooms = new ArrayList<Room>();
    @JsonProperty("rateKey")
    private String rateKey;
    
    public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRateKey() {
		return rateKey;
	}

	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}

	@JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public RoomGroup withRooms(List<Room> rooms) {
        this.rooms = rooms;
        return this;
    }

}
