
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "geoId",
    "parentId",
    "type",
    "name",
    "localeName",
    "centroid"
})
public class GeoRegion {

    @JsonProperty("geoId")
    private String geoId;
    @JsonProperty("parentId")
    private String parentId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("localeName")
    private String localeName;
    @JsonProperty("centroid")
    private Centroid centroid;

    @JsonProperty("geoId")
    public String getGeoId() {
        return geoId;
    }

    @JsonProperty("geoId")
    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    public GeoRegion withGeoId(String geoId) {
        this.geoId = geoId;
        return this;
    }

    @JsonProperty("parentId")
    public String getParentId() {
        return parentId;
    }

    @JsonProperty("parentId")
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public GeoRegion withParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public GeoRegion withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public GeoRegion withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("localeName")
    public String getLocaleName() {
        return localeName;
    }

    @JsonProperty("localeName")
    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public GeoRegion withLocaleName(String localeName) {
        this.localeName = localeName;
        return this;
    }

    @JsonProperty("centroid")
    public Centroid getCentroid() {
        return centroid;
    }

    @JsonProperty("centroid")
    public void setCentroid(Centroid centroid) {
        this.centroid = centroid;
    }

    public GeoRegion withCentroid(Centroid centroid) {
        this.centroid = centroid;
        return this;
    }

}
