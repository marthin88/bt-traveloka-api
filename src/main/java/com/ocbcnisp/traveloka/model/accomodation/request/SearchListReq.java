
package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "checkInDate",
    "checkOutDate",
    "rateTypes",
    "roomGroup",
    "options",
    "geoId",
    "hotelIds",
    "sortType",
    "stars",
    "limit",
    "offset",
    "metadata"
})
public class SearchListReq {

    @JsonProperty("checkInDate")
    private String checkInDate;
    @JsonProperty("checkOutDate")
    private String checkOutDate;
    @JsonProperty("rateTypes")
    private List<String> rateTypes = new ArrayList<String>();
    @JsonProperty("roomGroup")
    private RoomGroup roomGroup;
    @JsonProperty("options")
    private List<String> options = new ArrayList<String>();
    @JsonProperty("geoId")
    private String geoId;
    @JsonProperty("hotelIds")
    private List<String> hotelIds;
    @JsonProperty("sortType")
    private String sortType;
    @JsonProperty("stars")
    private List<Boolean> stars = new ArrayList<Boolean>();
    @JsonProperty("limit")
    private String limit;
    @JsonProperty("offset")
    private String offset;
    @JsonProperty("metadata")
    private Metadata metadata;

    public List<String> getHotelIds() {
		return hotelIds;
	}

	public void setHotelIds(List<String> hotelIds) {
		this.hotelIds = hotelIds;
	}

	@JsonProperty("checkInDate")
    public String getCheckInDate() {
        return checkInDate;
    }

    @JsonProperty("checkInDate")
    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public SearchListReq withCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    @JsonProperty("checkOutDate")
    public String getCheckOutDate() {
        return checkOutDate;
    }

    @JsonProperty("checkOutDate")
    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public SearchListReq withCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    @JsonProperty("rateTypes")
    public List<String> getRateTypes() {
        return rateTypes;
    }

    @JsonProperty("rateTypes")
    public void setRateTypes(List<String> rateTypes) {
        this.rateTypes = rateTypes;
    }

    public SearchListReq withRateTypes(List<String> rateTypes) {
        this.rateTypes = rateTypes;
        return this;
    }

    @JsonProperty("roomGroup")
    public RoomGroup getRoomGroup() {
        return roomGroup;
    }

    @JsonProperty("roomGroup")
    public void setRoomGroup(RoomGroup roomGroup) {
        this.roomGroup = roomGroup;
    }

    public SearchListReq withRoomGroup(RoomGroup roomGroup) {
        this.roomGroup = roomGroup;
        return this;
    }

    @JsonProperty("options")
    public List<String> getOptions() {
        return options;
    }

    @JsonProperty("options")
    public void setOptions(List<String> options) {
        this.options = options;
    }

    public SearchListReq withOptions(List<String> options) {
        this.options = options;
        return this;
    }

    @JsonProperty("geoId")
    public String getGeoId() {
        return geoId;
    }

    @JsonProperty("geoId")
    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    public SearchListReq withGeoId(String geoId) {
        this.geoId = geoId;
        return this;
    }

    @JsonProperty("sortType")
    public String getSortType() {
        return sortType;
    }

    @JsonProperty("sortType")
    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public SearchListReq withSortType(String sortType) {
        this.sortType = sortType;
        return this;
    }

    @JsonProperty("stars")
    public List<Boolean> getStars() {
        return stars;
    }

    @JsonProperty("stars")
    public void setStars(List<Boolean> stars) {
        this.stars = stars;
    }

    public SearchListReq withStars(List<Boolean> stars) {
        this.stars = stars;
        return this;
    }

    @JsonProperty("limit")
    public String getLimit() {
        return limit;
    }

    @JsonProperty("limit")
    public void setLimit(String limit) {
        this.limit = limit;
    }

    public SearchListReq withLimit(String limit) {
        this.limit = limit;
        return this;
    }

    @JsonProperty("offset")
    public String getOffset() {
        return offset;
    }

    @JsonProperty("offset")
    public void setOffset(String offset) {
        this.offset = offset;
    }

    public SearchListReq withOffset(String offset) {
        this.offset = offset;
        return this;
    }

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public SearchListReq withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

}
