package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "itineraryId",
        "email",
        "affiliateConfirmationId",
        "reason"
})
public class Cancellation {

    @JsonProperty("itineraryId")
    private String itineraryId;
    @JsonProperty("email")
    private String email;
    @JsonProperty("affiliateConfirmationId")
    private String affiliateConfirmationId;
    @JsonProperty("reason")
    private String reason;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("itineraryId")
    public String getItineraryId() {
        return itineraryId;
    }

    @JsonProperty("itineraryId")
    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("affiliateConfirmationId")
    public String getAffiliateConfirmationId() {
        return affiliateConfirmationId;
    }

    @JsonProperty("affiliateConfirmationId")
    public void setAffiliateConfirmationId(String affiliateConfirmationId) {
        this.affiliateConfirmationId = affiliateConfirmationId;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}