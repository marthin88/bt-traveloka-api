
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "propertyId",
    "propertySummary",
    "propertyDetail",
    "propertyImages",
    "propertyAmenities"
})
public class PropertyData {

    @JsonProperty("status")
    private String status;
    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("propertySummary")
    private PropertySummary propertySummary;
    @JsonProperty("propertyDetail")
    private PropertyDetail propertyDetail;
    @JsonProperty("propertyImages")
    private List<PropertyImage> propertyImages = new ArrayList<PropertyImage>();
    @JsonProperty("propertyAmenities")
    private List<PropertyAmenity> propertyAmenities = new ArrayList<PropertyAmenity>();

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public PropertyData withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public PropertyData withPropertyId(String propertyId) {
        this.propertyId = propertyId;
        return this;
    }

    @JsonProperty("propertySummary")
    public PropertySummary getPropertySummary() {
        return propertySummary;
    }

    @JsonProperty("propertySummary")
    public void setPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
    }

    public PropertyData withPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
        return this;
    }

    @JsonProperty("propertyDetail")
    public PropertyDetail getPropertyDetail() {
        return propertyDetail;
    }

    @JsonProperty("propertyDetail")
    public void setPropertyDetail(PropertyDetail propertyDetail) {
        this.propertyDetail = propertyDetail;
    }

    public PropertyData withPropertyDetail(PropertyDetail propertyDetail) {
        this.propertyDetail = propertyDetail;
        return this;
    }

    @JsonProperty("propertyImages")
    public List<PropertyImage> getPropertyImages() {
        return propertyImages;
    }

    @JsonProperty("propertyImages")
    public void setPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
    }

    public PropertyData withPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
        return this;
    }

    @JsonProperty("propertyAmenities")
    public List<PropertyAmenity> getPropertyAmenities() {
        return propertyAmenities;
    }

    @JsonProperty("propertyAmenities")
    public void setPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
        this.propertyAmenities = propertyAmenities;
    }

    public PropertyData withPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
        this.propertyAmenities = propertyAmenities;
        return this;
    }

}
