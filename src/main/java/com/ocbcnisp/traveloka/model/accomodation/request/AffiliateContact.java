package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "affiliatePhoneNumber",
        "affiliateEmail"
})
public class AffiliateContact {

    @JsonProperty("affiliatePhoneNumber")
    private List<String> affiliatePhoneNumber = null;
    @JsonProperty("affiliateEmail")
    private String affiliateEmail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("affiliatePhoneNumber")
    public List<String> getAffiliatePhoneNumber() {
        return affiliatePhoneNumber;
    }

    @JsonProperty("affiliatePhoneNumber")
    public void setAffiliatePhoneNumber(List<String> affiliatePhoneNumber) {
        this.affiliatePhoneNumber = affiliatePhoneNumber;
    }

    @JsonProperty("affiliateEmail")
    public String getAffiliateEmail() {
        return affiliateEmail;
    }

    @JsonProperty("affiliateEmail")
    public void setAffiliateEmail(String affiliateEmail) {
        this.affiliateEmail = affiliateEmail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}