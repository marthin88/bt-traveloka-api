
package com.ocbcnisp.traveloka.model.accomodation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bedType",
    "total"
})
public class Arrangement {

    @JsonProperty("bedType")
    private String bedType;
    @JsonProperty("total")
    private String total;

    @JsonProperty("bedType")
    public String getBedType() {
        return bedType;
    }

    @JsonProperty("bedType")
    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public Arrangement withBedType(String bedType) {
        this.bedType = bedType;
        return this;
    }

    @JsonProperty("total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(String total) {
        this.total = total;
    }

    public Arrangement withTotal(String total) {
        this.total = total;
        return this;
    }

}
