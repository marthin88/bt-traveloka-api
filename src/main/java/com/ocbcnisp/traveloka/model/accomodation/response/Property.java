
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "propertyId",
    "propertySummary",
    "propertyImages",
    "cheapestRoom",
    "cheapestRoomName",
    "rateKey"
})
public class Property {

    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("propertySummary")
    private PropertySummary propertySummary;
    @JsonProperty("propertyImages")
    private List<PropertyImage> propertyImages = new ArrayList<PropertyImage>();
    @JsonProperty("cheapestRoom")
    private CheapestRoom cheapestRoom;
    @JsonProperty("cheapestRoomName")
    private String cheapestRoomName;
    @JsonProperty("rateKey")
    private String rateKey;

    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public Property withPropertyId(String propertyId) {
        this.propertyId = propertyId;
        return this;
    }

    @JsonProperty("propertySummary")
    public PropertySummary getPropertySummary() {
        return propertySummary;
    }

    @JsonProperty("propertySummary")
    public void setPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
    }

    public Property withPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
        return this;
    }

    @JsonProperty("propertyImages")
    public List<PropertyImage> getPropertyImages() {
        return propertyImages;
    }

    @JsonProperty("propertyImages")
    public void setPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
    }

    public Property withPropertyImages(List<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
        return this;
    }

    @JsonProperty("cheapestRoom")
    public CheapestRoom getCheapestRoom() {
        return cheapestRoom;
    }

    @JsonProperty("cheapestRoom")
    public void setCheapestRoom(CheapestRoom cheapestRoom) {
        this.cheapestRoom = cheapestRoom;
    }

    public Property withCheapestRoom(CheapestRoom cheapestRoom) {
        this.cheapestRoom = cheapestRoom;
        return this;
    }

    @JsonProperty("cheapestRoomName")
    public String getCheapestRoomName() {
        return cheapestRoomName;
    }

    @JsonProperty("cheapestRoomName")
    public void setCheapestRoomName(String cheapestRoomName) {
        this.cheapestRoomName = cheapestRoomName;
    }

    public Property withCheapestRoomName(String cheapestRoomName) {
        this.cheapestRoomName = cheapestRoomName;
        return this;
    }

    @JsonProperty("rateKey")
    public String getRateKey() {
        return rateKey;
    }

    @JsonProperty("rateKey")
    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public Property withRateKey(String rateKey) {
        this.rateKey = rateKey;
        return this;
    }

}
