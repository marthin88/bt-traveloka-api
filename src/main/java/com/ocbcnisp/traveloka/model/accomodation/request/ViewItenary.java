package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "metadata",
        "itineraryId",
        "affiliateConfirmationId",
        "bookingTimeRange",
        "email"
})
public class ViewItenary {

    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("itineraryId")
    private String itineraryId;
    @JsonProperty("affiliateConfirmationId")
    private String affiliateConfirmationId;
    @JsonProperty("bookingTimeRange")
    private BookingTimeRange bookingTimeRange;
    @JsonProperty("email")
    private String email;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonProperty("itineraryId")
    public String getItineraryId() {
        return itineraryId;
    }

    @JsonProperty("itineraryId")
    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    @JsonProperty("affiliateConfirmationId")
    public String getAffiliateConfirmationId() {
        return affiliateConfirmationId;
    }

    @JsonProperty("affiliateConfirmationId")
    public void setAffiliateConfirmationId(String affiliateConfirmationId) {
        this.affiliateConfirmationId = affiliateConfirmationId;
    }

    @JsonProperty("bookingTimeRange")
    public BookingTimeRange getBookingTimeRange() {
        return bookingTimeRange;
    }

    @JsonProperty("bookingTimeRange")
    public void setBookingTimeRange(BookingTimeRange bookingTimeRange) {
        this.bookingTimeRange = bookingTimeRange;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}