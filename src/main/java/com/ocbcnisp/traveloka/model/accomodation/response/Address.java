
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "lines",
    "city",
    "countryName",
    "postalCode"
})
public class Address {

    @JsonProperty("lines")
    private List<String> lines = new ArrayList<String>();
    @JsonProperty("city")
    private String city;
    @JsonProperty("countryName")
    private String countryName;
    @JsonProperty("postalCode")
    private String postalCode;

    @JsonProperty("lines")
    public List<String> getLines() {
        return lines;
    }

    @JsonProperty("lines")
    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public Address withLines(List<String> lines) {
        this.lines = lines;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public Address withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("countryName")
    public String getCountryName() {
        return countryName;
    }

    @JsonProperty("countryName")
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Address withCountryName(String countryName) {
        this.countryName = countryName;
        return this;
    }

    @JsonProperty("postalCode")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("postalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Address withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

}
