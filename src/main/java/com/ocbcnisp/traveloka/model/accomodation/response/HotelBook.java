
package com.ocbcnisp.traveloka.model.accomodation.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "bookingId",
    "itineraryId",
    "issuerName",
    "propertyId",
    "propertySummary",
    "checkInDate",
    "checkOutDate",
    "rooms",
    "totalChargeableRate",
    "chargeableRateInfo",
    "additionalRates",
    "convertedTotalChargeableRate",
    "surchargeTotal",
    "convertedSurchargeTotal"
})
public class HotelBook {

    @JsonProperty("status")
    private String status;
    @JsonProperty("bookingId")
    private String bookingId;
    @JsonProperty("itineraryId")
    private String itineraryId;
    @JsonProperty("issuerName")
    private String issuerName;
    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("propertySummary")
    private PropertySummary propertySummary;
    @JsonProperty("checkInDate")
    private String checkInDate;
    @JsonProperty("checkOutDate")
    private String checkOutDate;
    @JsonProperty("rooms")
    private List<Room> rooms = new ArrayList<Room>();
    @JsonProperty("totalChargeableRate")
    private TotalChargeableRate totalChargeableRate;
    @JsonProperty("chargeableRateInfo")
    private Object chargeableRateInfo;
    @JsonProperty("additionalRates")
    private List<Object> additionalRates = new ArrayList<Object>();
    @JsonProperty("convertedTotalChargeableRate")
    private ConvertedTotalChargeableRate convertedTotalChargeableRate;
    @JsonProperty("surchargeTotal")
    private SurchargeTotal surchargeTotal;
    @JsonProperty("convertedSurchargeTotal")
    private ConvertedSurchargeTotal convertedSurchargeTotal;

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public HotelBook withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("bookingId")
    public String getBookingId() {
        return bookingId;
    }

    @JsonProperty("bookingId")
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public HotelBook withBookingId(String bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    @JsonProperty("itineraryId")
    public String getItineraryId() {
        return itineraryId;
    }

    @JsonProperty("itineraryId")
    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    public HotelBook withItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
        return this;
    }

    @JsonProperty("issuerName")
    public String getIssuerName() {
        return issuerName;
    }

    @JsonProperty("issuerName")
    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public HotelBook withIssuerName(String issuerName) {
        this.issuerName = issuerName;
        return this;
    }

    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public HotelBook withPropertyId(String propertyId) {
        this.propertyId = propertyId;
        return this;
    }

    @JsonProperty("propertySummary")
    public PropertySummary getPropertySummary() {
        return propertySummary;
    }

    @JsonProperty("propertySummary")
    public void setPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
    }

    public HotelBook withPropertySummary(PropertySummary propertySummary) {
        this.propertySummary = propertySummary;
        return this;
    }

    @JsonProperty("checkInDate")
    public String getCheckInDate() {
        return checkInDate;
    }

    @JsonProperty("checkInDate")
    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public HotelBook withCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    @JsonProperty("checkOutDate")
    public String getCheckOutDate() {
        return checkOutDate;
    }

    @JsonProperty("checkOutDate")
    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public HotelBook withCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    @JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public HotelBook withRooms(List<Room> rooms) {
        this.rooms = rooms;
        return this;
    }

    @JsonProperty("totalChargeableRate")
    public TotalChargeableRate getTotalChargeableRate() {
        return totalChargeableRate;
    }

    @JsonProperty("totalChargeableRate")
    public void setTotalChargeableRate(TotalChargeableRate totalChargeableRate) {
        this.totalChargeableRate = totalChargeableRate;
    }

    public HotelBook withTotalChargeableRate(TotalChargeableRate totalChargeableRate) {
        this.totalChargeableRate = totalChargeableRate;
        return this;
    }

    @JsonProperty("chargeableRateInfo")
    public Object getChargeableRateInfo() {
        return chargeableRateInfo;
    }

    @JsonProperty("chargeableRateInfo")
    public void setChargeableRateInfo(Object chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
    }

    public HotelBook withChargeableRateInfo(Object chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
        return this;
    }

    @JsonProperty("additionalRates")
    public List<Object> getAdditionalRates() {
        return additionalRates;
    }

    @JsonProperty("additionalRates")
    public void setAdditionalRates(List<Object> additionalRates) {
        this.additionalRates = additionalRates;
    }

    public HotelBook withAdditionalRates(List<Object> additionalRates) {
        this.additionalRates = additionalRates;
        return this;
    }

    @JsonProperty("convertedTotalChargeableRate")
    public ConvertedTotalChargeableRate getConvertedTotalChargeableRate() {
        return convertedTotalChargeableRate;
    }

    @JsonProperty("convertedTotalChargeableRate")
    public void setConvertedTotalChargeableRate(ConvertedTotalChargeableRate convertedTotalChargeableRate) {
        this.convertedTotalChargeableRate = convertedTotalChargeableRate;
    }

    public HotelBook withConvertedTotalChargeableRate(ConvertedTotalChargeableRate convertedTotalChargeableRate) {
        this.convertedTotalChargeableRate = convertedTotalChargeableRate;
        return this;
    }

    @JsonProperty("surchargeTotal")
    public SurchargeTotal getSurchargeTotal() {
        return surchargeTotal;
    }

    @JsonProperty("surchargeTotal")
    public void setSurchargeTotal(SurchargeTotal surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
    }

    public HotelBook withSurchargeTotal(SurchargeTotal surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
        return this;
    }

    @JsonProperty("convertedSurchargeTotal")
    public ConvertedSurchargeTotal getConvertedSurchargeTotal() {
        return convertedSurchargeTotal;
    }

    @JsonProperty("convertedSurchargeTotal")
    public void setConvertedSurchargeTotal(ConvertedSurchargeTotal convertedSurchargeTotal) {
        this.convertedSurchargeTotal = convertedSurchargeTotal;
    }

    public HotelBook withConvertedSurchargeTotal(ConvertedSurchargeTotal convertedSurchargeTotal) {
        this.convertedSurchargeTotal = convertedSurchargeTotal;
        return this;
    }

}
