
package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "roomId",
    "rateKey",
    "numOfAdult",
    "guests"
})
public class Room {

    @JsonProperty("roomId")
    private String roomId;
    @JsonProperty("rateKey")
    private String rateKey;
    @JsonProperty("numOfAdult")
    private String numOfAdult;
    @JsonProperty("numOfAdults")
    private String numOfAdults;
    @JsonProperty("guests")
    private List<Guest> guests;

    @JsonProperty("roomId")
    public String getRoomId() {
        return roomId;
    }

    @JsonProperty("roomId")
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Room withRoomId(String roomId) {
        this.roomId = roomId;
        return this;
    }

    @JsonProperty("rateKey")
    public String getRateKey() {
        return rateKey;
    }

    @JsonProperty("rateKey")
    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public Room withRateKey(String rateKey) {
        this.rateKey = rateKey;
        return this;
    }

    @JsonProperty("numOfAdult")
    public String getNumOfAdult() {
        return numOfAdult;
    }

    @JsonProperty("numOfAdult")
    public void setNumOfAdult(String numOfAdult) {
        this.numOfAdult = numOfAdult;
    }

    public Room withNumOfAdult(String numOfAdult) {
        this.numOfAdult = numOfAdult;
        return this;
    }

    @JsonProperty("guests")
    public List<Guest> getGuests() {
        return guests;
    }

    @JsonProperty("guests")
    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public Room withGuests(List<Guest> guests) {
        this.guests = guests;
        return this;
    }

	public String getNumOfAdults() {
		return numOfAdults;
	}

	public void setNumOfAdults(String numOfAdults) {
		this.numOfAdults = numOfAdults;
	}

}
