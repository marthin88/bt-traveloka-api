package com.ocbcnisp.traveloka.model.accomodation.request;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "propertyId",
        "checkInDate",
        "checkOutDate",
        "rateTypes",
        "roomGroup",
        "metadata"
})
public class PreBookReq {

    @JsonProperty("propertyId")
    private String propertyId;
    @JsonProperty("checkInDate")
    private String checkInDate;
    @JsonProperty("checkOutDate")
    private String checkOutDate;
    @JsonProperty("rateTypes")
    private List<String> rateTypes = null;
    @JsonProperty("roomGroup")
    private RoomGroup roomGroup;
    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PreBookReq(String guestNumber, String roomId, String rateKey) {
    	String rate = "PAY_NOW";
		this.rateTypes = Arrays.asList(rate);
		this.roomGroup = new RoomGroup();
		Room room = new Room();
		room.setNumOfAdults(guestNumber);
		List<Room> rooms = Arrays.asList(room);
		this.roomGroup.setRooms(rooms);
		this.roomGroup.setRoomId(roomId);
		this.roomGroup.setRateKey(rateKey);
	}
    public PreBookReq() {}
    @JsonProperty("propertyId")
    public String getPropertyId() {
        return propertyId;
    }

    @JsonProperty("propertyId")
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    @JsonProperty("checkInDate")
    public String getCheckInDate() {
        return checkInDate;
    }

    @JsonProperty("checkInDate")
    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    @JsonProperty("checkOutDate")
    public String getCheckOutDate() {
        return checkOutDate;
    }

    @JsonProperty("checkOutDate")
    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @JsonProperty("rateTypes")
    public List<String> getRateTypes() {
        return rateTypes;
    }

    @JsonProperty("rateTypes")
    public void setRateTypes(List<String> rateTypes) {
        this.rateTypes = rateTypes;
    }

    @JsonProperty("roomGroup")
    public RoomGroup getRoomGroup() {
        return roomGroup;
    }

    @JsonProperty("roomGroup")
    public void setRoomGroup(RoomGroup roomGroup) {
        this.roomGroup = roomGroup;
    }

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}