
package com.ocbcnisp.traveloka.model.accomodation.request;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "customerSessionId",
    "customerIpAddress",
    "customerUserAgent",
    "locale",
    "currencyCode",
    "clientInterface"
})
public class Metadata {

    @JsonProperty("customerSessionId")
    private String customerSessionId;
    @JsonProperty("customerIpAddress")
    private String customerIpAddress;
    @JsonProperty("customerUserAgent")
    private String customerUserAgent;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("clientInterface")
    private String clientInterface;

    public Metadata(String currencyCode, String locale) throws UnknownHostException {
		this.locale = locale;
		this.currencyCode = currencyCode;
		this.clientInterface = "desktop";
		this.customerUserAgent = "api-ocbc";
		this.customerIpAddress = InetAddress.getLocalHost().toString();
		NISPADUserDetails userDetail = (NISPADUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		this.customerSessionId = userDetail.getSession();
	}
    public Metadata() {
	}
    @JsonProperty("customerSessionId")
    public String getCustomerSessionId() {
        return customerSessionId;
    }

    @JsonProperty("customerSessionId")
    public void setCustomerSessionId(String customerSessionId) {
        this.customerSessionId = customerSessionId;
    }

    public Metadata withCustomerSessionId(String customerSessionId) {
        this.customerSessionId = customerSessionId;
        return this;
    }

    @JsonProperty("customerIpAddress")
    public String getCustomerIpAddress() {
        return customerIpAddress;
    }

    @JsonProperty("customerIpAddress")
    public void setCustomerIpAddress(String customerIpAddress) {
        this.customerIpAddress = customerIpAddress;
    }

    public Metadata withCustomerIpAddress(String customerIpAddress) {
        this.customerIpAddress = customerIpAddress;
        return this;
    }

    @JsonProperty("customerUserAgent")
    public String getCustomerUserAgent() {
        return customerUserAgent;
    }

    @JsonProperty("customerUserAgent")
    public void setCustomerUserAgent(String customerUserAgent) {
        this.customerUserAgent = customerUserAgent;
    }

    public Metadata withCustomerUserAgent(String customerUserAgent) {
        this.customerUserAgent = customerUserAgent;
        return this;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Metadata withLocale(String locale) {
        this.locale = locale;
        return this;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Metadata withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    @JsonProperty("clientInterface")
    public String getClientInterface() {
        return clientInterface;
    }

    @JsonProperty("clientInterface")
    public void setClientInterface(String clientInterface) {
        this.clientInterface = clientInterface;
    }

    public Metadata withClientInterface(String clientInterface) {
        this.clientInterface = clientInterface;
        return this;
    }

}
