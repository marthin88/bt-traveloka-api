
package com.ocbcnisp.traveloka.model.auth;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "refresh_token_expires_in",
    "originating_environment",
    "api_product_list",
    "api_product_list_json",
    "organization_name",
    "developer.email",
    "token_type",
    "issued_at",
    "client_id",
    "access_token",
    "application_name",
    "scope",
    "expires_in",
    "refresh_count",
    "status"
})
public class Auth {

    @JsonProperty("refresh_token_expires_in")
    private String refreshTokenExpiresIn;
    @JsonProperty("originating_environment")
    private String originatingEnvironment;
    @JsonProperty("api_product_list")
    private String apiProductList;
    @JsonProperty("api_product_list_json")
    private List<String> apiProductListJson = new ArrayList<String>();
    @JsonProperty("organization_name")
    private String organizationName;
    @JsonProperty("developer.email")
    private String developerEmail;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("issued_at")
    private String issuedAt;
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("application_name")
    private String applicationName;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("expires_in")
    private String expiresIn;
    @JsonProperty("refresh_count")
    private String refreshCount;
    @JsonProperty("status")
    private String status;

    @JsonProperty("refresh_token_expires_in")
    public String getRefreshTokenExpiresIn() {
        return refreshTokenExpiresIn;
    }

    @JsonProperty("refresh_token_expires_in")
    public void setRefreshTokenExpiresIn(String refreshTokenExpiresIn) {
        this.refreshTokenExpiresIn = refreshTokenExpiresIn;
    }

    public Auth withRefreshTokenExpiresIn(String refreshTokenExpiresIn) {
        this.refreshTokenExpiresIn = refreshTokenExpiresIn;
        return this;
    }

    @JsonProperty("originating_environment")
    public String getOriginatingEnvironment() {
        return originatingEnvironment;
    }

    @JsonProperty("originating_environment")
    public void setOriginatingEnvironment(String originatingEnvironment) {
        this.originatingEnvironment = originatingEnvironment;
    }

    public Auth withOriginatingEnvironment(String originatingEnvironment) {
        this.originatingEnvironment = originatingEnvironment;
        return this;
    }

    @JsonProperty("api_product_list")
    public String getApiProductList() {
        return apiProductList;
    }

    @JsonProperty("api_product_list")
    public void setApiProductList(String apiProductList) {
        this.apiProductList = apiProductList;
    }

    public Auth withApiProductList(String apiProductList) {
        this.apiProductList = apiProductList;
        return this;
    }

    @JsonProperty("api_product_list_json")
    public List<String> getApiProductListJson() {
        return apiProductListJson;
    }

    @JsonProperty("api_product_list_json")
    public void setApiProductListJson(List<String> apiProductListJson) {
        this.apiProductListJson = apiProductListJson;
    }

    public Auth withApiProductListJson(List<String> apiProductListJson) {
        this.apiProductListJson = apiProductListJson;
        return this;
    }

    @JsonProperty("organization_name")
    public String getOrganizationName() {
        return organizationName;
    }

    @JsonProperty("organization_name")
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Auth withOrganizationName(String organizationName) {
        this.organizationName = organizationName;
        return this;
    }

    @JsonProperty("developer.email")
    public String getDeveloperEmail() {
        return developerEmail;
    }

    @JsonProperty("developer.email")
    public void setDeveloperEmail(String developerEmail) {
        this.developerEmail = developerEmail;
    }

    public Auth withDeveloperEmail(String developerEmail) {
        this.developerEmail = developerEmail;
        return this;
    }

    @JsonProperty("token_type")
    public String getTokenType() {
        return tokenType;
    }

    @JsonProperty("token_type")
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Auth withTokenType(String tokenType) {
        this.tokenType = tokenType;
        return this;
    }

    @JsonProperty("issued_at")
    public String getIssuedAt() {
        return issuedAt;
    }

    @JsonProperty("issued_at")
    public void setIssuedAt(String issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Auth withIssuedAt(String issuedAt) {
        this.issuedAt = issuedAt;
        return this;
    }

    @JsonProperty("client_id")
    public String getClientId() {
        return clientId;
    }

    @JsonProperty("client_id")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Auth withClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    @JsonProperty("access_token")
    public String getAccessToken() {
        return accessToken;
    }

    @JsonProperty("access_token")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Auth withAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    @JsonProperty("application_name")
    public String getApplicationName() {
        return applicationName;
    }

    @JsonProperty("application_name")
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Auth withApplicationName(String applicationName) {
        this.applicationName = applicationName;
        return this;
    }

    @JsonProperty("scope")
    public String getScope() {
        return scope;
    }

    @JsonProperty("scope")
    public void setScope(String scope) {
        this.scope = scope;
    }

    public Auth withScope(String scope) {
        this.scope = scope;
        return this;
    }

    @JsonProperty("expires_in")
    public String getExpiresIn() {
        return expiresIn;
    }

    @JsonProperty("expires_in")
    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Auth withExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
        return this;
    }

    @JsonProperty("refresh_count")
    public String getRefreshCount() {
        return refreshCount;
    }

    @JsonProperty("refresh_count")
    public void setRefreshCount(String refreshCount) {
        this.refreshCount = refreshCount;
    }

    public Auth withRefreshCount(String refreshCount) {
        this.refreshCount = refreshCount;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public Auth withStatus(String status) {
        this.status = status;
        return this;
    }

}
