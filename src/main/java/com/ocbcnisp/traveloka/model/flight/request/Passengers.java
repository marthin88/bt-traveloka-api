package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adult",
    "child",
    "infant"
})
public class Passengers {

    @JsonProperty("adult")
    private String adult;
    @JsonProperty("child")
    private String child;
    @JsonProperty("infant")
    private String infant;

    @JsonProperty("adult")
    public String getAdult() {
        return adult;
    }

    @JsonProperty("adult")
    public void setAdult(String adult) {
        this.adult = adult;
    }

    public Passengers withAdult(String adult) {
        this.adult = adult;
        return this;
    }

    @JsonProperty("child")
    public String getChild() {
        return child;
    }

    @JsonProperty("child")
    public void setChild(String child) {
        this.child = child;
    }

    public Passengers withChild(String child) {
        this.child = child;
        return this;
    }

    @JsonProperty("infant")
    public String getInfant() {
        return infant;
    }

    @JsonProperty("infant")
    public void setInfant(String infant) {
        this.infant = infant;
    }

    public Passengers withInfant(String infant) {
        this.infant = infant;
        return this;
    }

}
