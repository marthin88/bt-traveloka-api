
package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "email",
    "firstName",
    "lastName",
    "phoneNumber",
    "phoneNumberCountryCode",
    "customerEmail",
    "customerPhoneNumber",
    "customerPhoneNumberCountryCode"
})
public class ContactDetail {

    @JsonProperty("email")
    private String email;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("phoneNumberCountryCode")
    private String phoneNumberCountryCode;
    @JsonProperty("customerEmail")
    private String customerEmail;
    @JsonProperty("customerPhoneNumber")
    private String customerPhoneNumber;
    @JsonProperty("customerPhoneNumberCountryCode")
    private String customerPhoneNumberCountryCode;

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public ContactDetail withEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public ContactDetail withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ContactDetail withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phoneNumber")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ContactDetail withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    @JsonProperty("phoneNumberCountryCode")
    public String getPhoneNumberCountryCode() {
        return phoneNumberCountryCode;
    }

    @JsonProperty("phoneNumberCountryCode")
    public void setPhoneNumberCountryCode(String phoneNumberCountryCode) {
        this.phoneNumberCountryCode = phoneNumberCountryCode;
    }

    public ContactDetail withPhoneNumberCountryCode(String phoneNumberCountryCode) {
        this.phoneNumberCountryCode = phoneNumberCountryCode;
        return this;
    }

    @JsonProperty("customerEmail")
    public String getCustomerEmail() {
        return customerEmail;
    }

    @JsonProperty("customerEmail")
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public ContactDetail withCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
        return this;
    }

    @JsonProperty("customerPhoneNumber")
    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    @JsonProperty("customerPhoneNumber")
    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public ContactDetail withCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
        return this;
    }

    @JsonProperty("customerPhoneNumberCountryCode")
    public String getCustomerPhoneNumberCountryCode() {
        return customerPhoneNumberCountryCode;
    }

    @JsonProperty("customerPhoneNumberCountryCode")
    public void setCustomerPhoneNumberCountryCode(String customerPhoneNumberCountryCode) {
        this.customerPhoneNumberCountryCode = customerPhoneNumberCountryCode;
    }

    public ContactDetail withCustomerPhoneNumberCountryCode(String customerPhoneNumberCountryCode) {
        this.customerPhoneNumberCountryCode = customerPhoneNumberCountryCode;
        return this;
    }

}
