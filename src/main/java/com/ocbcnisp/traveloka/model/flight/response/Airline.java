
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "airlineCode",
    "airlineName",
    "logoUrl"
})
public class Airline {

    @JsonProperty("airlineCode")
    private String airlineCode;
    @JsonProperty("airlineName")
    private String airlineName;
    @JsonProperty("logoUrl")
    private String logoUrl;

    @JsonProperty("airlineCode")
    public String getAirlineCode() {
        return airlineCode;
    }

    @JsonProperty("airlineCode")
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public Airline withAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
        return this;
    }

    @JsonProperty("airlineName")
    public String getAirlineName() {
        return airlineName;
    }

    @JsonProperty("airlineName")
    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public Airline withAirlineName(String airlineName) {
        this.airlineName = airlineName;
        return this;
    }

    @JsonProperty("logoUrl")
    public String getLogoUrl() {
        return logoUrl;
    }

    @JsonProperty("logoUrl")
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Airline withLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
        return this;
    }

}
