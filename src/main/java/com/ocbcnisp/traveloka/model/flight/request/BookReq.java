
package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "contactDetail",
    "passengers",
    "flightIds",
    "journeyType",
    "locale",
    "loginID",
    "loginType",
    "customerLoginID",
    "customerLoginType"
})
public class BookReq {

    @JsonProperty("contactDetail")
    private ContactDetail contactDetail;
    @JsonProperty("passengers")
    private PassengersDetail passengers;
    @JsonProperty("flightIds")
    private List<String> flightIds = new ArrayList<String>();
    @JsonProperty("journeyType")
    private String journeyType;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("loginID")
    private String loginID;
    @JsonProperty("loginType")
    private String loginType;
    @JsonProperty("customerLoginID")
    private String customerLoginID;
    @JsonProperty("customerLoginType")
    private String customerLoginType;

    @JsonProperty("contactDetail")
    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    @JsonProperty("contactDetail")
    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }

    public BookReq withContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
        return this;
    }

    @JsonProperty("passengers")
    public PassengersDetail getPassengers() {
        return passengers;
    }

    @JsonProperty("passengers")
    public void setPassengers(PassengersDetail passengers) {
        this.passengers = passengers;
    }

    public BookReq withPassengers(PassengersDetail passengers) {
        this.passengers = passengers;
        return this;
    }

    @JsonProperty("flightIds")
    public List<String> getFlightIds() {
        return flightIds;
    }

    @JsonProperty("flightIds")
    public void setFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
    }

    public BookReq withFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
        return this;
    }

    @JsonProperty("journeyType")
    public String getJourneyType() {
        return journeyType;
    }

    @JsonProperty("journeyType")
    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public BookReq withJourneyType(String journeyType) {
        this.journeyType = journeyType;
        return this;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    public BookReq withLocale(String locale) {
        this.locale = locale;
        return this;
    }

    @JsonProperty("loginID")
    public String getLoginID() {
        return loginID;
    }

    @JsonProperty("loginID")
    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    public BookReq withLoginID(String loginID) {
        this.loginID = loginID;
        return this;
    }

    @JsonProperty("loginType")
    public String getLoginType() {
        return loginType;
    }

    @JsonProperty("loginType")
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public BookReq withLoginType(String loginType) {
        this.loginType = loginType;
        return this;
    }

    @JsonProperty("customerLoginID")
    public String getCustomerLoginID() {
        return customerLoginID;
    }

    @JsonProperty("customerLoginID")
    public void setCustomerLoginID(String customerLoginID) {
        this.customerLoginID = customerLoginID;
    }

    public BookReq withCustomerLoginID(String customerLoginID) {
        this.customerLoginID = customerLoginID;
        return this;
    }

    @JsonProperty("customerLoginType")
    public String getCustomerLoginType() {
        return customerLoginType;
    }

    @JsonProperty("customerLoginType")
    public void setCustomerLoginType(String customerLoginType) {
        this.customerLoginType = customerLoginType;
    }

    public BookReq withCustomerLoginType(String customerLoginType) {
        this.customerLoginType = customerLoginType;
        return this;
    }

}
