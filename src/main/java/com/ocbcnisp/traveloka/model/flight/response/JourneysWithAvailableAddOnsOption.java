
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segmentsWithAvailableAddOns",
    "availableAddOnsOptions"
})
public class JourneysWithAvailableAddOnsOption {

    @JsonProperty("segmentsWithAvailableAddOns")
    private List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns = new ArrayList<SegmentsWithAvailableAddOn>();
    @JsonProperty("availableAddOnsOptions")
    private Object availableAddOnsOptions;

    @JsonProperty("segmentsWithAvailableAddOns")
    public List<SegmentsWithAvailableAddOn> getSegmentsWithAvailableAddOns() {
        return segmentsWithAvailableAddOns;
    }

    @JsonProperty("segmentsWithAvailableAddOns")
    public void setSegmentsWithAvailableAddOns(List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
    }

    public JourneysWithAvailableAddOnsOption withSegmentsWithAvailableAddOns(List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
        return this;
    }

    @JsonProperty("availableAddOnsOptions")
    public Object getAvailableAddOnsOptions() {
        return availableAddOnsOptions;
    }

    @JsonProperty("availableAddOnsOptions")
    public void setAvailableAddOnsOptions(Object availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
    }

    public JourneysWithAvailableAddOnsOption withAvailableAddOnsOptions(Object availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
        return this;
    }

}
