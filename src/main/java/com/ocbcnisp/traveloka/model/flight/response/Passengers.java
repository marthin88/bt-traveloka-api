
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adults",
    "children",
    "infants"
})
public class Passengers {

    @JsonProperty("adults")
    private List<Adult> adults = new ArrayList<Adult>();
    @JsonProperty("children")
    private List<Object> children = new ArrayList<Object>();
    @JsonProperty("infants")
    private List<Object> infants = new ArrayList<Object>();

    @JsonProperty("adults")
    public List<Adult> getAdults() {
        return adults;
    }

    @JsonProperty("adults")
    public void setAdults(List<Adult> adults) {
        this.adults = adults;
    }

    public Passengers withAdults(List<Adult> adults) {
        this.adults = adults;
        return this;
    }

    @JsonProperty("children")
    public List<Object> getChildren() {
        return children;
    }

    @JsonProperty("children")
    public void setChildren(List<Object> children) {
        this.children = children;
    }

    public Passengers withChildren(List<Object> children) {
        this.children = children;
        return this;
    }

    @JsonProperty("infants")
    public List<Object> getInfants() {
        return infants;
    }

    @JsonProperty("infants")
    public void setInfants(List<Object> infants) {
        this.infants = infants;
    }

    public Passengers withInfants(List<Object> infants) {
        this.infants = infants;
        return this;
    }

}
