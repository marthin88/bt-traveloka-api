
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ocbcnisp.traveloka.model.flight.response.FareWithCurrency;
import com.ocbcnisp.traveloka.model.flight.response.Journey;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "contactDetail",
    "passengers",
    "locale",
    "status",
    "issuanceFailedReason",
    "fareDetail",
    "journeys",
    "grandTotalFareWithCurrency",
    "bookingTime",
    "netToAgent"
})
public class FlightBookingDetail {

    @JsonProperty("contactDetail")
    private ContactDetail contactDetail;
    @JsonProperty("passengers")
    private Passengers passengers;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("status")
    private String status;
    @JsonProperty("issuanceFailedReason")
    private Object issuanceFailedReason;
    @JsonProperty("fareDetail")
    private FareDetail fareDetail;
    @JsonProperty("journeys")
    private List<Journey> journeys = new ArrayList<Journey>();
    @JsonProperty("grandTotalFareWithCurrency")
    private FareWithCurrency grandTotalFareWithCurrency;
    @JsonProperty("bookingTime")
    private String bookingTime;
    @JsonProperty("netToAgent")
    private FareWithCurrency netToAgent;

    @JsonProperty("contactDetail")
    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    @JsonProperty("contactDetail")
    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }

    public FlightBookingDetail withContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
        return this;
    }

    @JsonProperty("passengers")
    public Passengers getPassengers() {
        return passengers;
    }

    @JsonProperty("passengers")
    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }

    public FlightBookingDetail withPassengers(Passengers passengers) {
        this.passengers = passengers;
        return this;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    public FlightBookingDetail withLocale(String locale) {
        this.locale = locale;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public FlightBookingDetail withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("issuanceFailedReason")
    public Object getIssuanceFailedReason() {
        return issuanceFailedReason;
    }

    @JsonProperty("issuanceFailedReason")
    public void setIssuanceFailedReason(Object issuanceFailedReason) {
        this.issuanceFailedReason = issuanceFailedReason;
    }

    public FlightBookingDetail withIssuanceFailedReason(Object issuanceFailedReason) {
        this.issuanceFailedReason = issuanceFailedReason;
        return this;
    }

    @JsonProperty("fareDetail")
    public FareDetail getFareDetail() {
        return fareDetail;
    }

    @JsonProperty("fareDetail")
    public void setFareDetail(FareDetail fareDetail) {
        this.fareDetail = fareDetail;
    }

    public FlightBookingDetail withFareDetail(FareDetail fareDetail) {
        this.fareDetail = fareDetail;
        return this;
    }

    @JsonProperty("journeys")
    public List<Journey> getJourneys() {
        return journeys;
    }

    @JsonProperty("journeys")
    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
    }

    public FlightBookingDetail withJourneys(List<Journey> journeys) {
        this.journeys = journeys;
        return this;
    }

    @JsonProperty("grandTotalFareWithCurrency")
    public FareWithCurrency getGrandTotalFareWithCurrency() {
        return grandTotalFareWithCurrency;
    }

    @JsonProperty("grandTotalFareWithCurrency")
    public void setGrandTotalFareWithCurrency(FareWithCurrency grandTotalFareWithCurrency) {
        this.grandTotalFareWithCurrency = grandTotalFareWithCurrency;
    }

    public FlightBookingDetail withGrandTotalFareWithCurrency(FareWithCurrency grandTotalFareWithCurrency) {
        this.grandTotalFareWithCurrency = grandTotalFareWithCurrency;
        return this;
    }

    @JsonProperty("bookingTime")
    public String getBookingTime() {
        return bookingTime;
    }

    @JsonProperty("bookingTime")
    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public FlightBookingDetail withBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
        return this;
    }

    @JsonProperty("netToAgent")
    public FareWithCurrency getNetToAgent() {
        return netToAgent;
    }

    @JsonProperty("netToAgent")
    public void setNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
    }

    public FlightBookingDetail withNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
        return this;
    }

}
