
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "feeType",
    "amountWithCurrency",
    "percentage",
    "flightRefundFeeLevelType"
})
public class RefundFee {

    @JsonProperty("feeType")
    private String feeType;
    @JsonProperty("amountWithCurrency")
    private FareWithCurrency amountWithCurrency;
    @JsonProperty("percentage")
    private String percentage;
    @JsonProperty("flightRefundFeeLevelType")
    private Object flightRefundFeeLevelType;

    @JsonProperty("feeType")
    public String getFeeType() {
        return feeType;
    }

    @JsonProperty("feeType")
    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public RefundFee withFeeType(String feeType) {
        this.feeType = feeType;
        return this;
    }

    @JsonProperty("amountWithCurrency")
    public Object getAmountWithCurrency() {
        return amountWithCurrency;
    }

    @JsonProperty("amountWithCurrency")
    public void setAmountWithCurrency(FareWithCurrency amountWithCurrency) {
        this.amountWithCurrency = amountWithCurrency;
    }

    public RefundFee withAmountWithCurrency(FareWithCurrency amountWithCurrency) {
        this.amountWithCurrency = amountWithCurrency;
        return this;
    }

    @JsonProperty("percentage")
    public String getPercentage() {
        return percentage;
    }

    @JsonProperty("percentage")
    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public RefundFee withPercentage(String percentage) {
        this.percentage = percentage;
        return this;
    }

    @JsonProperty("flightRefundFeeLevelType")
    public Object getFlightRefundFeeLevelType() {
        return flightRefundFeeLevelType;
    }

    @JsonProperty("flightRefundFeeLevelType")
    public void setFlightRefundFeeLevelType(Object flightRefundFeeLevelType) {
        this.flightRefundFeeLevelType = flightRefundFeeLevelType;
    }

    public RefundFee withFlightRefundFeeLevelType(Object flightRefundFeeLevelType) {
        this.flightRefundFeeLevelType = flightRefundFeeLevelType;
        return this;
    }

}
