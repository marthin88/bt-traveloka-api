package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "depAirportOrAreaCode",
    "arrAirportOrAreaCode",
    "depDate",
    "seatClass"
})
public class Journey {

    @JsonProperty("depAirportOrAreaCode")
    private String depAirportOrAreaCode;
    @JsonProperty("arrAirportOrAreaCode")
    private String arrAirportOrAreaCode;
    @JsonProperty("depDate")
    private String depDate;
    @JsonProperty("seatClass")
    private String seatClass;

    @JsonProperty("depAirportOrAreaCode")
    public String getDepAirportOrAreaCode() {
        return depAirportOrAreaCode;
    }

    @JsonProperty("depAirportOrAreaCode")
    public void setDepAirportOrAreaCode(String depAirportOrAreaCode) {
        this.depAirportOrAreaCode = depAirportOrAreaCode;
    }

    public Journey withDepAirportOrAreaCode(String depAirportOrAreaCode) {
        this.depAirportOrAreaCode = depAirportOrAreaCode;
        return this;
    }

    @JsonProperty("arrAirportOrAreaCode")
    public String getArrAirportOrAreaCode() {
        return arrAirportOrAreaCode;
    }

    @JsonProperty("arrAirportOrAreaCode")
    public void setArrAirportOrAreaCode(String arrAirportOrAreaCode) {
        this.arrAirportOrAreaCode = arrAirportOrAreaCode;
    }

    public Journey withArrAirportOrAreaCode(String arrAirportOrAreaCode) {
        this.arrAirportOrAreaCode = arrAirportOrAreaCode;
        return this;
    }

    @JsonProperty("depDate")
    public String getDepDate() {
        return depDate;
    }

    @JsonProperty("depDate")
    public void setDepDate(String depDate) {
        this.depDate = depDate;
    }

    public Journey withDepDate(String depDate) {
        this.depDate = depDate;
        return this;
    }

    @JsonProperty("seatClass")
    public String getSeatClass() {
        return seatClass;
    }

    @JsonProperty("seatClass")
    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public Journey withSeatClass(String seatClass) {
        this.seatClass = seatClass;
        return this;
    }

}
