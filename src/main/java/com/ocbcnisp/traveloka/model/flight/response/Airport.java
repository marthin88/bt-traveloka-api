
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "airportCode",
    "city",
    "countryCode",
    "areaCode",
    "timeZone",
    "internationalAirportName",
    "airportIcaoCode",
    "localAirportName",
    "localCityName"
})
public class Airport {

    @JsonProperty("airportCode")
    private String airportCode;
    @JsonProperty("city")
    private String city;
    @JsonProperty("countryCode")
    private String countryCode;
    @JsonProperty("areaCode")
    private String areaCode;
    @JsonProperty("timeZone")
    private String timeZone;
    @JsonProperty("internationalAirportName")
    private String internationalAirportName;
    @JsonProperty("airportIcaoCode")
    private String airportIcaoCode;
    @JsonProperty("localAirportName")
    private String localAirportName;
    @JsonProperty("localCityName")
    private String localCityName;

    @JsonProperty("airportCode")
    public String getAirportCode() {
        return airportCode;
    }

    @JsonProperty("airportCode")
    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public Airport withAirportCode(String airportCode) {
        this.airportCode = airportCode;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public Airport withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("countryCode")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("countryCode")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Airport withCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    @JsonProperty("areaCode")
    public String getAreaCode() {
        return areaCode;
    }

    @JsonProperty("areaCode")
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Airport withAreaCode(String areaCode) {
        this.areaCode = areaCode;
        return this;
    }

    @JsonProperty("timeZone")
    public String getTimeZone() {
        return timeZone;
    }

    @JsonProperty("timeZone")
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Airport withTimeZone(String timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    @JsonProperty("internationalAirportName")
    public String getInternationalAirportName() {
        return internationalAirportName;
    }

    @JsonProperty("internationalAirportName")
    public void setInternationalAirportName(String internationalAirportName) {
        this.internationalAirportName = internationalAirportName;
    }

    public Airport withInternationalAirportName(String internationalAirportName) {
        this.internationalAirportName = internationalAirportName;
        return this;
    }

    @JsonProperty("airportIcaoCode")
    public String getAirportIcaoCode() {
        return airportIcaoCode;
    }

    @JsonProperty("airportIcaoCode")
    public void setAirportIcaoCode(String airportIcaoCode) {
        this.airportIcaoCode = airportIcaoCode;
    }

    public Airport withAirportIcaoCode(String airportIcaoCode) {
        this.airportIcaoCode = airportIcaoCode;
        return this;
    }

    @JsonProperty("localAirportName")
    public String getLocalAirportName() {
        return localAirportName;
    }

    @JsonProperty("localAirportName")
    public void setLocalAirportName(String localAirportName) {
        this.localAirportName = localAirportName;
    }

    public Airport withLocalAirportName(String localAirportName) {
        this.localAirportName = localAirportName;
        return this;
    }

    @JsonProperty("localCityName")
    public String getLocalCityName() {
        return localCityName;
    }

    @JsonProperty("localCityName")
    public void setLocalCityName(String localCityName) {
        this.localCityName = localCityName;
    }

    public Airport withLocalCityName(String localCityName) {
        this.localCityName = localCityName;
        return this;
    }

}
