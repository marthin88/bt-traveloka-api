
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "baseFareWithCurrency",
    "vatWithCurrency",
    "pscWithCurrency",
    "fuelSurchargeWithCurrency",
    "adminFeeWithCurrency",
    "additionalFeeWithCurrency",
    "totalFareWithCurrency"
})
public class AdultFare {

    @JsonProperty("baseFareWithCurrency")
    private FareWithCurrency baseFareWithCurrency;
    @JsonProperty("vatWithCurrency")
    private FareWithCurrency vatWithCurrency;
    @JsonProperty("pscWithCurrency")
    private FareWithCurrency pscWithCurrency;
    @JsonProperty("fuelSurchargeWithCurrency")
    private DepartureDetail fuelSurchargeWithCurrency;
    @JsonProperty("adminFeeWithCurrency")
    private FareWithCurrency adminFeeWithCurrency;
    @JsonProperty("additionalFeeWithCurrency")
    private FareWithCurrency additionalFeeWithCurrency;
    @JsonProperty("totalFareWithCurrency")
    private FareWithCurrency totalFareWithCurrency;

    @JsonProperty("baseFareWithCurrency")
    public FareWithCurrency getBaseFareWithCurrency() {
        return baseFareWithCurrency;
    }

    @JsonProperty("baseFareWithCurrency")
    public void setBaseFareWithCurrency(FareWithCurrency baseFareWithCurrency) {
        this.baseFareWithCurrency = baseFareWithCurrency;
    }

    public AdultFare withBaseFareWithCurrency(FareWithCurrency baseFareWithCurrency) {
        this.baseFareWithCurrency = baseFareWithCurrency;
        return this;
    }

    @JsonProperty("vatWithCurrency")
    public FareWithCurrency getVatWithCurrency() {
        return vatWithCurrency;
    }

    @JsonProperty("vatWithCurrency")
    public void setVatWithCurrency(FareWithCurrency vatWithCurrency) {
        this.vatWithCurrency = vatWithCurrency;
    }

    public AdultFare withVatWithCurrency(FareWithCurrency vatWithCurrency) {
        this.vatWithCurrency = vatWithCurrency;
        return this;
    }

    @JsonProperty("pscWithCurrency")
    public FareWithCurrency getPscWithCurrency() {
        return pscWithCurrency;
    }

    @JsonProperty("pscWithCurrency")
    public void setPscWithCurrency(FareWithCurrency pscWithCurrency) {
        this.pscWithCurrency = pscWithCurrency;
    }

    public AdultFare withPscWithCurrency(FareWithCurrency pscWithCurrency) {
        this.pscWithCurrency = pscWithCurrency;
        return this;
    }

    @JsonProperty("fuelSurchargeWithCurrency")
    public DepartureDetail getFuelSurchargeWithCurrency() {
        return fuelSurchargeWithCurrency;
    }

    @JsonProperty("fuelSurchargeWithCurrency")
    public void setFuelSurchargeWithCurrency(DepartureDetail fuelSurchargeWithCurrency) {
        this.fuelSurchargeWithCurrency = fuelSurchargeWithCurrency;
    }

    public AdultFare withFuelSurchargeWithCurrency(DepartureDetail fuelSurchargeWithCurrency) {
        this.fuelSurchargeWithCurrency = fuelSurchargeWithCurrency;
        return this;
    }

    @JsonProperty("adminFeeWithCurrency")
    public FareWithCurrency getAdminFeeWithCurrency() {
        return adminFeeWithCurrency;
    }

    @JsonProperty("adminFeeWithCurrency")
    public void setAdminFeeWithCurrency(FareWithCurrency adminFeeWithCurrency) {
        this.adminFeeWithCurrency = adminFeeWithCurrency;
    }

    public AdultFare withAdminFeeWithCurrency(FareWithCurrency adminFeeWithCurrency) {
        this.adminFeeWithCurrency = adminFeeWithCurrency;
        return this;
    }

    @JsonProperty("additionalFeeWithCurrency")
    public FareWithCurrency getAdditionalFeeWithCurrency() {
        return additionalFeeWithCurrency;
    }

    @JsonProperty("additionalFeeWithCurrency")
    public void setAdditionalFeeWithCurrency(FareWithCurrency additionalFeeWithCurrency) {
        this.additionalFeeWithCurrency = additionalFeeWithCurrency;
    }

    public AdultFare withAdditionalFeeWithCurrency(FareWithCurrency additionalFeeWithCurrency) {
        this.additionalFeeWithCurrency = additionalFeeWithCurrency;
        return this;
    }

    @JsonProperty("totalFareWithCurrency")
    public FareWithCurrency getTotalFareWithCurrency() {
        return totalFareWithCurrency;
    }

    @JsonProperty("totalFareWithCurrency")
    public void setTotalFareWithCurrency(FareWithCurrency totalFareWithCurrency) {
        this.totalFareWithCurrency = totalFareWithCurrency;
    }

    public AdultFare withTotalFareWithCurrency(FareWithCurrency totalFareWithCurrency) {
        this.totalFareWithCurrency = totalFareWithCurrency;
        return this;
    }

}
