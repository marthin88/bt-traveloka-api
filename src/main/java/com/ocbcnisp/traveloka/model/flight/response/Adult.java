
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "firstName",
    "lastName",
    "gender",
    "dateOfBirth",
    "documentDetail",
    "nationality",
    "birthLocation",
    "addOns"
})
public class Adult {

    @JsonProperty("title")
    private String title;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("dateOfBirth")
    private String dateOfBirth;
    @JsonProperty("documentDetail")
    private DocumentDetail documentDetail;
    @JsonProperty("nationality")
    private String nationality;
    @JsonProperty("birthLocation")
    private Object birthLocation;
    @JsonProperty("addOns")
    private List<AddOn> addOns = new ArrayList<AddOn>();

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Adult withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Adult withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Adult withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Adult withGender(String gender) {
        this.gender = gender;
        return this;
    }

    @JsonProperty("dateOfBirth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonProperty("dateOfBirth")
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Adult withDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    @JsonProperty("documentDetail")
    public DocumentDetail getDocumentDetail() {
        return documentDetail;
    }

    @JsonProperty("documentDetail")
    public void setDocumentDetail(DocumentDetail documentDetail) {
        this.documentDetail = documentDetail;
    }

    public Adult withDocumentDetail(DocumentDetail documentDetail) {
        this.documentDetail = documentDetail;
        return this;
    }

    @JsonProperty("nationality")
    public String getNationality() {
        return nationality;
    }

    @JsonProperty("nationality")
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Adult withNationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    @JsonProperty("birthLocation")
    public Object getBirthLocation() {
        return birthLocation;
    }

    @JsonProperty("birthLocation")
    public void setBirthLocation(Object birthLocation) {
        this.birthLocation = birthLocation;
    }

    public Adult withBirthLocation(Object birthLocation) {
        this.birthLocation = birthLocation;
        return this;
    }

    @JsonProperty("addOns")
    public List<AddOn> getAddOns() {
        return addOns;
    }

    @JsonProperty("addOns")
    public void setAddOns(List<AddOn> addOns) {
        this.addOns = addOns;
    }

    public Adult withAddOns(List<AddOn> addOns) {
        this.addOns = addOns;
        return this;
    }

}
