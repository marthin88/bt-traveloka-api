
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "airportCode",
    "arrivalDate",
    "arrivalTime",
    "arrivalTerminal"
})
public class ArrivalDetail {

    @JsonProperty("airportCode")
    private String airportCode;
    @JsonProperty("arrivalDate")
    private String arrivalDate;
    @JsonProperty("arrivalTime")
    private String arrivalTime;
    @JsonProperty("arrivalTerminal")
    private String arrivalTerminal;

    @JsonProperty("airportCode")
    public String getAirportCode() {
        return airportCode;
    }

    @JsonProperty("airportCode")
    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public ArrivalDetail withAirportCode(String airportCode) {
        this.airportCode = airportCode;
        return this;
    }

    @JsonProperty("arrivalDate")
    public String getArrivalDate() {
        return arrivalDate;
    }

    @JsonProperty("arrivalDate")
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public ArrivalDetail withArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
        return this;
    }

    @JsonProperty("arrivalTime")
    public String getArrivalTime() {
        return arrivalTime;
    }

    @JsonProperty("arrivalTime")
    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public ArrivalDetail withArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
        return this;
    }

    @JsonProperty("arrivalTerminal")
    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    @JsonProperty("arrivalTerminal")
    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public ArrivalDetail withArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
        return this;
    }

}
