
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segments",
    "departureDetail",
    "arrivalDetail",
    "numOfTransits",
    "journeyDuration",
    "fareInfo",
    "daysOffset",
    "refundableStatus"
})
public class DepartureFlightDetail {

    @JsonProperty("segments")
    private List<Segment> segments = new ArrayList<Segment>();
    @JsonProperty("departureDetail")
    private DepartureDetail departureDetail;
    @JsonProperty("arrivalDetail")
    private ArrivalDetail arrivalDetail;
    @JsonProperty("numOfTransits")
    private String numOfTransits;
    @JsonProperty("journeyDuration")
    private String journeyDuration;
    @JsonProperty("fareInfo")
    private Object fareInfo;
    @JsonProperty("daysOffset")
    private String daysOffset;
    @JsonProperty("refundableStatus")
    private String refundableStatus;

    @JsonProperty("segments")
    public List<Segment> getSegments() {
        return segments;
    }

    @JsonProperty("segments")
    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public DepartureFlightDetail withSegments(List<Segment> segments) {
        this.segments = segments;
        return this;
    }

    @JsonProperty("departureDetail")
    public DepartureDetail getDepartureDetail() {
        return departureDetail;
    }

    @JsonProperty("departureDetail")
    public void setDepartureDetail(DepartureDetail departureDetail) {
        this.departureDetail = departureDetail;
    }

    public DepartureFlightDetail withDepartureDetail(DepartureDetail departureDetail) {
        this.departureDetail = departureDetail;
        return this;
    }

    @JsonProperty("arrivalDetail")
    public ArrivalDetail getArrivalDetail() {
        return arrivalDetail;
    }

    @JsonProperty("arrivalDetail")
    public void setArrivalDetail(ArrivalDetail arrivalDetail) {
        this.arrivalDetail = arrivalDetail;
    }

    public DepartureFlightDetail withArrivalDetail(ArrivalDetail arrivalDetail) {
        this.arrivalDetail = arrivalDetail;
        return this;
    }

    @JsonProperty("numOfTransits")
    public String getNumOfTransits() {
        return numOfTransits;
    }

    @JsonProperty("numOfTransits")
    public void setNumOfTransits(String numOfTransits) {
        this.numOfTransits = numOfTransits;
    }

    public DepartureFlightDetail withNumOfTransits(String numOfTransits) {
        this.numOfTransits = numOfTransits;
        return this;
    }

    @JsonProperty("journeyDuration")
    public String getJourneyDuration() {
        return journeyDuration;
    }

    @JsonProperty("journeyDuration")
    public void setJourneyDuration(String journeyDuration) {
        this.journeyDuration = journeyDuration;
    }

    public DepartureFlightDetail withJourneyDuration(String journeyDuration) {
        this.journeyDuration = journeyDuration;
        return this;
    }

    @JsonProperty("fareInfo")
    public Object getFareInfo() {
        return fareInfo;
    }

    @JsonProperty("fareInfo")
    public void setFareInfo(Object fareInfo) {
        this.fareInfo = fareInfo;
    }

    public DepartureFlightDetail withFareInfo(Object fareInfo) {
        this.fareInfo = fareInfo;
        return this;
    }

    @JsonProperty("daysOffset")
    public String getDaysOffset() {
        return daysOffset;
    }

    @JsonProperty("daysOffset")
    public void setDaysOffset(String daysOffset) {
        this.daysOffset = daysOffset;
    }

    public DepartureFlightDetail withDaysOffset(String daysOffset) {
        this.daysOffset = daysOffset;
        return this;
    }

    @JsonProperty("refundableStatus")
    public String getRefundableStatus() {
        return refundableStatus;
    }

    @JsonProperty("refundableStatus")
    public void setRefundableStatus(String refundableStatus) {
        this.refundableStatus = refundableStatus;
    }

    public DepartureFlightDetail withRefundableStatus(String refundableStatus) {
        this.refundableStatus = refundableStatus;
        return this;
    }

}
