
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "baggageOptions",
    "mealOptions"
})
public class AvailableAddOnsOptions {

    @JsonProperty("baggageOptions")
    private List<BaggageOption> baggageOptions = new ArrayList<BaggageOption>();
    @JsonProperty("mealOptions")
    private List<MealOption> mealOptions = new ArrayList<MealOption>();

    @JsonProperty("baggageOptions")
    public List<BaggageOption> getBaggageOptions() {
        return baggageOptions;
    }

    @JsonProperty("baggageOptions")
    public void setBaggageOptions(List<BaggageOption> baggageOptions) {
        this.baggageOptions = baggageOptions;
    }

    public AvailableAddOnsOptions withBaggageOptions(List<BaggageOption> baggageOptions) {
        this.baggageOptions = baggageOptions;
        return this;
    }

    @JsonProperty("mealOptions")
    public List<MealOption> getMealOptions() {
        return mealOptions;
    }

    @JsonProperty("mealOptions")
    public void setMealOptions(List<MealOption> mealOptions) {
        this.mealOptions = mealOptions;
    }

    public AvailableAddOnsOptions withMealOptions(List<MealOption> mealOptions) {
        this.mealOptions = mealOptions;
        return this;
    }

}
