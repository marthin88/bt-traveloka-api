
package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "baggageQuantity",
    "baggageType",
    "baggageWeight",
    "id",
    "priceWithCurrency"
})
public class BaggageOption {

    @JsonProperty("baggageQuantity")
    private String baggageQuantity;
    @JsonProperty("baggageType")
    private String baggageType;
    @JsonProperty("baggageWeight")
    private String baggageWeight;
    @JsonProperty("id")
    private String id;
    @JsonProperty("priceWithCurrency")
    private PriceWithCurrency priceWithCurrency;

    @JsonProperty("baggageQuantity")
    public String getBaggageQuantity() {
        return baggageQuantity;
    }

    @JsonProperty("baggageQuantity")
    public void setBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
    }

    public BaggageOption withBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
        return this;
    }

    @JsonProperty("baggageType")
    public String getBaggageType() {
        return baggageType;
    }

    @JsonProperty("baggageType")
    public void setBaggageType(String baggageType) {
        this.baggageType = baggageType;
    }

    public BaggageOption withBaggageType(String baggageType) {
        this.baggageType = baggageType;
        return this;
    }

    @JsonProperty("baggageWeight")
    public String getBaggageWeight() {
        return baggageWeight;
    }

    @JsonProperty("baggageWeight")
    public void setBaggageWeight(String baggageWeight) {
        this.baggageWeight = baggageWeight;
    }

    public BaggageOption withBaggageWeight(String baggageWeight) {
        this.baggageWeight = baggageWeight;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public BaggageOption withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("priceWithCurrency")
    public PriceWithCurrency getPriceWithCurrency() {
        return priceWithCurrency;
    }

    @JsonProperty("priceWithCurrency")
    public void setPriceWithCurrency(PriceWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
    }

    public BaggageOption withPriceWithCurrency(PriceWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
        return this;
    }

}
