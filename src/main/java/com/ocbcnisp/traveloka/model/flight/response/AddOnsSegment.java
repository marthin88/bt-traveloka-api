
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sourceAirport",
    "destinationAirport",
    "departureDate",
    "marketingAirline",
    "operatingAirline",
    "seatClass"
})
public class AddOnsSegment {

    @JsonProperty("sourceAirport")
    private String sourceAirport;
    @JsonProperty("destinationAirport")
    private String destinationAirport;
    @JsonProperty("departureDate")
    private String departureDate;
    @JsonProperty("marketingAirline")
    private String marketingAirline;
    @JsonProperty("operatingAirline")
    private String operatingAirline;
    @JsonProperty("seatClass")
    private String seatClass;

    @JsonProperty("sourceAirport")
    public String getSourceAirport() {
        return sourceAirport;
    }

    @JsonProperty("sourceAirport")
    public void setSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public AddOnsSegment withSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
        return this;
    }

    @JsonProperty("destinationAirport")
    public String getDestinationAirport() {
        return destinationAirport;
    }

    @JsonProperty("destinationAirport")
    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public AddOnsSegment withDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
        return this;
    }

    @JsonProperty("departureDate")
    public String getDepartureDate() {
        return departureDate;
    }

    @JsonProperty("departureDate")
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public AddOnsSegment withDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    @JsonProperty("marketingAirline")
    public String getMarketingAirline() {
        return marketingAirline;
    }

    @JsonProperty("marketingAirline")
    public void setMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
    }

    public AddOnsSegment withMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
        return this;
    }

    @JsonProperty("operatingAirline")
    public String getOperatingAirline() {
        return operatingAirline;
    }

    @JsonProperty("operatingAirline")
    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public AddOnsSegment withOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
        return this;
    }

    @JsonProperty("seatClass")
    public String getSeatClass() {
        return seatClass;
    }

    @JsonProperty("seatClass")
    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public AddOnsSegment withSeatClass(String seatClass) {
        this.seatClass = seatClass;
        return this;
    }

}
