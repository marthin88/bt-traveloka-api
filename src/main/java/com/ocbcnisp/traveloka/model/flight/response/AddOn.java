
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ocbcnisp.traveloka.model.flight.response.AvailableAddOnsOptions;
import com.ocbcnisp.traveloka.model.flight.response.SegmentsWithAvailableAddOn;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segmentsWithAvailableAddOns",
    "availableAddOnsOptions"
})
public class AddOn {

    @JsonProperty("segmentsWithAvailableAddOns")
    private List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns;
    @JsonProperty("availableAddOnsOptions")
    private AvailableAddOnsOptions availableAddOnsOptions;

    @JsonProperty("segmentsWithAvailableAddOns")
    public List<SegmentsWithAvailableAddOn> getSegmentsWithAvailableAddOns() {
        return segmentsWithAvailableAddOns;
    }

    @JsonProperty("segmentsWithAvailableAddOns")
    public void setSegmentsWithAvailableAddOns(List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
    }

    public AddOn withSegmentsWithAvailableAddOns(List<SegmentsWithAvailableAddOn> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
        return this;
    }

    @JsonProperty("availableAddOnsOptions")
    public AvailableAddOnsOptions getAvailableAddOnsOptions() {
        return availableAddOnsOptions;
    }

    @JsonProperty("availableAddOnsOptions")
    public void setAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
    }

    public AddOn withAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
        return this;
    }

}
