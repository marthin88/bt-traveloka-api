
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adultFare",
    "childFare",
    "infantFare"
})
public class NetToAgent {

    @JsonProperty("adultFare")
    private FareWithCurrency adultFare;
    @JsonProperty("childFare")
    private FareWithCurrency childFare;
    @JsonProperty("infantFare")
    private FareWithCurrency infantFare;

    @JsonProperty("adultFare")
    public FareWithCurrency getAdultFare() {
        return adultFare;
    }

    @JsonProperty("adultFare")
    public void setAdultFare(FareWithCurrency adultFare) {
        this.adultFare = adultFare;
    }

    public NetToAgent withAdultFare(FareWithCurrency adultFare) {
        this.adultFare = adultFare;
        return this;
    }

    @JsonProperty("childFare")
    public FareWithCurrency getChildFare() {
        return childFare;
    }

    @JsonProperty("childFare")
    public void setChildFare(FareWithCurrency childFare) {
        this.childFare = childFare;
    }

    public NetToAgent withChildFare(FareWithCurrency childFare) {
        this.childFare = childFare;
        return this;
    }

    @JsonProperty("infantFare")
    public FareWithCurrency getInfantFare() {
        return infantFare;
    }

    @JsonProperty("infantFare")
    public void setInfantFare(FareWithCurrency infantFare) {
        this.infantFare = infantFare;
    }

    public NetToAgent withInfantFare(FareWithCurrency infantFare) {
        this.infantFare = infantFare;
        return this;
    }

}
