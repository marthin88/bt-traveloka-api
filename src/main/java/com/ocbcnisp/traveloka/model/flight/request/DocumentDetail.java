
package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "issuingCountry",
    "documentNo",
    "expirationDate",
    "documentType"
})
public class DocumentDetail {

    @JsonProperty("issuingCountry")
    private String issuingCountry;
    @JsonProperty("documentNo")
    private String documentNo;
    @JsonProperty("expirationDate")
    private String expirationDate;
    @JsonProperty("documentType")
    private String documentType;

    @JsonProperty("issuingCountry")
    public String getIssuingCountry() {
        return issuingCountry;
    }

    @JsonProperty("issuingCountry")
    public void setIssuingCountry(String issuingCountry) {
        this.issuingCountry = issuingCountry;
    }

    public DocumentDetail withIssuingCountry(String issuingCountry) {
        this.issuingCountry = issuingCountry;
        return this;
    }

    @JsonProperty("documentNo")
    public String getDocumentNo() {
        return documentNo;
    }

    @JsonProperty("documentNo")
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public DocumentDetail withDocumentNo(String documentNo) {
        this.documentNo = documentNo;
        return this;
    }

    @JsonProperty("expirationDate")
    public String getExpirationDate() {
        return expirationDate;
    }

    @JsonProperty("expirationDate")
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public DocumentDetail withExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    @JsonProperty("documentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("documentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public DocumentDetail withDocumentType(String documentType) {
        this.documentType = documentType;
        return this;
    }

}
