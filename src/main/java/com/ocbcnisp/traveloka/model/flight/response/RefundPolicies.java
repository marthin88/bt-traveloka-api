
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "refundPolicy",
    "refundHalfUsed"
})
public class RefundPolicies {

    @JsonProperty("refundPolicy")
    private List<RefundPolicyDetail> refundPolicy = new ArrayList<RefundPolicyDetail>();
    @JsonProperty("refundHalfUsed")
    private Object refundHalfUsed;

    @JsonProperty("refundPolicy")
    public List<RefundPolicyDetail> getRefundPolicy() {
        return refundPolicy;
    }

    @JsonProperty("refundPolicy")
    public void setRefundPolicy(List<RefundPolicyDetail> refundPolicy) {
        this.refundPolicy = refundPolicy;
    }

    public RefundPolicies withRefundPolicy(List<RefundPolicyDetail> refundPolicy) {
        this.refundPolicy = refundPolicy;
        return this;
    }

    @JsonProperty("refundHalfUsed")
    public Object getRefundHalfUsed() {
        return refundHalfUsed;
    }

    @JsonProperty("refundHalfUsed")
    public void setRefundHalfUsed(Object refundHalfUsed) {
        this.refundHalfUsed = refundHalfUsed;
    }

    public RefundPolicies withRefundHalfUsed(Object refundHalfUsed) {
        this.refundHalfUsed = refundHalfUsed;
        return this;
    }

}
