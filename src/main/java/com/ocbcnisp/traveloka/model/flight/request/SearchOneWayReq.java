
package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "journey",
    "passengers"
})
public class SearchOneWayReq {

    @JsonProperty("journey")
    private Journey journey;
    @JsonProperty("passengers")
    private Passengers passengers;

    @JsonProperty("journey")
    public Journey getJourney() {
        return journey;
    }

    @JsonProperty("journey")
    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public SearchOneWayReq withJourney(Journey journey) {
        this.journey = journey;
        return this;
    }

    @JsonProperty("passengers")
    public Passengers getPassengers() {
        return passengers;
    }

    @JsonProperty("passengers")
    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }

    public SearchOneWayReq withPassengers(Passengers passengers) {
        this.passengers = passengers;
        return this;
    }

}
