
package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segmentsWithAvailableAddOns",
    "availableAddOnsOptions"
})
public class AddOn {

    @JsonProperty("segmentsWithAvailableAddOns")
    private List<Object> segmentsWithAvailableAddOns = new ArrayList<Object>();
    @JsonProperty("availableAddOnsOptions")
    private AvailableAddOnsOptions availableAddOnsOptions;

    @JsonProperty("segmentsWithAvailableAddOns")
    public List<Object> getSegmentsWithAvailableAddOns() {
        return segmentsWithAvailableAddOns;
    }

    @JsonProperty("segmentsWithAvailableAddOns")
    public void setSegmentsWithAvailableAddOns(List<Object> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
    }

    public AddOn withSegmentsWithAvailableAddOns(List<Object> segmentsWithAvailableAddOns) {
        this.segmentsWithAvailableAddOns = segmentsWithAvailableAddOns;
        return this;
    }

    @JsonProperty("availableAddOnsOptions")
    public AvailableAddOnsOptions getAvailableAddOnsOptions() {
        return availableAddOnsOptions;
    }

    @JsonProperty("availableAddOnsOptions")
    public void setAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
    }

    public AddOn withAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
        return this;
    }

}
