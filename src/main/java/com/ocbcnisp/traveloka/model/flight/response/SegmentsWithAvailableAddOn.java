
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ocbcnisp.traveloka.model.flight.response.AvailableAddOnsOptions;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segment",
    "availableAddOnsOptions"
})
public class SegmentsWithAvailableAddOn {

    @JsonProperty("segment")
    private AddOnsSegment segment;
    @JsonProperty("availableAddOnsOptions")
    private AvailableAddOnsOptions availableAddOnsOptions;

    @JsonProperty("segment")
    public AddOnsSegment getSegment() {
        return segment;
    }

    @JsonProperty("segment")
    public void setSegment(AddOnsSegment segment) {
        this.segment = segment;
    }

    public SegmentsWithAvailableAddOn withSegment(AddOnsSegment segment) {
        this.segment = segment;
        return this;
    }

    @JsonProperty("availableAddOnsOptions")
    public AvailableAddOnsOptions getAvailableAddOnsOptions() {
        return availableAddOnsOptions;
    }

    @JsonProperty("availableAddOnsOptions")
    public void setAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
    }

    public SegmentsWithAvailableAddOn withAvailableAddOnsOptions(AvailableAddOnsOptions availableAddOnsOptions) {
        this.availableAddOnsOptions = availableAddOnsOptions;
        return this;
    }

}
