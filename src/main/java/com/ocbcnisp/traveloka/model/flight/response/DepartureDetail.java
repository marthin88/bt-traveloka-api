
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "airportCode",
    "departureDate",
    "departureTime",
    "departureTerminal"
})
public class DepartureDetail {

    @JsonProperty("airportCode")
    private String airportCode;
    @JsonProperty("departureDate")
    private String departureDate;
    @JsonProperty("departureTime")
    private String departureTime;
    @JsonProperty("departureTerminal")
    private String departureTerminal;

    @JsonProperty("airportCode")
    public String getAirportCode() {
        return airportCode;
    }

    @JsonProperty("airportCode")
    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public DepartureDetail withAirportCode(String airportCode) {
        this.airportCode = airportCode;
        return this;
    }

    @JsonProperty("departureDate")
    public String getDepartureDate() {
        return departureDate;
    }

    @JsonProperty("departureDate")
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public DepartureDetail withDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    @JsonProperty("departureTime")
    public String getDepartureTime() {
        return departureTime;
    }

    @JsonProperty("departureTime")
    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public DepartureDetail withDepartureTime(String departureTime) {
        this.departureTime = departureTime;
        return this;
    }

    @JsonProperty("departureTerminal")
    public String getDepartureTerminal() {
        return departureTerminal;
    }

    @JsonProperty("departureTerminal")
    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public DepartureDetail withDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
        return this;
    }

}
