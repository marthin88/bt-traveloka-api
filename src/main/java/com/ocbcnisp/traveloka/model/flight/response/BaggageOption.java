
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "priceWithCurrency",
    "netToAgent",
    "id",
    "baggageType",
    "baggageQuantity",
    "baggageWeight"
})
public class BaggageOption {

    @JsonProperty("priceWithCurrency")
    private FareWithCurrency priceWithCurrency;
    @JsonProperty("netToAgent")
    private FareWithCurrency netToAgent;
    @JsonProperty("id")
    private String id;
    @JsonProperty("baggageType")
    private String baggageType;
    @JsonProperty("baggageQuantity")
    private String baggageQuantity;
    @JsonProperty("baggageWeight")
    private String baggageWeight;

    @JsonProperty("priceWithCurrency")
    public FareWithCurrency getPriceWithCurrency() {
        return priceWithCurrency;
    }

    @JsonProperty("priceWithCurrency")
    public void setPriceWithCurrency(FareWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
    }

    public BaggageOption withPriceWithCurrency(FareWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
        return this;
    }

    @JsonProperty("netToAgent")
    public FareWithCurrency getNetToAgent() {
        return netToAgent;
    }

    @JsonProperty("netToAgent")
    public void setNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
    }

    public BaggageOption withNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public BaggageOption withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("baggageType")
    public String getBaggageType() {
        return baggageType;
    }

    @JsonProperty("baggageType")
    public void setBaggageType(String baggageType) {
        this.baggageType = baggageType;
    }

    public BaggageOption withBaggageType(String baggageType) {
        this.baggageType = baggageType;
        return this;
    }

    @JsonProperty("baggageQuantity")
    public String getBaggageQuantity() {
        return baggageQuantity;
    }

    @JsonProperty("baggageQuantity")
    public void setBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
    }

    public BaggageOption withBaggageQuantity(String baggageQuantity) {
        this.baggageQuantity = baggageQuantity;
        return this;
    }

    @JsonProperty("baggageWeight")
    public String getBaggageWeight() {
        return baggageWeight;
    }

    @JsonProperty("baggageWeight")
    public void setBaggageWeight(String baggageWeight) {
        this.baggageWeight = baggageWeight;
    }

    public BaggageOption withBaggageWeight(String baggageWeight) {
        this.baggageWeight = baggageWeight;
        return this;
    }

}
