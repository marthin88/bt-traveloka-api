
package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "journeyType",
    "flightIds"
})
public class RefundPolicyReq {

    @JsonProperty("journeyType")
    private String journeyType;
    @JsonProperty("flightIds")
    private List<String> flightIds = new ArrayList<String>();

    @JsonProperty("journeyType")
    public String getJourneyType() {
        return journeyType;
    }

    @JsonProperty("journeyType")
    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public RefundPolicyReq withJourneyType(String journeyType) {
        this.journeyType = journeyType;
        return this;
    }

    @JsonProperty("flightIds")
    public List<String> getFlightIds() {
        return flightIds;
    }

    @JsonProperty("flightIds")
    public void setFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
    }

    public RefundPolicyReq withFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
        return this;
    }

}
