
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "periodType",
    "numHours"
})
public class StartPeriod {

    @JsonProperty("periodType")
    private String periodType;
    @JsonProperty("numHours")
    private String numHours;

    @JsonProperty("periodType")
    public String getPeriodType() {
        return periodType;
    }

    @JsonProperty("periodType")
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public StartPeriod withPeriodType(String periodType) {
        this.periodType = periodType;
        return this;
    }

    @JsonProperty("numHours")
    public String getNumHours() {
        return numHours;
    }

    @JsonProperty("numHours")
    public void setNumHours(String numHours) {
        this.numHours = numHours;
    }

    public StartPeriod withNumHours(String numHours) {
        this.numHours = numHours;
        return this;
    }

}
