
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "success",
    "errorMessage",
    "data"
})
public class SearchRoundTrip {

    @JsonProperty("success")
    private Boolean success;
    @JsonProperty("errorMessage")
    private Object errorMessage;
    @JsonProperty("data")
    private Data data;

    @JsonProperty("success")
    public Boolean getSuccess() {
        return success;
    }

    @JsonProperty("success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public SearchRoundTrip withSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    @JsonProperty("errorMessage")
    public Object getErrorMessage() {
        return errorMessage;
    }

    @JsonProperty("errorMessage")
    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public SearchRoundTrip withErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    public SearchRoundTrip withData(Data data) {
        this.data = data;
        return this;
    }

}
