
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "flightCode",
    "marketingAirline",
    "brandAirline",
    "operatingAirline",
    "subClass",
    "seatClass",
    "flightDurationInMinutes",
    "transitDurationInMinutes",
    "departureDetail",
    "arrivalDetail",
    "stopInfo",
    "addOns",
    "fareBasisCode"
})
public class Segment {

	@JsonProperty("flightCode")
    private String flightCode;
    @JsonProperty("marketingAirline")
    private String marketingAirline;
    @JsonProperty("brandAirline")
    private String brandAirline;
    @JsonProperty("operatingAirline")
    private String operatingAirline;
    @JsonProperty("subClass")
    private String subClass;
    @JsonProperty("seatClass")
    private String seatClass;
    @JsonProperty("flightDurationInMinutes")
    private String flightDurationInMinutes;
    @JsonProperty("transitDurationInMinutes")
    private String transitDurationInMinutes;
    @JsonProperty("departureDetail")
    private DepartureDetail departureDetail;
    @JsonProperty("arrivalDetail")
    private ArrivalDetail arrivalDetail;
    @JsonProperty("stopInfo")
    private Object stopInfo;
    @JsonProperty("addOns")
    private AvailableAddOnsOptions addOns;
    @JsonProperty("fareBasisCode")
    private String fareBasisCode;

    @JsonProperty("flightCode")
    public String getFlightCode() {
        return flightCode;
    }

    @JsonProperty("flightCode")
    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public Segment withFlightCode(String flightCode) {
        this.flightCode = flightCode;
        return this;
    }

    @JsonProperty("marketingAirline")
    public String getMarketingAirline() {
        return marketingAirline;
    }

    @JsonProperty("marketingAirline")
    public void setMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
    }

    public Segment withMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
        return this;
    }

    @JsonProperty("brandAirline")
    public String getBrandAirline() {
        return brandAirline;
    }

    @JsonProperty("brandAirline")
    public void setBrandAirline(String brandAirline) {
        this.brandAirline = brandAirline;
    }

    public Segment withBrandAirline(String brandAirline) {
        this.brandAirline = brandAirline;
        return this;
    }

    @JsonProperty("operatingAirline")
    public String getOperatingAirline() {
        return operatingAirline;
    }

    @JsonProperty("operatingAirline")
    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public Segment withOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
        return this;
    }

    @JsonProperty("subClass")
    public String getSubClass() {
        return subClass;
    }

    @JsonProperty("subClass")
    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public Segment withSubClass(String subClass) {
        this.subClass = subClass;
        return this;
    }

    @JsonProperty("seatClass")
    public String getSeatClass() {
        return seatClass;
    }

    @JsonProperty("seatClass")
    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public Segment withSeatClass(String seatClass) {
        this.seatClass = seatClass;
        return this;
    }

    @JsonProperty("flightDurationInMinutes")
    public String getFlightDurationInMinutes() {
        return flightDurationInMinutes;
    }

    @JsonProperty("flightDurationInMinutes")
    public void setFlightDurationInMinutes(String flightDurationInMinutes) {
        this.flightDurationInMinutes = flightDurationInMinutes;
    }

    public Segment withFlightDurationInMinutes(String flightDurationInMinutes) {
        this.flightDurationInMinutes = flightDurationInMinutes;
        return this;
    }

    @JsonProperty("transitDurationInMinutes")
    public Object getTransitDurationInMinutes() {
        return transitDurationInMinutes;
    }

    @JsonProperty("transitDurationInMinutes")
    public void setTransitDurationInMinutes(String transitDurationInMinutes) {
        this.transitDurationInMinutes = transitDurationInMinutes;
    }

    public Segment withTransitDurationInMinutes(String transitDurationInMinutes) {
        this.transitDurationInMinutes = transitDurationInMinutes;
        return this;
    }

    @JsonProperty("departureDetail")
    public DepartureDetail getDepartureDetail() {
        return departureDetail;
    }

    @JsonProperty("departureDetail")
    public void setDepartureDetail(DepartureDetail departureDetail) {
        this.departureDetail = departureDetail;
    }

    public Segment withDepartureDetail(DepartureDetail departureDetail) {
        this.departureDetail = departureDetail;
        return this;
    }

    @JsonProperty("arrivalDetail")
    public ArrivalDetail getArrivalDetail() {
        return arrivalDetail;
    }

    @JsonProperty("arrivalDetail")
    public void setArrivalDetail(ArrivalDetail arrivalDetail) {
        this.arrivalDetail = arrivalDetail;
    }

    public Segment withArrivalDetail(ArrivalDetail arrivalDetail) {
        this.arrivalDetail = arrivalDetail;
        return this;
    }

    @JsonProperty("stopInfo")
    public Object getStopInfo() {
        return stopInfo;
    }

    @JsonProperty("stopInfo")
    public void setStopInfo(Object stopInfo) {
        this.stopInfo = stopInfo;
    }

    public Segment withStopInfo(Object stopInfo) {
        this.stopInfo = stopInfo;
        return this;
    }

    @JsonProperty("addOns")
    public AvailableAddOnsOptions getAddOns() {
        return addOns;
    }

    @JsonProperty("addOns")
    public void setAddOns(AvailableAddOnsOptions addOns) {
        this.addOns = addOns;
    }

    public Segment withAddOns(AvailableAddOnsOptions addOns) {
        this.addOns = addOns;
        return this;
    }

    @JsonProperty("fareBasisCode")
    public Object getFareBasisCode() {
        return fareBasisCode;
    }

    @JsonProperty("fareBasisCode")
    public void setFareBasisCode(String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }

    public Segment withFareBasisCode(String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
        return this;
    }

}
