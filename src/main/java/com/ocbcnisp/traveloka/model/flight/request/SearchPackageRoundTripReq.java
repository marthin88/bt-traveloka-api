package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "journeys",
    "passengers"
})
public class SearchPackageRoundTripReq {

    @JsonProperty("journeys")
    private List<Journey> journeys = new ArrayList<Journey>();
    @JsonProperty("passengers")
    private Passengers passengers;

    @JsonProperty("journeys")
    public List<Journey> getJourneys() {
        return journeys;
    }

    @JsonProperty("journeys")
    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
    }

    public SearchPackageRoundTripReq withJourneys(List<Journey> journeys) {
        this.journeys = journeys;
        return this;
    }

    @JsonProperty("passengers")
    public Passengers getPassengers() {
        return passengers;
    }

    @JsonProperty("passengers")
    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }

    public SearchPackageRoundTripReq withPassengers(Passengers passengers) {
        this.passengers = passengers;
        return this;
    }

}
