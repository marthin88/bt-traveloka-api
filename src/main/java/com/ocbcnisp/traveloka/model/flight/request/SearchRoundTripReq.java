package com.ocbcnisp.traveloka.model.flight.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchRoundTripReq {

	@JsonProperty("journey")
    private Journey journey;
    @JsonProperty("passengers")
    private Passengers passengers;

    @JsonProperty("journey")
    public Journey getJourney() {
        return journey;
    }

    @JsonProperty("journey")
    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public SearchRoundTripReq withJourney(Journey journey) {
        this.journey = journey;
        return this;
    }

    @JsonProperty("passengers")
    public Passengers getPassengers() {
        return passengers;
    }

    @JsonProperty("passengers")
    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }

    public SearchRoundTripReq withPassengers(Passengers passengers) {
        this.passengers = passengers;
        return this;
    }
    
}
