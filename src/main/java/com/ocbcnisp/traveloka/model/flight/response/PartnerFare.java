
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adultFare",
    "childFare",
    "infantFare"
})
public class PartnerFare {

    @JsonProperty("adultFare")
    private AdultFare adultFare;
    @JsonProperty("childFare")
    private ChildFare childFare;
    @JsonProperty("infantFare")
    private InfantFare infantFare;

    @JsonProperty("adultFare")
    public AdultFare getAdultFare() {
        return adultFare;
    }

    @JsonProperty("adultFare")
    public void setAdultFare(AdultFare adultFare) {
        this.adultFare = adultFare;
    }

    public PartnerFare withAdultFare(AdultFare adultFare) {
        this.adultFare = adultFare;
        return this;
    }

    @JsonProperty("childFare")
    public ChildFare getChildFare() {
        return childFare;
    }

    @JsonProperty("childFare")
    public void setChildFare(ChildFare childFare) {
        this.childFare = childFare;
    }

    public PartnerFare withChildFare(ChildFare childFare) {
        this.childFare = childFare;
        return this;
    }

    @JsonProperty("infantFare")
    public InfantFare getInfantFare() {
        return infantFare;
    }

    @JsonProperty("infantFare")
    public void setInfantFare(InfantFare infantFare) {
        this.infantFare = infantFare;
    }

    public PartnerFare withInfantFare(InfantFare infantFare) {
        this.infantFare = infantFare;
        return this;
    }

}
