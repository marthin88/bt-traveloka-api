package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddOnsReq {

	@JsonProperty("journeyType")
    private String journeyType;
    @JsonProperty("flightIds")
    private List<String> flightIds = new ArrayList<String>();

    @JsonProperty("journeyType")
    public String getJourneyType() {
        return journeyType;
    }

    @JsonProperty("journeyType")
    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public AddOnsReq withJourneyType(String journeyType) {
        this.journeyType = journeyType;
        return this;
    }

    @JsonProperty("flightIds")
    public List<String> getFlightIds() {
        return flightIds;
    }

    @JsonProperty("flightIds")
    public void setFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
    }

    public AddOnsReq withFlightIds(List<String> flightIds) {
        this.flightIds = flightIds;
        return this;
    }
}
