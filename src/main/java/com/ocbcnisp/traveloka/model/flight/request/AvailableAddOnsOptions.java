
package com.ocbcnisp.traveloka.model.flight.request;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "baggageOptions",
    "mealOptions"
})
public class AvailableAddOnsOptions {

    @JsonProperty("baggageOptions")
    private List<BaggageOption> baggageOptions = new ArrayList<BaggageOption>();
    @JsonProperty("mealOptions")
    private List<Object> mealOptions = new ArrayList<Object>();

    @JsonProperty("baggageOptions")
    public List<BaggageOption> getBaggageOptions() {
        return baggageOptions;
    }

    @JsonProperty("baggageOptions")
    public void setBaggageOptions(List<BaggageOption> baggageOptions) {
        this.baggageOptions = baggageOptions;
    }

    public AvailableAddOnsOptions withBaggageOptions(List<BaggageOption> baggageOptions) {
        this.baggageOptions = baggageOptions;
        return this;
    }

    @JsonProperty("mealOptions")
    public List<Object> getMealOptions() {
        return mealOptions;
    }

    @JsonProperty("mealOptions")
    public void setMealOptions(List<Object> mealOptions) {
        this.mealOptions = mealOptions;
    }

    public AvailableAddOnsOptions withMealOptions(List<Object> mealOptions) {
        this.mealOptions = mealOptions;
        return this;
    }

}
