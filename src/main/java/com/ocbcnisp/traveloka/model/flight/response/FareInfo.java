
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "partnerFare",
    "airlineFare",
    "netToAgent"
})
public class FareInfo {

    @JsonProperty("partnerFare")
    private PartnerFare partnerFare;
    @JsonProperty("airlineFare")
    private AirlineFare airlineFare;
    @JsonProperty("netToAgent")
    private NetToAgent netToAgent;

    @JsonProperty("partnerFare")
    public PartnerFare getPartnerFare() {
        return partnerFare;
    }

    @JsonProperty("partnerFare")
    public void setPartnerFare(PartnerFare partnerFare) {
        this.partnerFare = partnerFare;
    }

    public FareInfo withPartnerFare(PartnerFare partnerFare) {
        this.partnerFare = partnerFare;
        return this;
    }

    @JsonProperty("airlineFare")
    public AirlineFare getAirlineFare() {
        return airlineFare;
    }

    @JsonProperty("airlineFare")
    public void setAirlineFare(AirlineFare airlineFare) {
        this.airlineFare = airlineFare;
    }

    public FareInfo withAirlineFare(AirlineFare airlineFare) {
        this.airlineFare = airlineFare;
        return this;
    }

    @JsonProperty("netToAgent")
    public NetToAgent getNetToAgent() {
        return netToAgent;
    }

    @JsonProperty("netToAgent")
    public void setNetToAgent(NetToAgent netToAgent) {
        this.netToAgent = netToAgent;
    }

    public FareInfo withNetToAgent(NetToAgent netToAgent) {
        this.netToAgent = netToAgent;
        return this;
    }

}
