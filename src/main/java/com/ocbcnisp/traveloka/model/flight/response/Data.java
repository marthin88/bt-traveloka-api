
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ocbcnisp.traveloka.model.flight.response.FlightBookingDetail;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {

    @JsonProperty("airports")
    private List<Airport> airports = null;
    
    @JsonProperty("airlines")
    private List<Airline> airlines = null;
    
    @JsonProperty("oneWayFlightSearchResults")
    private List<OneWayFlightSearchResult> oneWayFlightSearchResults = null;
    
    @JsonProperty("basicRoundTripFlightSearchResults")
    private List<BasicRoundTripFlightSearchResult> basicRoundTripFlightSearchResults = null;
    
    @JsonProperty("departureFlightDetail")
    private List<DepartureFlightDetail> departureFlightDetail = null;
    
    @JsonProperty("requiresBirthDate")
    private Boolean requiresBirthDate;
    
    @JsonProperty("requiresNationality")
    private Boolean requiresNationality;
    
    @JsonProperty("requiresId")
    private Boolean requiresId;
    
    @JsonProperty("requiresDocumentNoForInternational")
    private Boolean requiresDocumentNoForInternational;
    
    @JsonProperty("requiresDocumentNoForDomestic")
    private Boolean requiresDocumentNoForDomestic;
    
    @JsonProperty("refundPolicies")
    private List<RefundPolicies> refundPolicies = null;
    
    @JsonProperty("journeysWithAvailableAddOnsOptions")
    private List<JourneysWithAvailableAddOnsOption> journeysWithAvailableAddOnsOptions = null;
    
    @JsonProperty("bookingSubmissionStatus")
    private String bookingSubmissionStatus;
    
    @JsonProperty("bookingFailureReason")
    private Object bookingFailureReason;
    
    @JsonProperty("bookingId")
    private String bookingId;
    
    @JsonProperty("paymentExpirationTime")
    private String paymentExpirationTime;
    
    @JsonProperty("flightBookingDetail")
    private FlightBookingDetail flightBookingDetail;

    @JsonProperty("bookingSubmissionStatus")
    public String getBookingSubmissionStatus() {
        return bookingSubmissionStatus;
    }

    @JsonProperty("bookingSubmissionStatus")
    public void setBookingSubmissionStatus(String bookingSubmissionStatus) {
        this.bookingSubmissionStatus = bookingSubmissionStatus;
    }

    public Data withBookingSubmissionStatus(String bookingSubmissionStatus) {
        this.bookingSubmissionStatus = bookingSubmissionStatus;
        return this;
    }

    @JsonProperty("bookingFailureReason")
    public Object getBookingFailureReason() {
        return bookingFailureReason;
    }

    @JsonProperty("bookingFailureReason")
    public void setBookingFailureReason(Object bookingFailureReason) {
        this.bookingFailureReason = bookingFailureReason;
    }

    public Data withBookingFailureReason(Object bookingFailureReason) {
        this.bookingFailureReason = bookingFailureReason;
        return this;
    }

    @JsonProperty("bookingId")
    public String getBookingId() {
        return bookingId;
    }

    @JsonProperty("bookingId")
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Data withBookingId(String bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    @JsonProperty("paymentExpirationTime")
    public String getPaymentExpirationTime() {
        return paymentExpirationTime;
    }

    @JsonProperty("paymentExpirationTime")
    public void setPaymentExpirationTime(String paymentExpirationTime) {
        this.paymentExpirationTime = paymentExpirationTime;
    }

    public Data withPaymentExpirationTime(String paymentExpirationTime) {
        this.paymentExpirationTime = paymentExpirationTime;
        return this;
    }

    @JsonProperty("flightBookingDetail")
    public FlightBookingDetail getFlightBookingDetail() {
        return flightBookingDetail;
    }

    @JsonProperty("flightBookingDetail")
    public void setFlightBookingDetail(FlightBookingDetail flightBookingDetail) {
        this.flightBookingDetail = flightBookingDetail;
    }

    public Data withFlightBookingDetail(FlightBookingDetail flightBookingDetail) {
        this.flightBookingDetail = flightBookingDetail;
        return this;
    }

    @JsonProperty("journeysWithAvailableAddOnsOptions")
    public List<JourneysWithAvailableAddOnsOption> getJourneysWithAvailableAddOnsOptions() {
        return journeysWithAvailableAddOnsOptions;
    }

    @JsonProperty("journeysWithAvailableAddOnsOptions")
    public void setJourneysWithAvailableAddOnsOptions(List<JourneysWithAvailableAddOnsOption> journeysWithAvailableAddOnsOptions) {
        this.journeysWithAvailableAddOnsOptions = journeysWithAvailableAddOnsOptions;
    }

    public Data withJourneysWithAvailableAddOnsOptions(List<JourneysWithAvailableAddOnsOption> journeysWithAvailableAddOnsOptions) {
        this.journeysWithAvailableAddOnsOptions = journeysWithAvailableAddOnsOptions;
        return this;
    }

    @JsonProperty("refundPolicies")
    public List<RefundPolicies> getRefundPolicies() {
        return refundPolicies;
    }

    @JsonProperty("refundPolicies")
    public void setRefundPolicies(List<RefundPolicies> refundPolicies) {
        this.refundPolicies = refundPolicies;
    }

    public Data withRefundPolicies(List<RefundPolicies> refundPolicies) {
        this.refundPolicies = refundPolicies;
        return this;
    }

    @JsonProperty("requiresBirthDate")
    public Boolean getRequiresBirthDate() {
        return requiresBirthDate;
    }

    @JsonProperty("requiresBirthDate")
    public void setRequiresBirthDate(Boolean requiresBirthDate) {
        this.requiresBirthDate = requiresBirthDate;
    }

    public Data withRequiresBirthDate(Boolean requiresBirthDate) {
        this.requiresBirthDate = requiresBirthDate;
        return this;
    }

    @JsonProperty("requiresNationality")
    public Boolean getRequiresNationality() {
        return requiresNationality;
    }

    @JsonProperty("requiresNationality")
    public void setRequiresNationality(Boolean requiresNationality) {
        this.requiresNationality = requiresNationality;
    }

    public Data withRequiresNationality(Boolean requiresNationality) {
        this.requiresNationality = requiresNationality;
        return this;
    }

    @JsonProperty("requiresId")
    public Boolean getRequiresId() {
        return requiresId;
    }

    @JsonProperty("requiresId")
    public void setRequiresId(Boolean requiresId) {
        this.requiresId = requiresId;
    }

    public Data withRequiresId(Boolean requiresId) {
        this.requiresId = requiresId;
        return this;
    }

    @JsonProperty("requiresDocumentNoForInternational")
    public Boolean getRequiresDocumentNoForInternational() {
        return requiresDocumentNoForInternational;
    }

    @JsonProperty("requiresDocumentNoForInternational")
    public void setRequiresDocumentNoForInternational(Boolean requiresDocumentNoForInternational) {
        this.requiresDocumentNoForInternational = requiresDocumentNoForInternational;
    }

    public Data withRequiresDocumentNoForInternational(Boolean requiresDocumentNoForInternational) {
        this.requiresDocumentNoForInternational = requiresDocumentNoForInternational;
        return this;
    }

    @JsonProperty("requiresDocumentNoForDomestic")
    public Boolean getRequiresDocumentNoForDomestic() {
        return requiresDocumentNoForDomestic;
    }

    @JsonProperty("requiresDocumentNoForDomestic")
    public void setRequiresDocumentNoForDomestic(Boolean requiresDocumentNoForDomestic) {
        this.requiresDocumentNoForDomestic = requiresDocumentNoForDomestic;
    }

    public Data withRequiresDocumentNoForDomestic(Boolean requiresDocumentNoForDomestic) {
        this.requiresDocumentNoForDomestic = requiresDocumentNoForDomestic;
        return this;
    }

    @JsonProperty("departureFlightDetail")
    public List<DepartureFlightDetail> getDepartureFlightDetail() {
        return departureFlightDetail;
    }

    @JsonProperty("departureFlightDetail")
    public void setDepartureFlightDetail(List<DepartureFlightDetail> departureFlightDetail) {
        this.departureFlightDetail = departureFlightDetail;
    }

    public Data withDepartureFlightDetail(List<DepartureFlightDetail> departureFlightDetail) {
        this.departureFlightDetail = departureFlightDetail;
        return this;
    }

    @JsonProperty("basicRoundTripFlightSearchResults")
    public List<BasicRoundTripFlightSearchResult> getBasicRoundTripFlightSearchResults() {
        return basicRoundTripFlightSearchResults;
    }

    @JsonProperty("basicRoundTripFlightSearchResults")
    public void setBasicRoundTripFlightSearchResults(List<BasicRoundTripFlightSearchResult> basicRoundTripFlightSearchResults) {
        this.basicRoundTripFlightSearchResults = basicRoundTripFlightSearchResults;
    }

    public Data withBasicRoundTripFlightSearchResults(List<BasicRoundTripFlightSearchResult> basicRoundTripFlightSearchResults) {
        this.basicRoundTripFlightSearchResults = basicRoundTripFlightSearchResults;
        return this;
    }

    @JsonProperty("airports")
    public List<Airport> getAirports() {
        return airports;
    }

    @JsonProperty("airports")
    public void setAirports(List<Airport> airports) {
        this.airports = airports;
    }

    public Data withAirports(List<Airport> airports) {
        this.airports = airports;
        return this;
    }

    @JsonProperty("airlines")
	public List<Airline> getAirlines() {
		return airlines;
	}

    @JsonProperty("airlines")
	public void setAirlines(List<Airline> airlines) {
		this.airlines = airlines;
	}
    
    public Data withAirlines(List<Airline> airlines) {
        this.airlines = airlines;
        return this;
    }
    
    @JsonProperty("oneWayFlightSearchResults")
    public List<OneWayFlightSearchResult> getOneWayFlightSearchResults() {
        return oneWayFlightSearchResults;
    }

    @JsonProperty("oneWayFlightSearchResults")
    public void setOneWayFlightSearchResults(List<OneWayFlightSearchResult> oneWayFlightSearchResults) {
        this.oneWayFlightSearchResults = oneWayFlightSearchResults;
    }

    public Data withOneWayFlightSearchResults(List<OneWayFlightSearchResult> oneWayFlightSearchResults) {
        this.oneWayFlightSearchResults = oneWayFlightSearchResults;
        return this;
    }

}
