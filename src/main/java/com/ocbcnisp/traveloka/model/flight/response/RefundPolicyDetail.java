
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "startPeriod",
    "endPeriod",
    "refundType",
    "refundFees"
})
public class RefundPolicyDetail {

    @JsonProperty("startPeriod")
    private StartPeriod startPeriod;
    @JsonProperty("endPeriod")
    private EndPeriod endPeriod;
    @JsonProperty("refundType")
    private String refundType;
    @JsonProperty("refundFees")
    private List<RefundFee> refundFees = new ArrayList<RefundFee>();

    @JsonProperty("startPeriod")
    public StartPeriod getStartPeriod() {
        return startPeriod;
    }

    @JsonProperty("startPeriod")
    public void setStartPeriod(StartPeriod startPeriod) {
        this.startPeriod = startPeriod;
    }

    public RefundPolicyDetail withStartPeriod(StartPeriod startPeriod) {
        this.startPeriod = startPeriod;
        return this;
    }

    @JsonProperty("endPeriod")
    public EndPeriod getEndPeriod() {
        return endPeriod;
    }

    @JsonProperty("endPeriod")
    public void setEndPeriod(EndPeriod endPeriod) {
        this.endPeriod = endPeriod;
    }

    public RefundPolicyDetail withEndPeriod(EndPeriod endPeriod) {
        this.endPeriod = endPeriod;
        return this;
    }

    @JsonProperty("refundType")
    public String getRefundType() {
        return refundType;
    }

    @JsonProperty("refundType")
    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public RefundPolicyDetail withRefundType(String refundType) {
        this.refundType = refundType;
        return this;
    }

    @JsonProperty("refundFees")
    public List<RefundFee> getRefundFees() {
        return refundFees;
    }

    @JsonProperty("refundFees")
    public void setRefundFees(List<RefundFee> refundFees) {
        this.refundFees = refundFees;
    }

    public RefundPolicyDetail withRefundFees(List<RefundFee> refundFees) {
        this.refundFees = refundFees;
        return this;
    }

}
