
package com.ocbcnisp.traveloka.model.flight.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "flightId",
    "departureAirport",
    "arrivalAirport",
    "numOfTransits",
    "journeys"
})
public class BasicRoundTripFlightSearchResult {

    @JsonProperty("flightId")
    private String flightId;
    @JsonProperty("departureAirport")
    private String departureAirport;
    @JsonProperty("arrivalAirport")
    private String arrivalAirport;
    @JsonProperty("numOfTransits")
    private String numOfTransits;
    @JsonProperty("journeys")
    private List<Journey> journeys = new ArrayList<Journey>();

    @JsonProperty("flightId")
    public String getFlightId() {
        return flightId;
    }

    @JsonProperty("flightId")
    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public BasicRoundTripFlightSearchResult withFlightId(String flightId) {
        this.flightId = flightId;
        return this;
    }

    @JsonProperty("departureAirport")
    public String getDepartureAirport() {
        return departureAirport;
    }

    @JsonProperty("departureAirport")
    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public BasicRoundTripFlightSearchResult withDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
        return this;
    }

    @JsonProperty("arrivalAirport")
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    @JsonProperty("arrivalAirport")
    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public BasicRoundTripFlightSearchResult withArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
        return this;
    }

    @JsonProperty("numOfTransits")
    public String getNumOfTransits() {
        return numOfTransits;
    }

    @JsonProperty("numOfTransits")
    public void setNumOfTransits(String numOfTransits) {
        this.numOfTransits = numOfTransits;
    }

    public BasicRoundTripFlightSearchResult withNumOfTransits(String numOfTransits) {
        this.numOfTransits = numOfTransits;
        return this;
    }

    @JsonProperty("journeys")
    public List<Journey> getJourneys() {
        return journeys;
    }

    @JsonProperty("journeys")
    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
    }

    public BasicRoundTripFlightSearchResult withJourneys(List<Journey> journeys) {
        this.journeys = journeys;
        return this;
    }

}
