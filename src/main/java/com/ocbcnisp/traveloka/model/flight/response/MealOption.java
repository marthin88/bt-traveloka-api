
package com.ocbcnisp.traveloka.model.flight.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "priceWithCurrency",
    "netToAgent",
    "id",
    "quantity",
    "displayName"
})
public class MealOption {

    @JsonProperty("priceWithCurrency")
    private FareWithCurrency priceWithCurrency;
    @JsonProperty("netToAgent")
    private FareWithCurrency netToAgent;
    @JsonProperty("id")
    private String id;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("displayName")
    private String displayName;

    @JsonProperty("priceWithCurrency")
    public FareWithCurrency getPriceWithCurrency() {
        return priceWithCurrency;
    }

    @JsonProperty("priceWithCurrency")
    public void setPriceWithCurrency(FareWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
    }

    public MealOption withPriceWithCurrency(FareWithCurrency priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
        return this;
    }

    @JsonProperty("netToAgent")
    public FareWithCurrency getNetToAgent() {
        return netToAgent;
    }

    @JsonProperty("netToAgent")
    public void setNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
    }

    public MealOption withNetToAgent(FareWithCurrency netToAgent) {
        this.netToAgent = netToAgent;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public MealOption withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public MealOption withQuantity(String quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public MealOption withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

}
