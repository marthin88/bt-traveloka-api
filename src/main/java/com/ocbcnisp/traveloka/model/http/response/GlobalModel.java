package com.ocbcnisp.traveloka.model.http.response;

public class GlobalModel {

	private String message;
	private Object Data;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return Data;
	}
	public void setData(Object data) {
		Data = data;
	}
}
