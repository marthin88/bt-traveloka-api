package com.ocbcnisp.traveloka.model.http.response;

import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;

public class Token {

	private NISPADUserDetails dataUser;
	private int httpStatus;
	private String type;
	private String token;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public NISPADUserDetails getDataUser() {
		return dataUser;
	}
	public void setDataUser(NISPADUserDetails dataUser) {
		this.dataUser = dataUser;
	}
	public int getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
}
