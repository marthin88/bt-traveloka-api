package com.ocbcnisp.traveloka.model.http.request;

import java.util.List;

import com.ocbcnisp.traveloka.model.accomodation.request.Guest;

public class HBook {

	private List<String> btId;
	private String roomId;
	private String propertyId;
	private String rateKey;
	private List<Guest> guest;
	private String email;
	private String phone;
	private String spesialRequest;
	public String getSpesialRequest() {
		return spesialRequest;
	}
	public void setSpesialRequest(String spesialRequest) {
		this.spesialRequest = spesialRequest;
	}
	public List<String> getBtId() {
		return btId;
	}
	public void setBtId(List<String> btId) {
		this.btId = btId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getRateKey() {
		return rateKey;
	}
	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Guest> getGuest() {
		return guest;
	}
	public void setGuest(List<Guest> guest) {
		this.guest = guest;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
