package com.ocbcnisp.traveloka.model.http.request;

import java.util.List;

public class HPreBook {
	
	private String rateKey;
	private String propertyId;
	private List<String> btId;
	private String roomId;
	public String getRateKey() {
		return rateKey;
	}
	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public List<String> getBtId() {
		return btId;
	}
	public void setBtId(List<String> btId) {
		this.btId = btId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

}
