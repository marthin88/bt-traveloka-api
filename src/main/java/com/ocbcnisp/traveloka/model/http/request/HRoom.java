package com.ocbcnisp.traveloka.model.http.request;

import java.util.List;

public class HRoom {

	private String propertyId;
	private List<String> btId;
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public List<String> getBtId() {
		return btId;
	}
	public void setBtId(List<String> btId) {
		this.btId = btId;
	}
}
