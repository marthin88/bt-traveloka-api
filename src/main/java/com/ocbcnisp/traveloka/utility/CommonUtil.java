package com.ocbcnisp.traveloka.utility;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtil {

	private final static  Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);
       

    // function to parsing XML to object
    public static Object convertStringToObject(Class<?> originClass, String xml) {
        JAXBContext context;
        Unmarshaller u;
        Object obj;
        try {
            context = JAXBContext.newInstance(originClass);
            u = context.createUnmarshaller();
            obj = (Object) u.unmarshal(new StringReader(xml));
        } catch (Exception e) {
        	LOGGER.info(e.getMessage(),e);;
            return null;
        }
        return obj;
    }

}
