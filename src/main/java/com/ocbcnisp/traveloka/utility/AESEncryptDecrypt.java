package com.ocbcnisp.traveloka.utility;

import org.apache.tomcat.util.codec.binary.Base64;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

public class AESEncryptDecrypt {

    private static final String key = "aesEncryptionKey";
    private static final String initVector = "encryptionIntVec";

    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encrypted) {
        byte[] original;
        String resp =null;
        IvParameterSpec iv;
        SecretKeySpec skeySpec;
        Cipher cipher;

        try {
            iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            original = cipher.doFinal(Base64.decodeBase64(encrypted));
            if(original != null || original.length > 0){
                resp = original.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return resp;
    }
}