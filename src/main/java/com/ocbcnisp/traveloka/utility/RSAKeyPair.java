package com.ocbcnisp.traveloka.utility;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Provider;

public class RSAKeyPair {

    public static KeyPair keyPairRSA() {
        KeyPairGenerator generator = null;
        Provider pr = null;
        try {
            generator = KeyPairGenerator.getInstance(RSAConstants.ALGORITHM);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (generator != null) {
            generator.initialize(RSAConstants.ALGORITHM_BITS);
            KeyPair keyPair = generator.genKeyPair();
            return keyPair;
        }

        return null;
    }

    public static KeyPair keyPairRSAPR() {
        KeyPairGenerator generator = null;
        KeyPairGenerator generatorWithPr = null;
        Provider pr = null;
        try {
            generator = KeyPairGenerator.getInstance(RSAConstants.ALGORITHM);
            pr = generator.getProvider();
            pr.get("id");
            pr.put("test","nisp");
            generatorWithPr = KeyPairGenerator.getInstance(RSAConstants.ALGORITHM,pr);
            System.out.println("test");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (generatorWithPr != null) {
            generatorWithPr.initialize(RSAConstants.ALGORITHM_BITS);
            KeyPair keyPair = generatorWithPr.genKeyPair();
            return keyPair;
        }
        return null;
    }
}
