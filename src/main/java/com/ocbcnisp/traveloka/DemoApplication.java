package com.ocbcnisp.traveloka;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.ocbcnisp.traveloka.exception.RestTemplateErrorHandler;

@SpringBootApplication
@EnableAutoConfiguration
public class DemoApplication {

	@Autowired
	Environment env;
	
	@Value("${traveloka.cert.path}")
	private Resource keyStore;
	
	@Value("${traveloka.cert.password}")
	private String keyStorePassword;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate() throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
//		File certFile = new File("."+File.separator+env.getProperty("traveloka.cert.path"));
		KeyStore keystore = KeyStore.getInstance("jks");
		InputStream inputStream = keyStore.getInputStream();
		keystore.load(inputStream, keyStorePassword.toCharArray());
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		SSLContext sslContext = new SSLContextBuilder()
				.loadKeyMaterial(keystore, keyStorePassword.toCharArray())
//				.build();
				.loadTrustMaterial(null,acceptingTrustStrategy).build(); //bypass certificate validation for proxy
//	            .loadTrustMaterial(
//	                    keyStore.getURL(),
//	                    keyStorePassword.toCharArray()).build();
		
		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				sslContext, new String[] {"TLSv1.2"}, null,
				SSLConnectionSocketFactory.getDefaultHostnameVerifier());
//	    HttpClient httpClient = HttpClients.custom()
//	            .setSSLSocketFactory(socketFactory).build();
		HttpClient httpClient = HttpClients.custom()
		            .setSSLSocketFactory(socketFactory).setProxy(new HttpHost("172.21.141.76", 3128)).build(); //kalo pake proxy *karena outsource gabisa internetan:(
	    HttpComponentsClientHttpRequestFactory factory =
	            new HttpComponentsClientHttpRequestFactory(httpClient);
	    RestTemplate restTemplate = new RestTemplate(factory);
//	    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
//	    mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED, MediaType.MULTIPART_FORM_DATA));
//	    restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
	    MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
//	    jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
	    restTemplate.setErrorHandler(new RestTemplateErrorHandler());
	    return restTemplate;
	}
}
