package com.ocbcnisp.traveloka.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocbcnisp.traveloka.entity.GeoLocationEntity;
import com.ocbcnisp.traveloka.repository.IGeoLocationRepository;
import com.ocbcnisp.traveloka.service.IGeoLocationService;

@Service
public class GeoLocationService implements IGeoLocationService {

	@Autowired
	private IGeoLocationRepository repo;
	@Override
	public List<GeoLocationEntity> findByName(String name) {
		return repo.findByName(name);
	}

}
