package com.ocbcnisp.traveloka.service.impl;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.ocbcnisp.traveloka.constants.AppConstants;
import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.BTDetailHarpaEntity;
import com.ocbcnisp.traveloka.entity.ClaimEntity;
import com.ocbcnisp.traveloka.entity.ClaimHarpaEntity;
import com.ocbcnisp.traveloka.entity.GeoLocationEntity;
import com.ocbcnisp.traveloka.entity.HotelBookingEntity;
import com.ocbcnisp.traveloka.exception.AccomodationException;
import com.ocbcnisp.traveloka.model.accomodation.request.CreateBooking;
import com.ocbcnisp.traveloka.model.accomodation.request.ExpectedTotalChargeableRate;
import com.ocbcnisp.traveloka.model.accomodation.request.GeoListReq;
import com.ocbcnisp.traveloka.model.accomodation.request.Metadata;
import com.ocbcnisp.traveloka.model.accomodation.request.PreBookReq;
import com.ocbcnisp.traveloka.model.accomodation.request.PropertyDetailsReq;
import com.ocbcnisp.traveloka.model.accomodation.request.PropertyListReq;
import com.ocbcnisp.traveloka.model.accomodation.request.Room;
import com.ocbcnisp.traveloka.model.accomodation.request.RoomGroup;
import com.ocbcnisp.traveloka.model.accomodation.request.RoomsListReq;
import com.ocbcnisp.traveloka.model.accomodation.request.SearchListReq;
import com.ocbcnisp.traveloka.model.accomodation.response.GeoList;
import com.ocbcnisp.traveloka.model.accomodation.response.HotelBook;
import com.ocbcnisp.traveloka.model.accomodation.response.PreBook;
import com.ocbcnisp.traveloka.model.accomodation.response.PropertyDetails;
import com.ocbcnisp.traveloka.model.accomodation.response.PropertyList;
import com.ocbcnisp.traveloka.model.accomodation.response.RoomsList;
import com.ocbcnisp.traveloka.model.accomodation.response.SearchList;
import com.ocbcnisp.traveloka.model.auth.APIType;
import com.ocbcnisp.traveloka.model.http.request.HBook;
import com.ocbcnisp.traveloka.model.http.request.HRoom;
import com.ocbcnisp.traveloka.model.http.request.Location;
import com.ocbcnisp.traveloka.repository.IGeoLocationRepository;
import com.ocbcnisp.traveloka.repository.IHotelBookingRepository;
import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;
import com.ocbcnisp.traveloka.service.IAccomodationService;
import com.ocbcnisp.traveloka.service.IBTDetailService;
import com.ocbcnisp.traveloka.service.IRestService;
import com.ocbcnisp.traveloka.constants.StarsFilter;

@Service
public class AccomodationService implements IAccomodationService{

	@Autowired
	private IRestService restService;
	
	@Autowired
	private IGeoLocationRepository geoLocationRepo;
	
	@Autowired
	private IBTDetailService btDetailService;
	
	@Autowired
	private IHotelBookingRepository hotelBookRepo;
	
	@Override
	public GeoList getGeoList(Location location) throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/geo/list";
		GeoListReq geoListReq = new GeoListReq();
		Metadata metadata = new Metadata();
		metadata.setLocale("id_ID");
		geoListReq.setMetadata(metadata);
		geoListReq.setCountryCode(location.getCountryCode());
		geoListReq.setLimit(location.getLimit());
		geoListReq.setOffset(location.getOffset());
		GeoList geoList = restService.post(geoListReq,APIType.ACCOMODATION,url,GeoList.class);
		return geoList;
	}

	@Override
	public PropertyList getPropertyList(PropertyListReq propertyListReq) throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/property/list";
		PropertyList propertyList = (PropertyList) restService.post(propertyListReq, APIType.ACCOMODATION, url,PropertyList.class);
		return propertyList;
	}

	@Override
	public PropertyDetails getPropertyDetails(PropertyDetailsReq propertyDetailsReq) throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/property/details";
		PropertyDetails propertyDetails = restService.post(propertyDetailsReq, APIType.ACCOMODATION, url, PropertyDetails.class);
		return propertyDetails;
	}

	@Override
	public RoomsList getRoomsList(HRoom hRoom) throws URISyntaxException {
		BTDetailHarpaEntity btDetail = btDetailService.getByBtIdOnHarpa(hRoom.getBtId().get(0));
		ClaimHarpaEntity claims = new ClaimHarpaEntity();
		for (ClaimHarpaEntity claim : btDetail.getClaimEntity()) {
			if (claim.getClaimItem().equals("Hotel")) {
				claims = claim;
			}
		}
		String url = "https://api.afc.staging-traveloka.com/accom/v1/rooms";
		RoomsListReq req = new RoomsListReq("2");
		req.setCheckInDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getFromDate()));
		req.setCheckOutDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getToDate()));
		req.setPropertyId(hRoom.getPropertyId());
		Metadata metadata;
		try {
			metadata = new Metadata("IDR", "id_ID");
			req.setMetadata(metadata);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		RoomsList roomsList = restService.post(req, APIType.ACCOMODATION, url, RoomsList.class);
		return roomsList;
	}

	@Override
	public PreBook getPreBook(ClaimHarpaEntity claims, String propertyId, String roomId, String rateKey, String numGuest) throws URISyntaxException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/avail";
		PreBookReq req = new PreBookReq(numGuest,roomId,rateKey);
		req.setPropertyId(propertyId);
		req.setCheckInDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getFromDate()));
		req.setCheckOutDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getToDate()));
		Metadata metadata;
		try {
			metadata = new Metadata("IDR","id_ID");
			req.setMetadata(metadata);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PreBook preBook = restService.post(req, APIType.ACCOMODATION, url, PreBook.class);
		return preBook;
	}

	@Override
	public SearchList getSearchList(String btId, String limit, String offset) throws URISyntaxException, UnknownHostException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/list";
		BTDetailHarpaEntity btDetail = btDetailService.getByBtIdOnHarpa(btId);
		ClaimHarpaEntity claims = new ClaimHarpaEntity();
		for (ClaimHarpaEntity claim : btDetail.getClaimEntity()) {
			if (claim.getClaimItem().equals("Hotel")) {
				claims = claim;
			}
		}
		SearchListReq req = new SearchListReq();
		req.setCheckInDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getFromDate()));
		req.setCheckOutDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getToDate()));
		List<GeoLocationEntity> geoLocation = geoLocationRepo.findByName(claims.getToCity());
		List<String> rateTypes = new ArrayList<String>();
		rateTypes.add("PAY_NOW");
		req.setRateTypes(rateTypes);
		req.setGeoId(geoLocation.get(0).getGeoId());
		req.setLimit(limit);
		req.setSortType(AppConstants.SORT_POPULAR);
		req.setOffset(offset);
		List<String> opt = new ArrayList<String>();
		opt.add("PROPERTY_IMAGES");
		req.setOptions(opt);
		Room room = new Room();
		room.setNumOfAdults("2");
		List<Room> rooms = new ArrayList<Room>();
		rooms.add(room);
		RoomGroup roomGroup = new RoomGroup();
		roomGroup.setRooms(rooms);
		req.setRoomGroup(roomGroup);
		StarsFilter starsFilter = new StarsFilter(Integer.valueOf(btDetail.getJobLevel()));
		req.setStars(starsFilter.getStars());
		Metadata metadata = new Metadata("IDR", "id_ID");
		req.setMetadata(metadata);
		SearchList searchList = restService.post(req, APIType.ACCOMODATION, url, SearchList.class);
		return searchList;
	}

	@Override
	public HotelBook createBook(HBook hbook) throws URISyntaxException, UnknownHostException, ParseException, AccomodationException {
		String url = "https://api.afc.staging-traveloka.com/accom/v1/itin/create";
		List<BTDetailHarpaEntity> btDetail = btDetailService.getByIds(hbook.getBtId());
		NISPADUserDetails userDetail = (NISPADUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ClaimHarpaEntity claims = new ClaimHarpaEntity();
		for (ClaimHarpaEntity claim : btDetail.get(0).getClaimEntity()) {
			if (claim.getClaimItem().equals("Hotel")) {
				claims = claim;
			}
		}
		PreBook preBook = getPreBook(claims, hbook.getPropertyId(), hbook.getRoomId(), hbook.getRateKey(),String.valueOf(btDetail.size()));
		CreateBooking req = new CreateBooking(hbook, preBook);
		List<String> rateTypes = new ArrayList<String>();
		rateTypes.add("PAY_NOW");
		req.setRateTypes(rateTypes);
		req.setAffiliateConfirmationIds(UUID.randomUUID().toString());
		req.setCheckInDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getFromDate()));
		req.setCheckOutDate(new SimpleDateFormat("yyyy-MM-dd").format(claims.getToDate()));
		req.setPropertyId(hbook.getPropertyId());
		req.setLoginId(userDetail.getEmail());
		req.setLoginType("EMAIL");
		req.setUserPaymentMethod("OTHERS");
		req.setSpecialRequest(hbook.getSpesialRequest());
		ExpectedTotalChargeableRate chargeRate = new ExpectedTotalChargeableRate();
		chargeRate.setCurrencyCode(preBook.getRooms().get(0).getRates().getConvertedChargeableRate().getCurrencyCode());
		chargeRate.setAmount(preBook.getRooms().get(0).getRates().getConvertedChargeableRate().getTotal());
		Metadata metadata = new Metadata("IDR", "id_ID");
		req.setExpectedTotalChargeableRate(chargeRate);
		req.setMetadata(metadata);
		HotelBook hotelBook;
		if (hbook.getGuest().size() > 1) {
			if (hbook.getGuest().get(0).getTitle().equals(hbook.getGuest().get(1).getTitle())) {
				hotelBook = restService.post(req, APIType.ACCOMODATION, url, HotelBook.class);
			}else {
				throw new AccomodationException(AppConstants.GENDER_EXCEPTION);
			}
		}else {
			hotelBook = restService.post(req, APIType.ACCOMODATION, url, HotelBook.class);
		}
		
		
		if (hotelBook.getStatus().toUpperCase().equals(AppConstants.SUCCESS)) {
			HotelBookingEntity hotelBookEntity = new HotelBookingEntity(hotelBook, true);
			hotelBookRepo.save(hotelBookEntity);
			int index = 0;
			for (String id : hbook.getBtId()) {
				BTDetailEntity btDetailNew = null;
				BTDetailEntity checkBT = btDetailService.getByIdCheck(id);
				if (checkBT != null) {
					btDetailNew = new BTDetailEntity(btDetail.get(index), false,hotelBookEntity.getId());
				}else {
					btDetailNew = new BTDetailEntity(btDetail.get(index), true,hotelBookEntity.getId());
				}
				btDetailNew = btDetailService.save(btDetailNew);
				index=index+1;
			}
		}
		return hotelBook;
	}

}
