package com.ocbcnisp.traveloka.service.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.ocbcnisp.traveloka.model.auth.APIType;
import com.ocbcnisp.traveloka.model.auth.Auth;
import com.ocbcnisp.traveloka.service.IRestService;

@Service
@PropertySource("classpath:auth.properties")
public class RestService implements IRestService{

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${accomodation.client.id}")
	private String accomClientId;
	
	@Value("${accomodation.client.secret}")
	private String accomClientSecret;
	
	@Value("${flight.client.id}")
	private String flightClientId;
	
	@Value("${flight.client.secret}")
	private String flightClientSecret;
	
	@Value("${auth.url}")
	private String authUrl;
	
	@Override
	public <T> T get(APIType type, String url, Class<T> clazz) throws URISyntaxException {
		Auth authToken = auth(type);
		URI postUri = new URI(url);
		RequestEntity<Void> requestEntity = RequestEntity.get(postUri).header("Authorization", authToken.getAccessToken()).build();
		ResponseEntity<T> result = restTemplate.exchange(requestEntity, clazz);
		T body = result.getBody();
		return body;
	}

	@Override
	public <T, S> T post(S s, APIType type, String url, Class<T> clazz) throws URISyntaxException {
		Auth authToken = auth(type);
		URI postUri = new URI(url);
		RequestEntity<S> requestEntity = RequestEntity.post(postUri).header("Authorization", authToken.getAccessToken()).body(s);
		ResponseEntity<T> result = restTemplate.exchange(requestEntity, clazz);
		T body = result.getBody();
		return body;
	}

	public Auth auth(APIType type) throws URISyntaxException {
		if (type.equals(APIType.FLIGHT)) {
			URI clientUri = new URI(authUrl);
			MultiValueMap<String, String> bodyAuth = new LinkedMultiValueMap<String, String>();
			bodyAuth.add("client_id", flightClientId);
			bodyAuth.add("client_secret", flightClientSecret);
			RequestEntity<MultiValueMap<String, String>> reqClient = RequestEntity.post(clientUri).contentType(MediaType.APPLICATION_FORM_URLENCODED).body(bodyAuth);
			ResponseEntity<Auth> authResult = restTemplate.exchange(reqClient, Auth.class);
			Auth authToken = authResult.getBody();
			return authToken;
		}else {
			URI clientUri = new URI(authUrl);
			MultiValueMap<String, String> bodyAuth = new LinkedMultiValueMap<String, String>();
			bodyAuth.add("client_id", accomClientId);
			bodyAuth.add("client_secret", accomClientSecret);
			RequestEntity<MultiValueMap<String, String>> reqClient = RequestEntity.post(clientUri).contentType(MediaType.APPLICATION_FORM_URLENCODED).body(bodyAuth);
			ResponseEntity<Auth> authResult = restTemplate.exchange(reqClient, Auth.class);
			Auth authToken = authResult.getBody();
			return authToken;
		}
	}

}
