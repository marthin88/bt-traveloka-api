package com.ocbcnisp.traveloka.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.BTDetailHarpaEntity;
import com.ocbcnisp.traveloka.entity.ClaimEntity;
import com.ocbcnisp.traveloka.entity.HotelBookingEntity;
import com.ocbcnisp.traveloka.repository.IBTDetailHarpaRepository;
import com.ocbcnisp.traveloka.repository.IBTDetailRepository;
import com.ocbcnisp.traveloka.repository.IHotelBookingRepository;
import com.ocbcnisp.traveloka.repository.impl.ProcBTDetailRepository;
import com.ocbcnisp.traveloka.service.IBTDetailService;

@Service
public class BTDetailService implements IBTDetailService{

	
	@Autowired
	private IBTDetailHarpaRepository btDetailHarpaRepo;
	
	@Autowired
	private IBTDetailRepository btDetailRepo;
	
	@Autowired
	private IHotelBookingRepository hotelBookRepo;
	
	@Autowired
	private ProcBTDetailRepository procBTDetailRepo;

	@Override
	public BTDetailEntity save(BTDetailEntity btDetail) {
		return btDetailRepo.save(btDetail);
	}

	@Override
	public BTDetailHarpaEntity getByBtIdOnHarpa(String btId) {
		return btDetailHarpaRepo.findById(btId).get();
	}

	@Override
	public BTDetailEntity getById(String btId) {
		return procBTDetailRepo.findEligible(btId);
	}
	
	@Override
	public BTDetailEntity getByIdCheck(String btId) {
		Optional<BTDetailEntity> btOpt =btDetailRepo.findById(btId);
		if (btOpt.isPresent()) {
			return btDetailRepo.findById(btId).get();
		}
		return null;
	}

	@Override
	public List<BTDetailHarpaEntity> getListBT() {
		List<BTDetailHarpaEntity> list = new ArrayList<BTDetailHarpaEntity>();
		btDetailHarpaRepo.findAll().forEach(list::add);
		return list;
	}

	@Override
	public List<BTDetailHarpaEntity> getByIds(List<String> btIds) {
		List<BTDetailHarpaEntity> entities = new ArrayList<BTDetailHarpaEntity>();
		btDetailHarpaRepo.findAllById(btIds).forEach(entities::add);
		return entities;
	}

}
