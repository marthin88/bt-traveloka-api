package com.ocbcnisp.traveloka.service;

import java.net.URISyntaxException;

import com.ocbcnisp.traveloka.model.auth.APIType;

public interface IRestService {
	
	public <T> T get(APIType type, String url, Class<T> clazz) throws URISyntaxException;
	public <T, S> T post(S s, APIType type, String url, Class<T> clazz) throws URISyntaxException;

}
