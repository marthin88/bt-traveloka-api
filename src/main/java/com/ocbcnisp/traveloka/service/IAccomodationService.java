package com.ocbcnisp.traveloka.service;

import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.ParseException;

import com.ocbcnisp.traveloka.entity.ClaimHarpaEntity;
import com.ocbcnisp.traveloka.exception.AccomodationException;
import com.ocbcnisp.traveloka.model.accomodation.request.PropertyDetailsReq;
import com.ocbcnisp.traveloka.model.accomodation.request.PropertyListReq;
import com.ocbcnisp.traveloka.model.accomodation.response.GeoList;
import com.ocbcnisp.traveloka.model.accomodation.response.HotelBook;
import com.ocbcnisp.traveloka.model.accomodation.response.PreBook;
import com.ocbcnisp.traveloka.model.accomodation.response.PropertyDetails;
import com.ocbcnisp.traveloka.model.accomodation.response.PropertyList;
import com.ocbcnisp.traveloka.model.accomodation.response.RoomsList;
import com.ocbcnisp.traveloka.model.accomodation.response.SearchList;
import com.ocbcnisp.traveloka.model.http.request.HBook;
import com.ocbcnisp.traveloka.model.http.request.HRoom;
import com.ocbcnisp.traveloka.model.http.request.Location;

public interface IAccomodationService {
	
	public GeoList getGeoList(Location location) throws URISyntaxException;
	public PropertyList getPropertyList(PropertyListReq propertyListReq) throws URISyntaxException;
	public PropertyDetails getPropertyDetails(PropertyDetailsReq propertyDetailsReq)throws URISyntaxException;
	public SearchList getSearchList(String btId, String limit, String offset) throws URISyntaxException, UnknownHostException;
	public RoomsList getRoomsList(HRoom hRoom) throws URISyntaxException;
	public PreBook getPreBook(ClaimHarpaEntity claims, String propertyId, String roomId, String rateKey, String numGuest) throws URISyntaxException;
	public HotelBook createBook(HBook hbook) throws URISyntaxException, UnknownHostException, ParseException, AccomodationException;
}
