package com.ocbcnisp.traveloka.service;


import java.util.List;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.BTDetailHarpaEntity;

public interface IBTDetailService {

	public BTDetailEntity save(BTDetailEntity btDetail);
	public BTDetailHarpaEntity getByBtIdOnHarpa(String btId);
	public List<BTDetailHarpaEntity> getByIds(List<String> btIds);
	public List<BTDetailHarpaEntity> getListBT();
	public BTDetailEntity getById(String btId);
	BTDetailEntity getByIdCheck(String btId);
}
