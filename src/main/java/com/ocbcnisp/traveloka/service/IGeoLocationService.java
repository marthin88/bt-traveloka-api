package com.ocbcnisp.traveloka.service;

import java.util.List;

import com.ocbcnisp.traveloka.entity.GeoLocationEntity;

public interface IGeoLocationService {

	public List<GeoLocationEntity> findByName(String name);
}
