package com.ocbcnisp.traveloka.security.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NISPADUserDetails {

    private String email;
    private String firstname;
    private String lastname;
    private String source;
    private String username;
    private String session;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;
    private Date lastConnectionAt;
    private String status;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getLastConnectionAt() {
		return lastConnectionAt;
	}
	public void setLastConnectionAt(Date lastConnectionAt) {
		this.lastConnectionAt = lastConnectionAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}

}
