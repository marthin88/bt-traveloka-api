package com.ocbcnisp.traveloka.security.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "RSAKeyValue")
public class RSAPrivateKeyWS {

    private String elmModulus;

    private String elmExponent;

    private String elmP;

    private String elmQ;

    private String elmDP;

    private String elmDQ;

    private String elmInverseQ;

    private String elmD;

    /**
     * @return the elmModulus
     */
    @XmlElement(name = "Modulus")
    public String getElmModulus() {
        return elmModulus;
    }

    /**
     * @param elmModulus the elmModulus to set
     */
    public void setElmModulus(String elmModulus) {
        this.elmModulus = elmModulus;
    }

    /**
     * @return the elmExponent
     */
    @XmlElement(name = "Exponent")
    public String getElmExponent() {
        return elmExponent;
    }

    /**
     * @param elmExponent the elmExponent to set
     */
    public void setElmExponent(String elmExponent) {
        this.elmExponent = elmExponent;
    }

    /**
     * @return the elmP
     */
    @XmlElement(name = "P")
    public String getElmP() {
        return elmP;
    }

    /**
     * @param elmP the elmP to set
     */
    public void setElmP(String elmP) {
        this.elmP = elmP;
    }

    /**
     * @return the elmQ
     */
    @XmlElement(name = "Q")
    public String getElmQ() {
        return elmQ;
    }

    /**
     * @param elmQ the elmQ to set
     */
    public void setElmQ(String elmQ) {
        this.elmQ = elmQ;
    }

    /**
     * @return the elmDP
     */
    @XmlElement(name = "DP")
    public String getElmDP() {
        return elmDP;
    }

    /**
     * @param elmDP the elmDP to set
     */
    public void setElmDP(String elmDP) {
        this.elmDP = elmDP;
    }

    /**
     * @return the elmDQ
     */
    @XmlElement(name = "DQ")
    public String getElmDQ() {
        return elmDQ;
    }

    /**
     * @param elmDQ the elmDQ to set
     */
    public void setElmDQ(String elmDQ) {
        this.elmDQ = elmDQ;
    }

    /**
     * @return the elmInverseQ
     */
    @XmlElement(name = "InverseQ")
    public String getElmInverseQ() {
        return elmInverseQ;
    }

    /**
     * @param elmInverseQ the elmInverseQ to set
     */
    public void setElmInverseQ(String elmInverseQ) {
        this.elmInverseQ = elmInverseQ;
    }

    /**
     * @return the elmD
     */
    @XmlElement(name = "D")
    public String getElmD() {
        return elmD;
    }

    /**
     * @param elmD the elmD to set
     */    
    public void setElmD(String elmD) {
        this.elmD = elmD;
    }

}