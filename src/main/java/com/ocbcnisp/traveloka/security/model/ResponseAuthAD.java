package com.ocbcnisp.traveloka.security.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
public class ResponseAuthAD {

    private String objectDN;

    private String guid2;

    private String description;

    private List<MemberAD> memberOfCollections;

    private String userAcctControl;

    private String objectSID;

    @XmlElement(name = "ObjectDn")
    public String getObjectDN() {
        return objectDN;
    }

    public void setObjectDN(String objectDN) {
        this.objectDN = objectDN;
    }

    @XmlElement(name = "GUID2")
    public String getGuid2() {
        return guid2;
    }

    public void setGuid2(String guid2) {
        this.guid2 = guid2;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MemberAD> getMemberOfCollections() {
        return memberOfCollections;
    }

    public void setMemberOfCollections(List<MemberAD> memberOfCollections) {
        this.memberOfCollections = memberOfCollections;
    }

    @XmlElement(name = "userAccountControl")
    public String getUserAcctControl() {
        return userAcctControl;
    }

    public void setUserAcctControl(String userAcctControl) {
        this.userAcctControl = userAcctControl;
    }

    @XmlElement(name = "objectSid")
    public String getObjectSID() {
        return objectSID;
    }

    public void setObjectSID(String objectSID) {
        this.objectSID = objectSID;
    }
}
