package com.ocbcnisp.traveloka.security.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="memberOfCollections")
public class MemberAD {
    private String memberOf;

    @XmlElement(name = "memberOf")
    public String getMemberOf() {
            return memberOf;
    }

    public void setMemberOf(String memberOf) {
            this.memberOf = memberOf;
    }
}
