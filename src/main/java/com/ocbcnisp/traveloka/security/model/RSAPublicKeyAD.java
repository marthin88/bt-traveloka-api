package com.ocbcnisp.traveloka.security.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RSAKeyValue")
public class RSAPublicKeyAD {

    private String modulus;

    private String exponent;

    /**
     * @return the modulus
     */
    @XmlElement(name = "Modulus")
    public String getModulus() {
        return modulus;
    }

    /**
     * @param modulus the modulus to set
     */
    public void setModulus(String modulus) {
        this.modulus = modulus;
    }

    /**
     * @return the exponent
     */
    @XmlElement(name = "Exponent")
    public String getExponent() {
        return exponent;
    }

    /**
     * @param exponent the exponent to set
     */
    public void setExponent(String exponent) {
        this.exponent = exponent;
    }

}

