package com.ocbcnisp.traveloka.security.provider;

import java.rmi.RemoteException;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;
import com.ocbcnisp.traveloka.security.model.ResponseAuthAD;

@Component
public class NISPADProvider implements AuthenticationProvider{

	private static final Logger LOGGER = LoggerFactory.getLogger(NISPADProvider.class);
	
	@Autowired
	private LoginADService loginADService;
	
	@Autowired
	private Environment env;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getPrincipal().toString();
		String password = authentication.getCredentials().toString();
		ResponseAuthAD response = null;
		String domainAD = env.getProperty("nispad.domainname");
		NISPADUserDetails user = null;
		List<GrantedAuthority> authorities = null;
		try {
			response = loginADService.authenticatedService(domainAD, username, password);
			if (response != null) {
				authorities = AuthorityUtils.createAuthorityList("USER");
				user = new NISPADUserDetails();
				String[] fullName = username.split("\\.");
				String firstname = fullName[0];
				String lastname = "";
				
				if (fullName.length>1) {
					lastname = fullName[1];
				}
				user.setEmail(username+"@"+domainAD);
				user.setFirstname(firstname);
				user.setLastname(lastname);
				user.setSource("nispad");
				user.setUsername(username);
			}
			if (user.getSource().equals("nispad")) {
				return new UsernamePasswordAuthenticationToken(user, "", authorities);
			}else {
				throw new UsernameNotFoundException("username not found");
			}
		} catch (AxisFault e) {
			LOGGER.error(e.getMessage(),e);
			throw new UsernameNotFoundException(e.getMessage());
		} catch (RemoteException e) {
			LOGGER.error(e.getMessage(),e);
			throw new UsernameNotFoundException(e.getMessage());
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
