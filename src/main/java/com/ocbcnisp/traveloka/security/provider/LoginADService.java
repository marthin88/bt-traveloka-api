package com.ocbcnisp.traveloka.security.provider;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ocbcnisp.traveloka.security.model.RSAPublicKeyAD;
import com.ocbcnisp.traveloka.security.model.ResponseAuthAD;
import com.ocbcnisp.traveloka.utility.CommonUtil;

@Component
public class LoginADService {

	public static final String APP_RSA_METHOD = "RSA";
	
	@Autowired
	private Environment env;
	
	@Autowired
	private NISPADService nispADService;
	
	private String pubKey;
	
	private final Logger logger = LoggerFactory.getLogger(LoginADService.class);
	
	public ResponseAuthAD authenticatedService(String domain, String username, String password) throws RemoteException {
//		RSAPublicKeyAD rsaResult;
//		KeyFactory factory;
//		byte[] modulusBytes;
//		byte[] exponentBytes;
//		
//		BigInteger mod;
//		BigInteger exp;
//		
//		RSAPublicKeySpec spec;
//		RSAPublicKey pub;
//		String encryptPassword;
//		Cipher cipher;
		String response;
		String stubUrl = env.getProperty("nispad.url");
//		
//		pubKey = nispADService.getRSAPublicKey(stubUrl);
//		rsaResult = (RSAPublicKeyAD) CommonUtil.convertStringToObject(RSAPublicKeyAD.class, pubKey);
		
//			factory = KeyFactory.getInstance(APP_RSA_METHOD);
//			
//			modulusBytes = Base64.getDecoder().decode(rsaResult.getModulus());
//			exponentBytes = Base64.getDecoder().decode(rsaResult.getExponent());
//			
//			mod = new BigInteger(1, modulusBytes);
//			exp = new BigInteger(1, exponentBytes);
//			spec = new RSAPublicKeySpec(mod, exp);
//			pub = (RSAPublicKey) factory.generatePublic(spec);
//			
//			cipher = Cipher.getInstance(APP_RSA_METHOD);
//			cipher.init(Cipher.ENCRYPT_MODE, pub);
//			encryptPassword = Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes(Charset.forName("UTF-8"))));
			response = nispADService.authenticateAD(domain, username, password, stubUrl);
			if (response != null) {
				ResponseAuthAD responseAuth = (ResponseAuthAD) CommonUtil.convertStringToObject(ResponseAuthAD.class, response);
				return responseAuth;
			}
		return null;
	}
}
