package com.ocbcnisp.traveloka.security.provider;

import java.rmi.RemoteException;

import org.apache.axis2.transport.http.HTTPConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.tempuri.Authenticate3;
import org.tempuri.Authenticate3Response;
import org.tempuri.GetRSAPublicKey;
import org.tempuri.GetRSAPublicKeyResponse;
import org.tempuri.ServiceStub;

@Component
public class NISPADService {

	private final Logger logger = LoggerFactory.getLogger(NISPADService.class);
	
	public String getRSAPublicKey(String url) throws RemoteException {
		String pubKey = null;
		logger.info("get rsa public key url" + url);
		ServiceStub stub = new ServiceStub(url);
		GetRSAPublicKey rsaPublicKey = new GetRSAPublicKey();
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, false);
		GetRSAPublicKeyResponse rsaPublicKeyResponse = stub.getRSAPublicKey(rsaPublicKey);
		pubKey = rsaPublicKeyResponse.getGetRSAPublicKeyResult();
		return pubKey;
	}
	
	public String authenticateAD(String adDomain, String adUsername, String adRSAEncryptPassword, String url) throws RemoteException {
		String authenticateResult = null;
		
		ServiceStub stub = new ServiceStub(url);
		Authenticate3 authenticate = new Authenticate3();
		authenticate.setAdDomain(adDomain);
		authenticate.setAdUserName(adUsername);
		authenticate.setAdRSAEncyrptedPassword(adRSAEncryptPassword);
		Authenticate3Response authResponse = stub.authenticate3(authenticate);
		authenticateResult = authResponse.getAuthenticate3Result();
		return authenticateResult;
	}
}
