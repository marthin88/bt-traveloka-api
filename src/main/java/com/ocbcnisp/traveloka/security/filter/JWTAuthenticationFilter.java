package com.ocbcnisp.traveloka.security.filter;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.internal.org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocbcnisp.traveloka.model.http.response.ResponseModel;
import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;

public class JWTAuthenticationFilter extends GenericFilterBean{

	private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationFilter.class);
	
	private final JWTVerifier jwtVerifier;
//	private JWTCookieGenerator jwtCookieGenerator;
	
	public JWTAuthenticationFilter(final String jwtSecret) {
        this.jwtVerifier = new JWTVerifier(jwtSecret);
//        this.jwtCookieGenerator = jwtCookieGenerator;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
        String stringToken = ((HttpServletRequest) request).getHeader("OCBCNISP-TOKEN");
        if(StringUtils.isEmpty(stringToken)) {
        	LOGGER.debug("Authorization header/cookie not found");
        	ResponseModel responseModel = new ResponseModel();
        	responseModel.setStatus(401);
        	responseModel.setMessage("Authorization is rejected");
        	response.getWriter().write(new ObjectMapper().writeValueAsString(responseModel));
        } else {
        	final String authorizationSchema = "OCBC-TOKEN";
        	if (stringToken.contains(authorizationSchema)) {
				final String jwtToken = stringToken.substring(authorizationSchema.length()).trim();
				try {
					final Map<String, Object> verify = jwtVerifier.verify(jwtToken);
					List<Map> permissions = (List<Map>) verify.get("permissions");
					List<SimpleGrantedAuthority>  authorities;
					
					if (permissions != null) {
						authorities = ((List<Map>) verify.get("permissions")).stream().map(map -> new SimpleGrantedAuthority(map.get("authority").toString())).collect(Collectors.toList());
					} else {
						authorities = Collections.emptyList();
					}
							
					final NISPADUserDetails userDetails = new NISPADUserDetails();
					userDetails.setUsername(getStringValue(verify.get("sub")));
					userDetails.setEmail((String)verify.get("email"));
					userDetails.setFirstname((String) verify.get("firstname"));
					userDetails.setLastname((String) verify.get("lastname"));
					userDetails.setSession((String) verify.get("session"));
					userDetails.setUsername((String) verify.get("lastname")+"."+(String) verify.get("lastname"));
					SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userDetails, null,authorities));
				} catch (Exception e) {
					final String errMsg ="invalid token";
					LOGGER.error(errMsg,e);
					ResponseModel responseModel = new ResponseModel();
		        	responseModel.setStatus(401);
		        	responseModel.setMessage(errMsg);
		        	response.getWriter().write(new ObjectMapper().writeValueAsString(responseModel));
					return;
				}
			}else {
				LOGGER.debug("Authorization schema not found");
				ResponseModel responseModel = new ResponseModel();
	        	responseModel.setStatus(401);
	        	responseModel.setMessage("Authorization schema not found");
	        	response.getWriter().write(new ObjectMapper().writeValueAsString(responseModel));
			}
        }
		chain.doFilter(request, response);
	}
	private String getStringValue(final Object object) {
        if (object == null) {
            return "";
        }
        return object.toString();
    }

}
