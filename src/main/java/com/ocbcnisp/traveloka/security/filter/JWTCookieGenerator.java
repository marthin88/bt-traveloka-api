package com.ocbcnisp.traveloka.security.filter;

import javax.servlet.http.Cookie;

import org.springframework.stereotype.Component;

@Component
public class JWTCookieGenerator {

	private static final boolean DEFAULT_JWT_COOKIE_SECURE = false;
	private static final String DEFAULT_JWT_COOKIE_PATH = "/";
    private static final String DEFAULT_JWT_COOKIE_DOMAIN = "";
    
    public Cookie generate(final String value) {
    	final Cookie cookie = new Cookie("Auth-OCBC-NISP", value);
    	cookie.setHttpOnly(true);
    	cookie.setSecure(DEFAULT_JWT_COOKIE_SECURE);
    	cookie.setPath(DEFAULT_JWT_COOKIE_PATH);
    	cookie.setDomain(DEFAULT_JWT_COOKIE_DOMAIN);
    	cookie.setMaxAge(value == null? 0 : 604800);
    	return cookie;
    }
}
