package com.ocbcnisp.traveloka.security.filter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.internal.org.apache.commons.io.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocbcnisp.traveloka.model.http.response.LoginRequest;

public class RequestBodyAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	final static Logger logger = LoggerFactory.getLogger(RequestBodyAuthenticationFilter.class);
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		final ObjectMapper objectMapper = new ObjectMapper();
		try {
			String login = IOUtils.toString(request.getReader());
			LoginRequest loginRequest = objectMapper.readValue(login, LoginRequest.class);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
			setDetails(request, token);
			return this.getAuthenticationManager().authenticate(token);
		} catch (IOException e) {
			logger.error("request body cannot be retrieved");
			throw new InternalAuthenticationServiceException("Something went wrong when parse request body");
		}
	}
}
