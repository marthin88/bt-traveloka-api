package com.ocbcnisp.traveloka.security.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWTSigner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocbcnisp.traveloka.model.http.response.Token;
import com.ocbcnisp.traveloka.security.model.NISPADUserDetails;

@Component
public class SuccessAuthenticationHandler implements AuthenticationSuccessHandler{

	private String jwtSecret;
	
	public SuccessAuthenticationHandler(String jwtSecret) {
		this.jwtSecret = jwtSecret;
	}
	public SuccessAuthenticationHandler() {}
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		if (authentication != null) {
			final Map<String, Object> claims = new HashMap<String, Object>();
			claims.put("iss", "bt-traveloka-authentication");
			NISPADUserDetails userDetails = new NISPADUserDetails();
			if (authentication.getPrincipal() instanceof NISPADUserDetails) {
				userDetails = (NISPADUserDetails) authentication.getPrincipal();
			}
			
			Set<GrantedAuthority> authorities = new HashSet<>(authentication.getAuthorities());
			
			claims.put("permissions", authorities);
            claims.put("sub", userDetails.getUsername());
            claims.put("email", userDetails.getEmail());
            claims.put("firstname", userDetails.getFirstname());
            claims.put("lastname", userDetails.getLastname());
            userDetails.setSession(UUID.randomUUID().toString());
            claims.put("session",userDetails.getSession());
            
            final JWTSigner.Options options = new JWTSigner.Options();
            options.setExpirySeconds(604800);
            options.setIssuedAt(true);
            options.setJwtId(true);
            final String sign = new JWTSigner(jwtSecret).sign(claims,options);
            final Token token = new Token();
            token.setType("OCBC-TOKEN");
            token.setToken(sign);
            token.setDataUser(userDetails);
            token.setHttpStatus(HttpStatus.OK.value());
            
            response.getWriter().write(new ObjectMapper().writeValueAsString(token));
            response.setStatus(HttpStatus.OK.value());
            response.setContentType("application/json");
		}
		
	}

}
