package com.ocbcnisp.traveloka.entity.rowmap;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ocbcnisp.traveloka.entity.GeoLocationEntity;

public class GeoLocationRowMapper implements RowMapper<GeoLocationEntity> {

	@Override
	public GeoLocationEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		GeoLocationEntity geo = new GeoLocationEntity();
		geo.setGeoId(rs.getString(1));
		geo.setParentId(rs.getString(2));
		geo.setType(rs.getString(3));
		geo.setName(rs.getString(4));
		geo.setLocaleName(rs.getString(5));
		return geo;
	}

}
