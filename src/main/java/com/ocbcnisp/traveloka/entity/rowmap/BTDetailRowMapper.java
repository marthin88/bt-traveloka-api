package com.ocbcnisp.traveloka.entity.rowmap;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.ClaimEntity;

public class BTDetailRowMapper implements RowMapper<BTDetailEntity>{

	@Override
	public BTDetailEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		BTDetailEntity btDetailEntity = new BTDetailEntity();
		ClaimEntity claimEntity = new ClaimEntity();
		btDetailEntity.setId(rs.getString("BT_ID"));
		btDetailEntity.setNik(rs.getString("NIK"));
		btDetailEntity.setName(rs.getString("NAMA"));
		btDetailEntity.setTransactionDate(rs.getDate("TRANSACTION_DATE"));
		btDetailEntity.setTransactionNumber(rs.getString("TRANSACTION_NO"));
		btDetailEntity.setTotalClaim(rs.getBigDecimal("TOTAL_CLAIM"));
		btDetailEntity.setPaidAmount(rs.getBigDecimal("PAID_AMOUNT"));
		btDetailEntity.setStartDate(rs.getDate("START_DATE"));
		btDetailEntity.setEndDate(rs.getDate("END_DATE"));
		btDetailEntity.setGender(rs.getString("GENDER"));
		btDetailEntity.setJobLevel(rs.getString("JOB_LEVEL"));
		claimEntity.setId(rs.getString("CLAIM_ID"));
		claimEntity.setClaimItem(rs.getString("CLAIM_ITEM"));
		claimEntity.setClaimUnit(rs.getString("CLAIM_UNIT"));
		claimEntity.setClaimAmount(rs.getBigDecimal("CLAIM_AMOUNT"));
		claimEntity.setFromDate(rs.getDate("FROM_DATE"));
		claimEntity.setToDate(rs.getDate("TO_DATE"));
		claimEntity.setFromCountry(rs.getString("FROM_COUNTRY"));
		claimEntity.setToCountry(rs.getString("TO_COUNTRY"));
		claimEntity.setFromCity(rs.getString("FROM_CITY"));
		claimEntity.setToCity(rs.getString("TO_CITY"));
		claimEntity.setRoundTrip(rs.getBoolean("ROUND_TRIP"));
		btDetailEntity.addClaimEntity(claimEntity);		
		return btDetailEntity;
	}

}
