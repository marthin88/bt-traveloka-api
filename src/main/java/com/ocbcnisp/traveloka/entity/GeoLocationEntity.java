package com.ocbcnisp.traveloka.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table("GEO_LOCATION")
public class GeoLocationEntity implements Persistable<String>{
	
	@Id
	private String geoId;
	@Column("PARENT_ID")
	private String parentId;
	@Column("TYPE")
	private String type;
	@Column("NAME")
	private String name;
	@Column("LOCALE_NAME")
	private String localeName;
	
	@JsonIgnore
	@Transient
	private boolean newEntity;
	
	public String getGeoId() {
		return geoId;
	}
	public void setGeoId(String geoId) {
		this.geoId = geoId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocaleName() {
		return localeName;
	}
	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}
	@JsonIgnore
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return geoId;
	}
	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
	@JsonIgnore
	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return newEntity;
	}

}
