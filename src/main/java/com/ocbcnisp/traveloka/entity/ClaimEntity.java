package com.ocbcnisp.traveloka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table("CLAIM_BT")
public class ClaimEntity implements Persistable<String>,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6184458121582111364L;
	@Id
	@Column("CLAIM_ID")
	private String id;
	private String claimItem;
	private String claimUnit;
	private BigDecimal claimAmount;
	private Date fromDate;
	private Date toDate;
	private String fromCountry;
	private String toCountry;
	private String fromCity;
	private String toCity;
	private Boolean roundTrip;
	private String bookId;
	@Transient
	private HotelBookingEntity hotelBookingEntity;
	
	public ClaimEntity(ClaimHarpaEntity claimEntity) {
		this.id = claimEntity.getId();
		this.claimItem = claimEntity.getClaimItem();
		this.claimUnit = claimEntity.getClaimUnit();
		this.claimAmount = claimEntity.getClaimAmount();
		this.fromDate = claimEntity.getFromDate();
		this.toDate = claimEntity.getToDate();
		this.fromCountry = claimEntity.getFromCountry();
		this.toCountry = claimEntity.getToCountry();
		this.fromCity = claimEntity.getFromCity();
		this.toCity = claimEntity.getToCity();
		this.roundTrip = claimEntity.getRoundTrip();
	}
	public ClaimEntity() {
	}
	@Transient
	@JsonIgnore
	private boolean newEntity;
	
	public HotelBookingEntity getHotelBookingEntity() {
		return hotelBookingEntity;
	}
	public void setHotelBookingEntity(HotelBookingEntity hotelBookingEntity) {
		this.hotelBookingEntity = hotelBookingEntity;
	}
	@Override
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getClaimItem() {
		return claimItem;
	}
	public void setClaimItem(String claimItem) {
		this.claimItem = claimItem;
	}
	public String getClaimUnit() {
		return claimUnit;
	}
	public void setClaimUnit(String claimUnit) {
		this.claimUnit = claimUnit;
	}
	public BigDecimal getClaimAmount() {
		return claimAmount;
	}
	public void setClaimAmount(BigDecimal claimAmount) {
		this.claimAmount = claimAmount;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getFromCountry() {
		return fromCountry;
	}
	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}
	public String getToCountry() {
		return toCountry;
	}
	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	public String getToCity() {
		return toCity;
	}
	public void setToCity(String toCity) {
		this.toCity = toCity;
	}
	public Boolean getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(Boolean roundTrip) {
		this.roundTrip = roundTrip;
	}
	@Override
	public boolean isNew() {
		return newEntity;
	}
	public boolean isNewEntity() {
		return newEntity;
	}
	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
}
