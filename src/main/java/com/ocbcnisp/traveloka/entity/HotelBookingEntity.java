package com.ocbcnisp.traveloka.entity;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ocbcnisp.traveloka.model.accomodation.response.HotelBook;

@Table("HOTEL_BOOKING")
public class HotelBookingEntity implements Persistable<String>{

	@Id
	@Column("BOOK_ID")
	private String id;
	private String iteneraryId;
	private String status;
	private String hotelName;
	private String hotelAddress;
	private String city;
	private String countryName;
	private String starRating;
	private Date checkInDate;
	private Date checkOutDate;
	private String roomName;
	private BigDecimal amount;
	
	@Transient
	@JsonIgnore
	private boolean newEntity;
	
	public HotelBookingEntity() {}
	public HotelBookingEntity(HotelBook hotelBook, boolean isNew) throws ParseException {
		this.setNewEntity(isNew);
		this.setId(hotelBook.getBookingId());
		this.setCheckInDate(new SimpleDateFormat("yyyy-MM-dd").parse(hotelBook.getCheckInDate()));
		this.setCheckOutDate(new SimpleDateFormat("yyyy-MM-dd").parse(hotelBook.getCheckOutDate()));
		this.setCity(hotelBook.getPropertySummary().getAddress().getCity());
		this.setCountryName(hotelBook.getPropertySummary().getAddress().getCountryName());
		this.setHotelName(hotelBook.getPropertySummary().getName());
		this.setIteneraryId(hotelBook.getItineraryId());
		this.setRoomName(hotelBook.getRooms().get(0).getName());
		this.setStarRating(hotelBook.getPropertySummary().getStarRating());
		this.setStatus(hotelBook.getStatus());
		this.setAmount(new BigDecimal(hotelBook.getTotalChargeableRate().getAmount()));
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIteneraryId() {
		return iteneraryId;
	}
	public void setIteneraryId(String iteneraryId) {
		this.iteneraryId = iteneraryId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getStarRating() {
		return starRating;
	}
	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	@Override
	public boolean isNew() {
		return newEntity;
	}
	public boolean isNewEntity() {
		return newEntity;
	}
	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
}
