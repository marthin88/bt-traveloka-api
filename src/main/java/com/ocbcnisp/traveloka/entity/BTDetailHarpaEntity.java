package com.ocbcnisp.traveloka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table("BT_DETAIL_HARPA")
public class BTDetailHarpaEntity implements Persistable<String>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1058693983326939649L;
	@Id
	@Column("BT_ID")
	private String id;
	@Column("NIK")
	private String nik;
	@Column("NAMA")
	private String name;
	@Column("GENDER")
	private String gender;
	@Column("JOB_LEVEL")
	private String jobLevel;
	@Column("TRANSACTION_DATE")
	private Date transactionDate;
	@Column("TRANSACTION_NO")
	private String transactionNumber;
	@Column("TOTAL_CLAIM")
	private BigDecimal totalClaim;
	@Column("PAID_AMOUNT")
	private BigDecimal paidAmount;
	@Column("START_DATE")
	private Date startDate;
	@Column("END_DATE")
	private Date endDate;
	@MappedCollection(idColumn = "BT_ID")
	private Set<ClaimHarpaEntity> claimEntity;
	
	@Transient
	@JsonIgnore
	private boolean newEntity;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public BigDecimal getTotalClaim() {
		return totalClaim;
	}
	public void setTotalClaim(BigDecimal totalClaim) {
		this.totalClaim = totalClaim;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return newEntity;
	}
	public boolean isNewEntity() {
		return newEntity;
	}
	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
	public Set<ClaimHarpaEntity> getClaimEntity() {
		return claimEntity;
	}
	public void setClaimEntity(Set<ClaimHarpaEntity> claimEntity) {
		this.claimEntity = claimEntity;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}
}
