package com.ocbcnisp.traveloka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table("BT_DETAIL")
public class BTDetailEntity implements Persistable<String>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610841941107348176L;
	@Id
	@Column("BT_ID")
	private String id;
	@Column("NIK")
	private String nik;
	@Column("NAMA")
	private String name;
	@Column("GENDER")
	private String gender;
	@Column("JOB_LEVEL")
	private String jobLevel;
	@Column("TRANSACTION_DATE")
	private Date transactionDate;
	@Column("TRANSACTION_NO")
	private String transactionNumber;
	@Column("TOTAL_CLAIM")
	private BigDecimal totalClaim;
	@Column("PAID_AMOUNT")
	private BigDecimal paidAmount;
	@Column("START_DATE")
	private Date startDate;
	@Column("END_DATE")
	private Date endDate;
	@MappedCollection(idColumn = "BT_ID")
	private Set<ClaimEntity> claimEntity = new HashSet<ClaimEntity>();
	
	public BTDetailEntity(BTDetailHarpaEntity btDetailHarpaEntity, boolean newEntity, String bookId) {
		this.id = btDetailHarpaEntity.getId();
		this.nik = btDetailHarpaEntity.getNik();
		this.name = btDetailHarpaEntity.getName();
		this.transactionDate = btDetailHarpaEntity.getTransactionDate();
		this.transactionNumber = btDetailHarpaEntity.getTransactionNumber();
		this.totalClaim = btDetailHarpaEntity.getTotalClaim();
		this.paidAmount = btDetailHarpaEntity.getPaidAmount();
		this.startDate = btDetailHarpaEntity.getStartDate();
		this.endDate = btDetailHarpaEntity.getEndDate();
		this.newEntity = newEntity;
		Set<ClaimEntity> claimSet = new HashSet<ClaimEntity>();
		for (ClaimHarpaEntity claim : btDetailHarpaEntity.getClaimEntity()) {
			if (newEntity == true) {
				claim.setNewEntity(true);
			}else {
				claim.setNewEntity(false);
			}
			ClaimEntity claims = new ClaimEntity(claim);
			claims.setBookId(bookId);
			claimSet.add(claims);
		}
		this.claimEntity = claimSet;
	}
	public BTDetailEntity() {
	}
	@Transient
	@JsonIgnore
	private boolean newEntity;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public BigDecimal getTotalClaim() {
		return totalClaim;
	}
	public void setTotalClaim(BigDecimal totalClaim) {
		this.totalClaim = totalClaim;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return newEntity;
	}
	public boolean isNewEntity() {
		return newEntity;
	}
	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
	public Set<ClaimEntity> getClaimEntity() {
		return claimEntity;
	}
	public void setClaimEntity(Set<ClaimEntity> claimEntity) {
		this.claimEntity = claimEntity;
	}
	
	public void addClaimEntity(ClaimEntity claimEntity) {
		this.claimEntity.add(claimEntity);
	}
	public void addAllClaimEntity(Set<ClaimEntity> claimEntity) {
		this.claimEntity.addAll(claimEntity);
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}
}
