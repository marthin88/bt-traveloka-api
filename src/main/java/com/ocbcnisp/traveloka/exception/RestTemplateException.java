package com.ocbcnisp.traveloka.exception;

import java.io.IOException;

import org.springframework.http.HttpStatus;

public class RestTemplateException extends IOException{

	private HttpStatus statusCode;
	private TravelokaErrorResponse error;
	public RestTemplateException(HttpStatus statusCode, TravelokaErrorResponse error) {
		super(error.getVerboseMessage());
		this.statusCode = statusCode;
		this.error = error;
	}
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	public TravelokaErrorResponse getError() {
		return error;
	}
	public void setError(TravelokaErrorResponse error) {
		this.error = error;
	}
	
	
}
