package com.ocbcnisp.traveloka.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import com.google.gson.Gson;

public class RestTemplateErrorHandler implements ResponseErrorHandler{

	public boolean hasError(ClientHttpResponse response) throws IOException {
		HttpStatus status = response.getStatusCode();
		return status.is4xxClientError() || status.is5xxServerError();
	}

	public void handleError(ClientHttpResponse response) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))){
			String bodyResponse =reader.lines().collect(Collectors.joining(""));
			Gson g = new Gson();
			TravelokaErrorResponse travelokaResponse = g.fromJson(bodyResponse, TravelokaErrorResponse.class);
			throw new RestTemplateException(response.getStatusCode(),travelokaResponse);
		} 
		
	}

}
