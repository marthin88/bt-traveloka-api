package com.ocbcnisp.traveloka.exception;

public class TravelokaErrorResponse {
	private String category;
	private String handling;
	private String errorCode;
	private String message;
	private String verboseMessage;
	private String serverTimestamp;
	private String data;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getHandling() {
		return handling;
	}
	public void setHandling(String handling) {
		this.handling = handling;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVerboseMessage() {
		return verboseMessage;
	}
	public void setVerboseMessage(String verboseMessage) {
		this.verboseMessage = verboseMessage;
	}
	public String getServerTimestamp() {
		return serverTimestamp;
	}
	public void setServerTimestamp(String serverTimestamp) {
		this.serverTimestamp = serverTimestamp;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
