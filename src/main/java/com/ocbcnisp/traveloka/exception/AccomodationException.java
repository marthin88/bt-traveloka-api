package com.ocbcnisp.traveloka.exception;

public class AccomodationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4991163679653501315L;
	public AccomodationException(String message) {
	super(message);
	}

}
