package com.ocbcnisp.traveloka.constants;

public abstract class AppConstants {
	public static final String SORT_POPULAR = "POPULARITY";
	public static final String SORT_REVIEW = "REVIEW";
	public static final String SORT_HIGH_PRC = "HIGHEST_PRICE";
	public static final String SORT_LOW_PRC = "LOWEST_PRICE";
	
	public static final String SUCCESS = "SUCCESS";
	public static final String GENDER_EXCEPTION = "Gender harus sama";
}
