package com.ocbcnisp.traveloka.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StarsFilter {

	private Boolean[] bDua = {false,true,false,false,false};
	private Boolean[] bTiga = {false,false,true,false,false};
	private Boolean[] bEmpat = {false,false,false,true,false};
	
	private List<Boolean> stars;
	
	public StarsFilter(int jobLevel) {
		if (jobLevel == 2) {
			this.stars = new ArrayList<Boolean>(Arrays.asList(this.bDua));
		}else if (jobLevel >=3 && jobLevel < 5) {
			this.stars = new ArrayList<Boolean>(Arrays.asList(this.bTiga));
		}else if (jobLevel >= 5) {
			this.stars = new ArrayList<Boolean>(Arrays.asList(this.bEmpat));
		}
	}

	public List<Boolean> getStars() {
		return stars;
	}
}
