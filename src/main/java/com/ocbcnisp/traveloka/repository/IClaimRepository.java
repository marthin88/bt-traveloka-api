package com.ocbcnisp.traveloka.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ocbcnisp.traveloka.entity.ClaimHarpaEntity;

@Repository
public interface IClaimRepository extends CrudRepository<ClaimHarpaEntity, String>{
}
