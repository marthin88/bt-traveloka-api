package com.ocbcnisp.traveloka.repository;

import org.springframework.data.repository.CrudRepository;

import com.ocbcnisp.traveloka.entity.HotelBookingEntity;

public interface IHotelBookingRepository extends CrudRepository<HotelBookingEntity, String>{
}
