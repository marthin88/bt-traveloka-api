package com.ocbcnisp.traveloka.repository;

import org.springframework.data.repository.CrudRepository;

import com.ocbcnisp.traveloka.entity.GeoLocationEntity;

public interface IGeoLocationRepository2 extends CrudRepository<GeoLocationEntity, String>{

}
