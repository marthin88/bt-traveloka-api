package com.ocbcnisp.traveloka.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ocbcnisp.traveloka.entity.BTDetailHarpaEntity;

@Repository
public interface IBTDetailHarpaRepository extends CrudRepository<BTDetailHarpaEntity, String>{

}
