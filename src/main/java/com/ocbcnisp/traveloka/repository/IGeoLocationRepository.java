package com.ocbcnisp.traveloka.repository;

import java.util.List;

import com.ocbcnisp.traveloka.entity.GeoLocationEntity;

public interface IGeoLocationRepository{

	public List<GeoLocationEntity> findByName(String name);
}
