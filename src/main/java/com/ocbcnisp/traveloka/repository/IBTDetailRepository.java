package com.ocbcnisp.traveloka.repository;

import org.springframework.data.repository.CrudRepository;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;

public interface IBTDetailRepository extends CrudRepository<BTDetailEntity, String>{
}
