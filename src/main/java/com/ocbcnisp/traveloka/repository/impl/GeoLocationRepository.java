package com.ocbcnisp.traveloka.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocbcnisp.traveloka.entity.GeoLocationEntity;
import com.ocbcnisp.traveloka.entity.rowmap.GeoLocationRowMapper;
import com.ocbcnisp.traveloka.repository.IGeoLocationRepository;

@Repository
public class GeoLocationRepository implements IGeoLocationRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ObjectMapper objMapper;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GeoLocationEntity> findByName(String name) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GEO_LOCATION_BY_NAME").returningResultSet("RS", new GeoLocationRowMapper());
		SqlParameterSource source = new MapSqlParameterSource().addValue("V_NAME", name);
		Map<String, Object> result = jdbcCall.execute(source);
		List<GeoLocationEntity> entity =  (List<GeoLocationEntity>) result.get("RS");
		
		return entity;
	}

}
