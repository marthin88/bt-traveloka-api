package com.ocbcnisp.traveloka.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.ocbcnisp.traveloka.entity.BTDetailEntity;
import com.ocbcnisp.traveloka.entity.rowmap.BTDetailRowMapper;

@Repository
public class ProcBTDetailRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@SuppressWarnings("unchecked")
	public BTDetailEntity findEligible(String btId) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_BT_DETAIL_ELIGIBLE").returningResultSet("RS", new BTDetailRowMapper());
		SqlParameterSource source = new MapSqlParameterSource().addValue("V_BT_ID", btId);
		Map<String, Object> result = jdbcCall.execute(source);
		List<BTDetailEntity> entity =  (List<BTDetailEntity>) result.get("RS");
		BTDetailEntity btEntity = entity.get(0);
		for (int i = 1; i < entity.size(); i++) {
			if (entity.get(i).getId().equals(btEntity.getId())) {
				btEntity.addAllClaimEntity(entity.get(i).getClaimEntity());
			}
		}
		return btEntity;
	}
}
